var ERRORMODULE, QUERYMODULE, RECORDMODULE, RUNTIMEMODULE, SEARCHMODULE, TRANSLATIONMODULE, UISERVERWIDGETMODULE, URLMODULE, QUALITYMODULE;
var INDEXLENGTH= 3;
var INDEXSEPARATOR= '-';

/**
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/error', 'N/query', 'N/record', 'N/runtime', 'N/search', 'N/translation', 
	'N/ui/serverWidget', 'N/url', '/SuiteScripts/QualityModule/qualityModule'], runUserEvent);

function runUserEvent(error, query, record, runtime, search, translation, uiserverWidget, url, qualityModule) {
	ERRORMODULE= error;
	QUERYMODULE= query;
	RECORDMODULE= record;
	RUNTIMEMODULE= runtime;
	SEARCHMODULE= search;
	TRANSLATIONMODULE= translation;
	UISERVERWIDGETMODULE= uiserverWidget;
	URLMODULE= url;
	QUALITYMODULE= qualityModule;
	
	var returnObj = {};
	returnObj['beforeLoad'] = _beforeLoad;
	returnObj['beforeSubmit'] = _beforeSubmit;
	returnObj['afterSubmit'] = _afterSubmit;
	return returnObj;
}

/**
 * Function definition to be triggered before record is loaded.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {string} scriptContext.type - Trigger type
 * @param {Form} scriptContext.form - Current form
 * @Since 2015.2
 */
function _beforeLoad(scriptContext) {
	log.debug({
		title: 'Start beforeLoad Script'
	});
	
	var woRecord = scriptContext.newRecord;
	var woRecordId = woRecord.getValue({
		fieldId: 'id'
	});
	
	// 15/04/2020 - Enhancement for quality module - Begin
	// Get setup on user's subsidiary
	var subsidiary = woRecord.getValue('subsidiary');
	var qualitySetup = QUALITYMODULE.getQualitySetup(subsidiary);
	if (subsidiary) {
		customizeLayoutForOutgoingQuality(scriptContext, qualitySetup);			
	}
	// 15/04/2020 - Enhancement for quality module - End
	
	if (scriptContext.request && scriptContext.type == scriptContext.UserEventType.VIEW) {
		// Attach the client script to handle actions on buttons
    	scriptContext.form.clientScriptModulePath = 'SuiteScripts/QualityModule/mob_cs_workorderquality.js';
		
		var userLanguage = RUNTIMEMODULE.getCurrentUser().getPreference('language');
		var printButtonLabel;
		switch(userLanguage){
			case 'fr_CA':
    		case 'fr_FR':
    			printButtonLabel = 'Etiquette Production';
    			break;
			default: 
				printButtonLabel = 'Print Labels';
		}
		/*
    	//get the url for the suitelet - add woRecordId parameter
    	var suiteletURL = URLMODULE.resolveScript({
    	 'scriptId':'customscript_mob_sl_display_prod_labels',
    	 'deploymentId':'customdeploy_mob_sl_display_prod_labels',
    	 'returnExternalUrl': false
    	}) + '&mob_worecordid=' + woRecordId;

    	//hook point for the button that will be fired client side;
//    	var scr = "require([], function() { window.open('"+creURL+"','_blank');});";
    	var scr = "require([], function() { window.open('"+suiteletURL+"');});";

    	// log.debug(funcName, scr);
    	*/

    	//add print labels button
		scriptContext.form.addButton({
			id : 'custpage_mob_button_print_labels',
			label : printButtonLabel,
			functionName : 'printLabels'
		});
		
		// Disable close button until quality results are completed
		// Display 'Force Close' custom button
		if (qualitySetup.outgoingQualityFlag) {
			var forceCloseParam = scriptContext.request.parameters.custparam_mob_forceclose;
			if (!forceCloseParam) {
				var closeButton = scriptContext.form.getButton({
					id: 'closeremaining' //'close' 'Close'
				});
				if (closeButton && !checkQualityTestAreCompleted(woRecordId)) {
					closeButton.isDisabled = true;
					
			    	//get the url for the suitelet - add woRecordId parameter
			    	var woRecordURL = URLMODULE.resolveRecord({
			    		recordType: 'workorder',
			    		recordId: woRecordId,
			    		params: {custparam_mob_forceclose: true}
			    	});
			    	
			    	//hook point for the button that will be fired client side;
//			    	var scr = "require([], function() { window.open('"+woRecordURL+"');});";
//			    	var scr = "require([], function() { window.location = '"+woRecordURL+"';});";
			    	
					var forceCloseButton = scriptContext.form.addButton({
						id: 'custpage_mob_forceclosebutton',
						label: TRANSLATIONMODULE.get({collection: 'custcollection_mob_outgoingquality', key: 'FORCE_CLOSE_BTN'})(),
						functionName: 'openWithForceClose'
					});				
				}				
			}
		}
		
		// Add Receive button and quality button when outgoing quality is enabled for the subsidiary
		// Check status == Released ('B') or In Process ('D')
		var orderStatus = woRecord.getValue('orderstatus');
		log.debug('order status ' + orderStatus);
		if (qualitySetup.outgoingQualityFlag && ['B', 'D'].indexOf(orderStatus) >= 0 ) {
			// At beforeLoad stage related lists do not contain any data ==> lineCount is always zero
			// ==> need to perform a search action on customrecord_mob_wo_pre_completion record
//			var lineCount = woRecord.getLineCount({
//				sublistId: 'recmachcustrecord_mob_cmpl_wonumber'
//			});
			var searchObj = SEARCHMODULE.create({
				type: 'customrecord_mob_wo_pre_completion',
				columns: ['internalid'],
				filters: [SEARCHMODULE.createFilter({
					name: 'custrecord_mob_cmpl_wonumber',
					operator: SEARCHMODULE.Operator.IS,
					values: woRecordId
				})]
			});
			var lineCount = searchObj.run().getRange({start: 0, end: 1}).length;
			if (lineCount > 0) {
		    	//add Receive button to the form
				scriptContext.form.addButton({
					id : 'custpage_mob_button_receive',
					label : TRANSLATIONMODULE.get({collection: 'custcollection_mob_outgoingquality', key: 'RECEIVE_BTN'})(),
					functionName : 'receive'
				});
				
			}
		}
		var woOutgoingQuality = woRecord.getValue({ fieldId: 'custbody_mob_outgoing_quality_' });
		// Check status == Released ('B'), In Process ('D') or Closed ('H')
		//add Quality button to the form
		if (woOutgoingQuality 
			&& qualitySetup.outgoingQualityFlag 
			&& ['B', 'D', 'H'].indexOf(orderStatus) >= 0 ) {
			//add Quality button to the form
			scriptContext.form.addButton({
				id : 'custpage_mob_button_quality',
				label : TRANSLATIONMODULE.get({collection: 'custcollection_mob_outgoingquality', key: 'QUALITY_BTN'})(),
				functionName : 'quality'
			});
		}
	}
	
	log.debug({
		title: 'End beforeLoad Script'
	});
}

/**
 * Function definition to be triggered before record is submitted.
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {Record} scriptContext.oldRecord - Old record
 * @param {string} scriptContext.type - Trigger type
 * @Since 2015.2
 */
function _beforeSubmit(scriptContext) {
	if ([scriptContext.UserEventType.EDIT, scriptContext.UserEventType.XEDIT].includes(scriptContext.type)) {
		let fieldsToCheck = ['quantity', 'custbodyrvd_number_of_packaging', 'custbody_mob_silocapacity', 
			'custbody_mob_containercapacity', 'assemblyitem', 'custbody_mob_wo_lot'];
		if (checkQualityIsStarted(scriptContext.newRecord.id) && checkAnyFieldIsModified(scriptContext, fieldsToCheck)) {
			let errorMessage;
			try {
				errorMessage = TRANSLATIONMODULE.get({collection: 'custcollection_mob_outgoingquality', key: 'INVALID_FLD_CHANGE_MSG'})();
			} catch (ex) {
				errorMessage = 'Quality has already been started, it is no longuer possible to modify any of this fields';
			}
			errorMessage += `: ${fieldsToCheck.join(',')}`;
			throw ERRORMODULE.create({
				name: 'BWP_ERROR',
				message: errorMessage
			});
		}
	}
}

function checkQualityIsStarted(woId) {
	let sqlString = `	Select top 1 'exists'
						From customrecord_mob_wo_productionsilo
						Where custrecord_mob_cmpl_qualresultset is not null
						  And custrecord_mob_cmpl_silowonumber = ${woId} `;
	let resultSet = QUERYMODULE.runSuiteQL({
		query: sqlString
	});
	return resultSet.results.length > 0;
}

function checkAnyFieldIsModified(scriptContext, fieldsToCheck) {
	let returnValue = false;
	for (let field of fieldsToCheck) {
		// logVar(field, 'field');
		if (checkFieldIsModified(scriptContext, field)) {
			log.debug({
				title: 'Field Value was changed',
				details: field
			});
			returnValue = true;
			break;
		}
	}
	return returnValue;
}

function checkFieldIsModified(scriptContext, fieldId) {
	let oldValue = (scriptContext.oldRecord || scriptContext.newRecord).getValue({ fieldId: fieldId });
	if (scriptContext.newRecord.getFields().includes(fieldId)) {
		return scriptContext.newRecord.getValue({ fieldId: fieldId }) != oldValue;
	}
	return false;
}

/**
 * Function definition to be triggered after record is submitted.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {Record} scriptContext.oldRecord - Old record
 * @param {string} scriptContext.type - Trigger type
 * @Since 2015.2
 */
function _afterSubmit(scriptContext) {	
	log.debug({
		title: 'Start afterSubmit Script'
	});
	
	var scriptObj = RUNTIMEMODULE.getCurrentScript();
	log.debug({
		title: 'Remaining Usage',
		details: scriptObj.getRemainingUsage()
	});
	
	var indexLength = INDEXLENGTH; 			// The length of the index part of package numbers
	var indexSeparator = INDEXSEPARATOR;	// The separator to use between the prefix part and the index part of the package number	
	
	//var packageSize = 0; 			// The size of a package
	//var numberOfSilos = 1;
	//var numberOfContainers = 0; 	// The number of containers (bigbag, octabin, ...) - can be calculated or populated by the user on the WO
	//var containersPerSilo = 0;

	var createPreCompletion = false;
	
	var woRecord = scriptContext.newRecord;
	var woRecordId = woRecord.getValue('id');
	
	/*
	// Get quality setup on user's subsidiary
	var qualitySetup = QUALITYMODULE.getQualitySetup();
	*/
	
	// Read information on the WO
	var numberOfPackaging = woRecord.getValue({
		fieldId: 'custbodyrvd_number_of_packaging'
	});
	
	var item = woRecord.getValue({
		fieldId: 'assemblyitem'
	});
	var woQuantity = woRecord.getValue({
		fieldId: 'quantity'
	});
	var subsidiary = woRecord.getValue({
		fieldId: 'subsidiary'
	});
	// When record is just created tranid is 'To Be Generated'.
	// Don't want to test on a hard coded string so always get tranid thru a new acces to the record on the server
	var tranid;
	var woFieldsValues = SEARCHMODULE.lookupFields({
		type: SEARCHMODULE.Type.WORK_ORDER,
		id: woRecordId,
		columns: ['tranid']
	});
	if ('tranid' in woFieldsValues) {
		tranid = woFieldsValues['tranid'];
	}

	var qualitySetup = QUALITYMODULE.getQualitySetup(subsidiary);
	
	var automaticLotNumbering = false;
	if (subsidiary) {
		var subsidiaryFieldsValues = SEARCHMODULE.lookupFields({
			type: SEARCHMODULE.Type.SUBSIDIARY,
			id: subsidiary,
			columns: ['custrecord_wo_lotnumber_automatic']
		});
		logRecord(subsidiaryFieldsValues, 'subsidiaryFieldsValues');
		if ('custrecord_wo_lotnumber_automatic' in subsidiaryFieldsValues) {
			automaticLotNumbering = subsidiaryFieldsValues['custrecord_wo_lotnumber_automatic'];
		}	
	}
	var lotNumber = woRecord.getValue({
		fieldId: 'custbody_mob_wo_lot'
	});
	if (!lotNumber && automaticLotNumbering) {
		lotNumber = tranid;
		RECORDMODULE.submitFields({
			type: RECORDMODULE.Type.WORK_ORDER,
			id: woRecordId,
			values: {
				custbody_mob_wo_lot: lotNumber
			}
		});
	}
	var capacityOfSilo = woRecord.getValue({
		fieldId: 'custbody_mob_silocapacity'
	});
	var capacityOfContainer = woRecord.getValue({
		fieldId: 'custbody_mob_containercapacity'
	});
	
	var preCompletionSublist = woRecord.getSublist({
		sublistId: 'recmachcustrecord_mob_cmpl_wonumber'
	});
	
	var completionListLineCount = woRecord.getLineCount({
        sublistId : 'recmachcustrecord_mob_cmpl_wonumber'
    });	
	
	if (completionListLineCount == 0) {
		createPreCompletion = true;
	}
	
	// When editing the WO, check for change in information that have an impact on WO labels
	// In case some value has changed, delete the existing wo_pre_completion records
	if (scriptContext.type == scriptContext.UserEventType.EDIT){
    	var woRecordOld = scriptContext.oldRecord;
		var woQuantityOld = woRecordOld.getValue({
    		fieldId: 'quantity'
    	});
		var lotNumberOld = woRecordOld.getValue({
    		fieldId: 'custbody_mob_wo_lot'
    	});
		var numberOfPackagingOld = woRecordOld.getValue({
			fieldId: 'custbodyrvd_number_of_packaging'
		});
		
		// 15/04/2020 - Enhancement for quality module - Begin
		var capacityOfSiloOld = woRecordOld.getValue({
    		fieldId: 'custbody_mob_silocapacity'
    	});
		var capacityOfContainerOld = woRecordOld.getValue({
    		fieldId: 'custbody_mob_containercapacity'
    	});
		// 15/04/2020 - Enhancement for quality module - End

		var itemOld = woRecordOld.getValue({
			fieldId: 'assemblyitem'
		});
		
		// Remove existing wo pre completion and production silo
		if (woQuantity != woQuantityOld || lotNumber != lotNumberOld || numberOfPackaging != numberOfPackagingOld
				|| capacityOfSilo != capacityOfSiloOld || capacityOfContainer != capacityOfContainerOld || item != itemOld) {
			createPreCompletion = true;
			/*
			 * This code no longer works because it is in aftersubmit. Initially it was in before submit
			if (completionListLineCount > 0) {
				for (var i = 1 ; i <= completionListLineCount ; i++) {
					woRecord.removeLine({
						sublistId: 'recmachcustrecord_mob_cmpl_wonumber',
						line: 0,
						ignoreRecalc: true
					});
				}	
				var siloListLineCount = woRecord.getLineCount({
			        sublistId : 'recmachcustrecord_mob_cmpl_silowonumber'
			    });
				log.debug({
					title: 'siloListLineCount',
					details: siloListLineCount
				});
				for (var i = 1 ; i <= siloListLineCount ; i++) {
					woRecord.removeLine({
						sublistId: 'recmachcustrecord_mob_cmpl_silowonumber',
						line: 0,
						ignoreRecalc: true
					});
				}
			}
			*/
			
			deleteWoPreCompletions(woRecordId);
		}	    	
	}
	
	if (!qualitySetup.outgoingQualityFlag) {
		// Original functionality (without outgoing quality)
		var numberOfContainers = parseInt(numberOfPackaging || 0, 10);
		capacityOfSilo = woQuantity;
		// In case number of containers is not populated on the WO, calculate the value based on container capacity parameter
		if (numberOfContainers <= 0) {
			var scriptObj = RUNTIMEMODULE.getCurrentScript();
			// When quantity exceeds the capacity of a container it must be dispatched on several containers
			capacityOfContainer = scriptObj.getParameter({
				name: 'custscript_mob_lotsize'
			})
			// Calculate the number of complete packages
			// https://stackoverflow.com/questions/4228356/integer-division-with-remainder-in-javascript
			numberOfContainers = Math.floor(woQuantity/capacityOfContainer);
			// Calculate the size of the last package
			var lastContainerSize = woQuantity % capacityOfContainer;
			if (lastContainerSize > 0) {
				numberOfContainers++;
			} else {
				lastContainerSize = capacityOfContainer;
			}
		} else {
			capacityOfContainer = Math.ceil(woQuantity/numberOfContainers);
		}	
	} else {	
		if (!qualitySetup.multiSiloFlag) {
			// Multisilo mode is not activated: default the capacity of silo with the total quantity of the wo
			capacityOfSilo = woQuantity;
		}
	}
	
	if (scriptContext.type == scriptContext.UserEventType.CREATE && preCompletionSublist != null){
		/*
        // Use event type EDIT for testing
        if (scriptContext.type == scriptContext.UserEventType.EDIT){
		*/
		createPreCompletion = true;
	}
	
	if (createPreCompletion) {		
		createPrecompletionList(woRecord, item, woQuantity, capacityOfSilo, capacityOfContainer, lotNumber, tranid);	
	}
	
	log.debug({
		title: 'End afterSubmit Script'
	});
}

function deleteWoPreCompletions(woRecordId) {
	log.debug('deleteWoPreCompletions started');	
	var scriptObj = RUNTIMEMODULE.getCurrentScript();
	log.debug({
		title: 'Remaining Usage',
		details: scriptObj.getRemainingUsage()
	});
	
	var woPreCompletionSearch = SEARCHMODULE.create({
		type: 'customrecord_mob_wo_pre_completion',
		columns: ['internalid', 'custrecord_mob_cmpl_siloid'],
  		filters: [
            SEARCHMODULE.createFilter({
				name: 'custrecord_mob_cmpl_wonumber',
				operator: SEARCHMODULE.Operator.IS,
				values: woRecordId
			})
		]
	});
	
	var silos = [];
	woPreCompletionSearch.run().each(function(result) {
		var siloId = result.getValue('custrecord_mob_cmpl_siloid');
		var found = false;
		for (var i = 0 ; i < silos.length ; i++) {
			if (silos[i] == siloId) {
				found = true;
				break;
			}
		}
		if (!found) {
			silos.push(siloId);
		}
		RECORDMODULE['delete']({
			type: 'customrecord_mob_wo_pre_completion',
			id: result.getValue('internalid')
		});
		return true;
	});
	for (var i = 0 ; i < silos.length ; i++) {
		RECORDMODULE['delete']({
			type: 'customrecord_mob_wo_productionsilo',
			id: silos[i]
		});
	}
	
	log.debug({
		title: 'Remaining Usage',
		details: scriptObj.getRemainingUsage()
	});	
	log.debug('deleteWoPreCompletions done');
} 

function checkQualityTestAreCompleted(workOrderId) {
	var returnValue = false;
	
	// Retrieve the result sets linked to the wocompetions of the work order
	// Check the work order field is populated on each result set
	var listOfWoCompletions = [];
	var woCompletionSearchObj = SEARCHMODULE.create({
		type: SEARCHMODULE.Type.WORK_ORDER_COMPLETION,
		columns: ['internalid'],
  		filters: [
            SEARCHMODULE.createFilter({
				name: 'createdfrom',
				operator: SEARCHMODULE.Operator.IS,
				values: workOrderId
			}),
			SEARCHMODULE.createFilter({
				name: 'mainline',
				operator: SEARCHMODULE.Operator.IS,
				values: true
			})
		]
	});
	woCompletionSearchObj.run().each(function(result) {
		listOfWoCompletions.push(result.getValue('internalid'));
		return true;
	});
//	logRecord(listOfWoCompletions, 'listOfWoCompletions');
	
	if (listOfWoCompletions.length > 0) {
		returnValue = true;
		var resultSetSearchObj = SEARCHMODULE.create({
			type: 'customrecord_mob_qualityresultset',
			columns: ['custrecord_mob_resultsetwonumber'],
	  		filters: [
	            SEARCHMODULE.createFilter({
					name: 'custrecord_mob_resultsetwocompletion',
					operator: SEARCHMODULE.Operator.ANYOF,
					values: listOfWoCompletions
				})
			]
		});		
		resultSetSearchObj.run().each(function(result) {
			logRecord(result, 'result');
			// Reminder: woNumber in resultset is populated only when the quality workflow is completed and the status of
			// the quality result set is definitevely defined
			var woNumber = result.getValue('custrecord_mob_resultsetwonumber');
			if (!woNumber) {
				returnValue = false;
				return false;
			}
			return true;
		});
	}	
	
	return returnValue;
}

function customizeLayoutForOutgoingQuality(scriptContext, qualitySetup) {
	var outgoingQualityField = scriptContext.form.getField({
		id: 'custbody_mob_outgoing_quality_'
	});
	var capacityOfSiloField = scriptContext.form.getField({
		id: 'custbody_mob_silocapacity'
	});
	var capacityOfContainerField = scriptContext.form.getField({
		id: 'custbody_mob_containercapacity'
	});	
	
	// Hide fields depending on setup
	if (!qualitySetup.outgoingQualityFlag) {
		outgoingQualityField.updateDisplayType({
			displayType: UISERVERWIDGETMODULE.FieldDisplayType.HIDDEN
		});
		capacityOfSiloField.updateDisplayType({
			displayType: UISERVERWIDGETMODULE.FieldDisplayType.HIDDEN
		});
		capacityOfContainerField.updateDisplayType({
			displayType: UISERVERWIDGETMODULE.FieldDisplayType.HIDDEN
		});	
	} else if (!qualitySetup.multiSiloFlag) {
		capacityOfSiloField.updateDisplayType({
			displayType: UISERVERWIDGETMODULE.FieldDisplayType.HIDDEN
		});
	}
	
	// Hide an original field from label printing
	if (qualitySetup.outgoingQualityFlag) {
		var numberOfPackagingField = scriptContext.form.getField({
			id: 'custbodyrvd_number_of_packaging'
		});
		numberOfPackagingField.updateDisplayType({
			displayType: UISERVERWIDGETMODULE.FieldDisplayType.HIDDEN
		});
	}
}

function createPrecompletionList(woRecord, item, woQuantity, capacityOfSilo, capacityOfContainer, lotNumber, tranid) {
	log.debug('createPrecompletionList started');	
	var scriptObj = RUNTIMEMODULE.getCurrentScript();
	log.debug({
		title: 'Remaining Usage',
		details: scriptObj.getRemainingUsage()
	});
	
	woQuantity = parseInt(woQuantity || 0, 10);
	capacityOfSilo = parseInt(capacityOfSilo || 0, 10);
	capacityOfContainer = parseInt(capacityOfContainer || 0, 10);
	// woQuantity, capacityOfSilo and capacityOfContainer parameters must be non zero
	if (woQuantity*capacityOfSilo*capacityOfContainer == 0) {
		return;
	}
	
	var indexLength = INDEXLENGTH; 			// The length of the index part of package numbers
	var indexSeparator = INDEXSEPARATOR;	// The separator to use between the prefix part and the index part of the package number
	
	var woRecordId = woRecord.getValue({
		fieldId: 'id'
	});
	
	var remainingOnWO = woQuantity;
	var lineQuantity = 0;
	var lineIndex = 0;
	var siloNum = 1;
	while (remainingOnWO > 0) {
		var remainingOnSilo = Math.min(remainingOnWO, capacityOfSilo);
		var siloRecord = RECORDMODULE.create({
			type: 'customrecord_mob_wo_productionsilo'
		});
		siloRecord.setValue({
			fieldId: 'name',
			value: tranid + ' Silo ' + siloNum
		});
		siloRecord.setValue({
			fieldId: 'custrecord_mob_cmpl_silo',
			value: siloNum
		});
		siloRecord.setValue({
			fieldId: 'custrecord_mob_cmpl_silowonumber',
			value: woRecordId
		});
		var siloRecordId = siloRecord.save();
		while (remainingOnSilo > 0) {
			lineQuantity = Math.min(remainingOnSilo, capacityOfContainer);
			remainingOnSilo -= lineQuantity;
			remainingOnWO -= lineQuantity;
			
			
			// Eric Moulin - 07/10/2020 - No longer concatenate the lot number in the package number
//			var packageIndex = String.concat('00000000', lineIndex + 1).slice(-indexLength);
//			var packageNumber = lotNumber.concat(indexSeparator, packageIndex);

			// var packageNumber = String.concat('00000000', lineIndex + 1).slice(-indexLength);
			var packageNumber = (lineIndex + 1).toString().padStart(indexLength, '0');
			
			/*
			 * This code no longer works because it is in aftersubmit. Initially it was in before submit
			woRecord.insertLine({
				sublistId: 'recmachcustrecord_mob_cmpl_wonumber',
				line: lineIndex,
				ignoreRecalc: true
			});

			woRecord.setSublistValue({
			    sublistId: 'recmachcustrecord_mob_cmpl_wonumber',
			    fieldId: 'custrecord_mob_cmpl_item',
			    line: lineIndex,
			    value: item
			});
			woRecord.setSublistValue({
			    sublistId: 'recmachcustrecord_mob_cmpl_wonumber',
			    fieldId: 'custrecord_mob_cmpl_lotnumber',
			    line: lineIndex,
			    value: lotNumber
			});
			woRecord.setSublistValue({
			    sublistId: 'recmachcustrecord_mob_cmpl_wonumber',
			    fieldId: 'custrecord_mob_cmpl_packagenumber',
			    line: lineIndex,
			    value: packageNumber
			});
			woRecord.setSublistValue({
			    sublistId: 'recmachcustrecord_mob_cmpl_wonumber',
			    fieldId: 'custrecord_mob_cmpl_quantity',
			    line: lineIndex,
			    value: lineQuantity
			});
			woRecord.setSublistValue({
			    sublistId: 'recmachcustrecord_mob_cmpl_wonumber',
			    fieldId: 'custrecord_mob_cmpl_siloid',
			    line: lineIndex,
			    value: siloRecordId
			});
			
			lineIndex++;
			*/			

			var woprecompletionRecord = RECORDMODULE.create({
				type: 'customrecord_mob_wo_pre_completion'
			});
			woprecompletionRecord.setValue({
				fieldId: 'custrecord_mob_cmpl_wonumber',
				value: woRecordId
			});
			woprecompletionRecord.setValue({
				fieldId: 'custrecord_mob_cmpl_item',
				value: item
			});
			woprecompletionRecord.setValue({
				fieldId: 'custrecord_mob_cmpl_lotnumber',
				value: lotNumber
			});
			woprecompletionRecord.setValue({
				fieldId: 'custrecord_mob_cmpl_packagenumber',
				value: packageNumber
			});
			woprecompletionRecord.setValue({
				fieldId: 'custrecord_mob_cmpl_quantity',
				value: lineQuantity
			});
			woprecompletionRecord.setValue({
				fieldId: 'custrecord_mob_cmpl_siloid',
				value: siloRecordId
			});
			woprecompletionRecord.setValue({
				fieldId: 'custrecord_mob_cmpl_sysid',
				value: (new Date()).getTime().toString()
			});
			var woprecompletionRecordId = woprecompletionRecord.save();			

			lineIndex++;
		}
		siloNum++;
	}
	
	log.debug({
		title: 'Remaining Usage',
		details: scriptObj.getRemainingUsage()
	});	
	log.debug('createPrecompletionList done');	
}

function logRecord(record, logTitle) {
	var logRecord = JSON.stringify(record) || 'UNDEFINED';
	var recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);		
	}
}

function logVar(value, title) {
	log.debug({
		title: title + ' (' + typeof value + ')',
		details: value 
	});	
}
