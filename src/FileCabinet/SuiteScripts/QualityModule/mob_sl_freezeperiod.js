var FORMATMODULE, RECORDMODULE, SEARCHMODULE, TRANSLATIONMODULE, UISERVERWIDGETMODULE, QUALITYMODULE;

/**
 * @NApiVersion 2.1
 * @NScriptType Suitelet
 */
define(['N/format', 'N/record', 'N/search', 'N/translation', 'N/ui/serverWidget', './qualityModule'], runSuitelet);

function runSuitelet(format, record, search, translation, uiserverWidget, qualityModule) {
    FORMATMODULE= format;
    RECORDMODULE= record;
	SEARCHMODULE= search;
	TRANSLATIONMODULE= translation;
	UISERVERWIDGETMODULE= uiserverWidget;
    QUALITYMODULE= qualityModule;
	
	let returnObj = {};
	returnObj['onRequest'] = _onRequest;
	return returnObj;
}
   
/**
 * Defines the Suitelet script trigger point.
 * @param {Object} scriptContext
 * @param {ServerRequest} scriptContext.request - Incoming request
 * @param {ServerResponse} scriptContext.response - Suitelet response
 * @since 2015.2
 */
function _onRequest(context) {
    let method = context.request.method;
    // logRecord(context, 'context');

	log.debug(method);
    if (method == 'GET') {
    	getFunction(context);
    } else if (method == 'POST') {
    	postFunction(context);
    }
}

function getFunction(context) {
  	let form = getFormTemplate();  	
  	context.response.writePage(form);
}

function postFunction(context) {
    processPostedData(context);
	let form = getFormTemplate();
  	context.response.writePage(form);
}

function getTranslationInfo() {
    // otis: objects translation infos
    let otis = { };
    otis.formTitle = { alias: 'quality', key: 'SL_FREEZEPERIODFORM_TITLE', defaultValue: 'no title', type: 'formTitle'  };
    addOtisForHeader(otis);
    addOtisForButtons(otis);
    let translationHandle = TRANSLATIONMODULE.load({
        collections: [
            {
                collection: 'custcollection_mob_outgoingquality', 
                alias: 'quality', 
                keys: retrieveTranslationKeys(otis)
            }
        ]
    });

    return { otis: otis, translationHandle: translationHandle };
}

function addOtisForHeader(otis) {
    otis.custpage_mob_periodtofreeze = { alias: 'quality', key: 'PERIODTOFREEZE_FLD', defaultValue: 'WorkOrder', type: 'field' };
    otis.custpage_mob_frozenperiod = { alias: 'quality', key: 'FROZENPERIOD_FLD', defaultValue: 'Customer', type: 'field'  };
}

function addOtisForButtons(otis) {
    otis.submitButton = { alias: 'quality', key: 'SUBMIT_BTN', defaultValue: 'submitButton', type: 'button' };
}
 
function getFormTemplate() {
	let translationInfo = getTranslationInfo();

    let form = UISERVERWIDGETMODULE.createForm({
        title : applyTranslation(translationInfo, 'formTitle')
    });
    // form.clientScriptModulePath = 'SuiteScripts/QualityModule/mob_cs_freezeperiod.js';

    let periods = retrieveData();
    getFormTemplateHeader(form, periods, translationInfo);
    populateFormTemplateHeader(form, periods, translationInfo);

    form.addSubmitButton({
        label : applyTranslation(translationInfo, 'submitButton')
    });
    
    return form;
}

/**
 * Create header fields
 * @param {*} form 
 */
function getFormTemplateHeader(form, periods, translationInfo) {
    let fieldGroup = form.addFieldGroup({
        id: 'custpage_mob_periods_fieldgroup',
        label: 'periodes'
    });
    fieldGroup.isBorderHidden = true;
    fieldGroup.isSingleColumn = true;
    fieldGroup.label = '';

    // Field custpage_mob_frozenperiod
    let frozenPeriodField = form.addField({
    	id: 'custpage_mob_frozenperiod',
    	label: 'custpage_mob_frozenperiod',
    	type: UISERVERWIDGETMODULE.FieldType.TEXT,
    	// type: UISERVERWIDGETMODULE.FieldType.SELECT,
    	// source: 'accountingperiod',
        container: 'custpage_mob_periods_fieldgroup'
    });
    frozenPeriodField.label = applyTranslation(translationInfo, frozenPeriodField.id);
    frozenPeriodField.updateDisplayType({ displayType: UISERVERWIDGETMODULE.FieldDisplayType.INLINE });

    // Field custpage_mob_periodtofreeze
    let periodToFreezeField = form.addField({
    	id: 'custpage_mob_periodtofreeze',
    	label: 'custpage_mob_periodtofreeze',
    	type: UISERVERWIDGETMODULE.FieldType.SELECT,
        container: 'custpage_mob_periods_fieldgroup'
    });
    periodToFreezeField.label = applyTranslation(translationInfo, periodToFreezeField.id);
    for (let i = 0 ; i < periods.available.length ; i++) {
    	let period = periods.available[i];
    	periodToFreezeField.addSelectOption({
    		value: period.value,
    		text: period.text,
    		isSelect: false
    	});
    }

    // Frozen period as text because the one based on a select source can't be empty, it contains a default value
    let frozenPeriodIdField = form.addField({
    	id: 'custpage_mob_frozenperiodid',
    	label: 'custpage_mob_frozenperiodid',
    	type: UISERVERWIDGETMODULE.FieldType.TEXT,
        container: 'custpage_mob_periods_fieldgroup'
    });
    frozenPeriodIdField.label = 'Hidden frozen period id';
    frozenPeriodIdField.updateDisplayType({ displayType: UISERVERWIDGETMODULE.FieldDisplayType.HIDDEN });
}

function populateFormTemplateHeader(form, periods, translationInfo) {
    if (periods.toFreeze) {
        form.getField({ id: 'custpage_mob_periodtofreeze' }).defaultValue = periods.toFreeze.value;
    }
    if (periods.frozen) {
        form.getField({ id: 'custpage_mob_frozenperiod' }).defaultValue = periods.frozen.period.text;
        form.getField({ id: 'custpage_mob_frozenperiodid' }).defaultValue = periods.frozen.period.value;
    } else {
        form.getField({ id: 'custpage_mob_frozenperiod' }).updateDisplayType({ displayType: UISERVERWIDGETMODULE.FieldDisplayType.HIDDEN });
    }
}

function processPostedData(context) {
    let periodToFreeze = context.request.parameters.custpage_mob_periodtofreeze;
    let frozenPeriod = QUALITYMODULE.retrieveFrozenPeriod();
    // logVar(periodToFreeze, 'periodToFreeze');
    // logVar(frozenPeriod, 'frozenPeriod');
    let record;
    if (frozenPeriod) {
        record = RECORDMODULE.load({
            type: 'customrecord_mob_frozenperiod',
            id: frozenPeriod.internalId,
            isDynamic: false
        });
    } else {        
        record = RECORDMODULE.create({
            type: 'customrecord_mob_frozenperiod',
            isDynamic: false
        });
    }
    record.setValue({ fieldId: 'custrecord_mob_frozenperiod', value: periodToFreeze});
    let fieldsValues = SEARCHMODULE.lookupFields({
        type: SEARCHMODULE.Type.ACCOUNTING_PERIOD,
        id: periodToFreeze,
        columns: ['enddate']
    });
    if ('enddate' in fieldsValues) {
        record.setValue({ 
            fieldId: 'custrecord_mob_frozenperiodenddate', 
            value: FORMATMODULE.parse({
                value: fieldsValues.enddate,
                type: FORMATMODULE.Type.DATE
            })
        });
    }
    record.save();
}

function retrieveData() {
    // let periods = {
    //     frozen: { internalid: 1, period: { value: 234, endDate: '31/12/2021' }},
    //     toFreeze: { value: 254, endDate: '31/01/2022' },
    //     available: [ { value: 254, text: 'Jan. 2022', endDate: '31/01/2022' },
    //         { value: 234, text: 'déc. 2021', endDate: '31/12/2021' },
    //         { value: 233, text: 'nov. 2021', endDate: '30/11/2021' },
    //         { value: 232, text: 'oct. 2021', endDate: '31/10/2021' }            
    //     ]
    // };

    /*
    toFreeze: the period prior to the actual date
    frozen: the period actually frozen (from the customrecord customrecord_mob_frozenperiod)
    available: all periods the user could choose to freeze. The list starts from the nearest locked period 
        and ends with the period prior to the actual date
    */

    let periods = {
        frozen: '',
        toFreeze: '',
        available: []
    };
    // Search on accountingperiod
    // A filter on start date is hard coded so it is not possible to unlock a period before the migration date
    // (because migration of wo completions on closed period does not allow the split, the migrated data is not compliant
    // with the new receive form --> we don't want any pre completion created on period prior to the migration date being updated
    // thru the receive form)
    let migrationDate = FORMATMODULE.format({
        value: new Date(2022, 2, 1),
        type: FORMATMODULE.Type.DATE
    });
    let searchObj = SEARCHMODULE.create({
        type: SEARCHMODULE.Type.ACCOUNTING_PERIOD,
        filters: [
            ['enddate', 'before', 'today'], 
            // ['startdate','before','today'], // for testing purpose
            'AND', 
            ['isquarter', 'is', 'F'], 
            'AND', 
            ['isyear', 'is', 'F'],
            'AND',
            ['startdate', 'onorafter', migrationDate]

        ],
        columns: ['internalid', 'periodname', 'alllocked', 'startdate',
            SEARCHMODULE.createColumn({
              name: 'enddate',
              sort: SEARCHMODULE.Sort.DESC
            })
        ]
    });

    let line = 1;
    searchObj.run().each(result => {
        // logRecord(result, 'result');
        let period = {
            value: result.getValue({ name: 'internalid' }),
            text: result.getValue({ name: 'periodname' }),
            endDate: result.getValue({ name: 'enddate' }),
        };
        if (line++ == 1) {
            periods.toFreeze = period;
            periods.available = [];
        }
        periods.available.push(period);
        // end after the first locked period
        return !result.getValue({ name: 'alllocked' });
    });

    periods.frozen = QUALITYMODULE.retrieveFrozenPeriod();

    return periods;
}


function retrieveNotStartedStatus() {
    let searchObj = SEARCHMODULE.create({
        type: 'customlist_mob_qualityglobalstatus',
        filters: [['scriptid', 'is', 'VAL_260840_5028481_SB1_408']],
        columns: ['internalid', 'name']
    });
    let searchResult = searchObj.run().getRange({ start: 0, end: 1 })[0];
    return {
        value: searchResult.getValue({ name: 'internalid' }),
        text: searchResult.getValue({ name: 'name' })
    };
}

/**
 * 
 * @param {*} translationInfo 
 * @param {*} objectKey the scriptid of the ui object
 * @returns 
 */
function applyTranslation(translationInfo, objectKey) {
    let ti = translationInfo;
    let oti = ti.otis[objectKey];
    if (!oti) return `${objectKey}**`
    try {
        return ti.translationHandle[oti['alias']][oti['key']]();
    } catch (ex) {
        return `${oti['key']}*`;
        return `${oti['defaultValue']}*`;
    }
}

function retrieveTranslationKeys(ftis) {
    let keys = [];
    for (let property in ftis) {
        if (ftis.hasOwnProperty(property)) {
            keys.push(ftis[property]['key']);
        }
    }
    logVar(keys, 'keys');
    return keys;
}

function logRecord(record, logTitle) {
	let logRecord = JSON.stringify(record) || 'UNDEFINED';
	let recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);		
	}
}

function logVar(value, logTitle) {
	log.debug({
		title: logTitle + ' (' + typeof value + ')',
		details: value 
	});	
}
