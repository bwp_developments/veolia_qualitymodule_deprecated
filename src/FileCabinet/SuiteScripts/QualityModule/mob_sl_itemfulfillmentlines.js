var RECORDMODULE, RUNTIMEMODULE, SEARCHMODULE, UISERVERWIDGETMODULE, URLMODULE, QUALITYMODULE;

/**
 * @NApiVersion 2.1
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/runtime', 'N/search', 'N/ui/serverWidget', 'N/url', './qualityModule'], runSuitelet);

function runSuitelet(record, runtime, search, uiserverWidget, url, qualityModule) {
	RECORDMODULE= record;
	RUNTIMEMODULE= runtime;
	SEARCHMODULE= search;
	UISERVERWIDGETMODULE= uiserverWidget;
	URLMODULE= url;
	QUALITYMODULE = qualityModule;
	
	var returnObj = {};
	returnObj['onRequest'] = _onRequest;
	return returnObj;	
}
   
/**
 * Definition of the Suitelet script trigger point.
 *
 * @param {Object} context
 * @param {ServerRequest} context.request - Encapsulation of the incoming request
 * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
 * @Since 2015.2
 */
function _onRequest(context) {
    var method = context.request.method;
    //logRecord(context, 'context');

	log.debug(method);
    if (method == 'GET') {
    	getFunction(context);
    } else if (method == 'POST') {
    	postFunction(context);
    }
}

function getFunction(context) {
	var itemFulfillmentId = context.request.parameters.custparam_mob_itemfulfillment || 0;
	
	// For testing purpose
//	itemFulfillmentId = 35093;
	
  	var form = getFormTemplate(itemFulfillmentId);
  	
  	context.response.writePage(form);
}
 
function getFormTemplate(itemFulfillmentId) {
    var uiList = UISERVERWIDGETMODULE.createList({
        title : 'Item Fulfillment Lines'
    });
    uiList.clientScriptModulePath = 'SuiteScripts/QualityModule/mob_cs_itemfulfillmentlines.js';
    
    var itemFulfillmentRec = RECORDMODULE.load({
    	type: RECORDMODULE.Type.ITEM_FULFILLMENT,
		id: itemFulfillmentId,
		isDynamic: false 
    })
    
    var buttonLabel = 'N/A';
    if (itemFulfillmentId) {
		buttonLabel = itemFulfillmentRec.getValue('tranid');
    }
    
    var itemFulfillmentUrl = URLMODULE.resolveRecord({
    	recordType: 'itemfulfillment',
    	recordId: itemFulfillmentId
    });
    
    // Anonymous code does not work - don't know why.
    // Must use a client script to attach a function to the button
//  var scr = "require([], function() { return;});";
//	var scr = "require([], function() { window.location.assign('" + itemFulfillmentUrl + "');});";
//	var scr = "require([], function() { window.open('" + itemFulfillmentUrl + "');});";
    var button = uiList.addButton({
    	id: 'custpage_mob_button_itemfulfillment',
    	label: buttonLabel,
    	functionName: "openItemFulfillment(" + itemFulfillmentId + ")"
    });
    
    var packingurlcolumn = uiList.addColumn({
    	id: 'columnpackingurl',
    	type: UISERVERWIDGETMODULE.FieldType.URL,
    	label: 'Packing'    	
    });
//    packingurlcolumn.setURL({
//    	url: '/app/site/hosting/scriptlet.nl?script=2280&deploy=1&compid=5028481_SB1&custparam_mob_itemfulfillment=35093&custparam_mob_fulfillmentline=0',
//    	dynamic: true
//    });
//    packingurlcolumn.setURL({
//	    url: '/app/site/hosting/scriptlet.nl?script=2280&deploy=1&compid=5028481_SB1',
//	    dynamic: false
//    });
    var packingSuitletUrl = URLMODULE.resolveScript({
    	scriptId: 'customscript_mob_sl_packinglist',
    	deploymentId: '1'
    });
    packingurlcolumn.setURL({
    	url: packingSuitletUrl,
    	dynamic: false
    });
    uiList.addColumn({
    	id: 'columnfulfillment',
    	type: UISERVERWIDGETMODULE.FieldType.TEXT,
    	label: 'Fulfillment'    	
    });
    uiList.addColumn({
    	id: 'columnline',
    	type: UISERVERWIDGETMODULE.FieldType.TEXT,
    	label: 'Line'    	
    });
    uiList.addColumn({
    	id: 'columnitem',
    	type: UISERVERWIDGETMODULE.FieldType.TEXT,
    	label: 'Item'    	
    });
    uiList.addColumn({
    	id: 'columnitemdescription',
    	type: UISERVERWIDGETMODULE.FieldType.TEXT,
    	label: 'Description'    	
    });
    uiList.addColumn({
    	id: 'columnquantity',
    	type: UISERVERWIDGETMODULE.FieldType.TEXT,
    	label: 'Quantity'    	
    });
    packingurlcolumn.addParamToURL({
    	param: 'custparam_mob_itemfulfillment',
    	value: 'columnfulfillment',
    	dynamic: true
    });
    packingurlcolumn.addParamToURL({
    	param: 'custparam_mob_fulfillmentline',
    	value: 'columnline',
    	dynamic: true
    });
    
    var lineCount = itemFulfillmentRec.getLineCount({
    	sublistId: 'item'
    });
    for (var i = 0 ; i < lineCount ; i++) {
//    	var line = itemFulfillmentRec.getSublistValue({
//    		sublistId: 'item',
//    		fieldId: 'line',
//    		line: i
//    	});
    	var line = parseInt(i, 10).toString();
//    	var fulfillmentLine = itemFulfillmentRec.getSublistValue({
//    		sublistId: 'item',
//    		fieldId: 'line',
//    		line: i
//    	});
//    	fulfillmentLine = parseInt(fulfillmentLine, 10).toString();
    	var itemName = itemFulfillmentRec.getSublistValue({
    		sublistId: 'item',
    		fieldId: 'itemname',
    		line: i
    	});
    	var itemDescription = itemFulfillmentRec.getSublistValue({
    		sublistId: 'item',
    		fieldId: 'itemdescription',
    		line: i
    	});
    	var quantity = itemFulfillmentRec.getSublistValue({
    		sublistId: 'item',
    		fieldId: 'quantity',
    		line: i
    	});
    	quantity = parseInt(quantity, 10).toString();
        uiList.addRow({
        	row: {
        		columnpackingurl: 'PACKING',
        		columnfulfillment: itemFulfillmentId,
        		columnline: line,
        		columnitem: itemName,
        		columnitemdescription: itemDescription,
        		columnquantity: quantity
    		}
        });
//        uiList.addRow({
//        	row: {
//        		columnpackingurl: 'PACKING', 
//        		columnfulfillment: '35093', 
//        		columnline: '0',
//        		columnitem: item,
//        		columnitemdescription: itemDescription,
//        		columnquantity: quantity
//    		}
//        });
    }
	
    return uiList;
}

function logRecord(record, logTitle) {
	var logRecord = JSON.stringify(record);
	var recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);		
	}
}
