var CACHEMODULE, ERRORMODULE, FILEMODULE, FORMATMODULE, RECORDMODULE, RUNTIMEMODULE, SEARCHMODULE, WORKFLOWMODULE, QUALITYMODULE;

/**
 * This script is in charge to generate a temporary Customer_nnn file in Work forlder
 * @NApiVersion 2.1
 * @NScriptType MapReduceScript
 */
define(['N/cache', 'N/error', 'N/file', 'N/format', 'N/record', 'N/runtime', 'N/search', 'N/workflow','./qualityModule'], runMapReduce);

function runMapReduce(cache, error, file, format, record, runtime, search, workflow, qualityModule) {
	CACHEMODULE= cache;
	ERRORMODULE= error;
	FILEMODULE= file;
	FORMATMODULE= format;
	RECORDMODULE= record;
	RUNTIMEMODULE= runtime;
	SEARCHMODULE= search;
    WORKFLOWMODULE= workflow;
    QUALITYMODULE= qualityModule;
	
	let returnObj = {};
	returnObj['getInputData'] = _getInputData;
	returnObj['map'] = _map;
	returnObj['reduce'] = _reduce;
	returnObj['summarize'] = _summarize;
	return returnObj;
}

/**
 * Defines the function that is executed at the beginning of the map/reduce process and generates the input data.
 * @param {Object} inputContext
 * @param {boolean} inputContext.isRestarted - Indicates whether the current invocation of this function is the first
 *     invocation (if true, the current invocation is not the first invocation and this function has been restarted)
 * @param {Object} inputContext.ObjectRef - Object that references the input data
 * @typedef {Object} ObjectRef
 * @property {string|number} ObjectRef.id - Internal ID of the record instance that contains the input data
 * @property {string} ObjectRef.type - Type of the record instance that contains the input data
 * @returns {Array|Object|Search|ObjectRef|File|Query} The input data to use in the map/reduce process
 * @since 2015.2
 */
function _getInputData(inputContext) {	
	log.debug('getInputData started');
	
    let sqlString = `   Select t1.id as resultsetid, 
                            t1.custrecord_mob_resultsetwocompletion as wocompletionid, 
                            t2.custbody_mob_status_qualite as qualitystatus, 
                            t2.custbody_mob_qualityglobalstatus as globalstatus, 
                            t4.custrecord_mob_72htesting, 
                            t5.siloid, 
                            t5.siloname
                        From customrecord_mob_qualityresultset t1 
                            Left Outer Join transaction t2 
                              On t2.id = t1.custrecord_mob_resultsetwocompletion
                            Left Outer Join transactionline t3 
                              On t3.transaction = t2.id 
                              And t3.mainline = 'T' 
                            Left Outer Join subsidiary t4 
                              On t4.id = t3.subsidiary 
                            Left Outer Join (
                                Select distinct t12.id as siloid, t12.name as siloname, t11.custrecord_mob_cmpl_wocompletion 
                                From customrecord_mob_wo_pre_completion t11 
                                    Inner Join customrecord_mob_wo_productionsilo t12 
                                    On t12.id = t11.custrecord_mob_cmpl_siloid
                            ) t5
                              On t5.custrecord_mob_cmpl_wocompletion  = t1.custrecord_mob_resultsetwocompletion `;
    sqlString += ' where t1.id not in (8606, 8605) ';
	log.debug('getInputData done');
	return {
        type: 'suiteql',
        query: sqlString
    };
}

function tagValues(values) {
	// logRecord('values', values);
	const tags = ['resultsetid', 'wocompletionid', 'qualitystatus', 'globalstatus', 'custrecord_mob_72htesting', 'siloid', 'siloname'];
	let taggedValues = {};
	for (let i = 0 ; i < tags.length ; i++) {
		taggedValues[tags[i]] = values[i];
	}
	return taggedValues;
}

/**
 * Defines the function that is executed when the map entry point is triggered. This entry point is triggered automatically
 * when the associated getInputData stage is complete. This function is applied to each key-value pair in the provided
 * context.
 * @param {Object} mapContext - Data collection containing the key-value pairs to process in the map stage. This parameter
 *     is provided automatically based on the results of the getInputData stage.
 * @param {Iterator} mapContext.errors - Serialized errors that were thrown during previous attempts to execute the map
 *     function on the current key-value pair
 * @param {number} mapContext.executionNo - Number of times the map function has been executed on the current key-value
 *     pair
 * @param {boolean} mapContext.isRestarted - Indicates whether the current invocation of this function is the first
 *     invocation (if true, the current invocation is not the first invocation and this function has been restarted)
 * @param {string} mapContext.key - Key to be processed during the map stage
 * @param {string} mapContext.value - Value to be processed during the map stage
 * @since 2015.2
 */
function _map(context) {
	// log.debug('map started');

    let mapValue = JSON.parse(context.value);
    // logRecord(mapValue, 'map value');
    let taggedValues = tagValues(mapValue.values);


    if (!taggedValues.wocompletionid) {
        log.debug({
            title: 'Empty wo completion',
            details: `Quality result set ${taggedValues.resultsetid} has no wo completion`
        });
        return;
    }

    let globalStatus = taggedValues.globalstatus;
    let qualityStatus = taggedValues.qualitystatus;
    let resultSetLocked = [3, 4].includes(globalStatus);
    let h72Testing = taggedValues.custrecord_mob_72htesting == 'T' ? true : false;
    
    RECORDMODULE.submitFields({
        type: 'customrecord_mob_qualityresultset',
        id: taggedValues.resultsetid,
        values: {
            custrecord_mob_resultsetstatus: qualityStatus,
            custrecord_mob_resultsetglobalstatus: globalStatus,
            custrecord_mob_resultsetlocked: resultSetLocked,
            custrecord_mob_resultset72htesting: h72Testing,
            custrecord_mob_resultsetsiloname: taggedValues.siloname
        }
    });

    log.debug({
        title: 'Update',
        details: `QualityResultSet:${taggedValues.resultsetid};WoCompletion:${taggedValues.wocompletionid};Status:${qualityStatus};
            GlobalStatus:${globalStatus};72hTesting:${h72Testing};SiloName:${taggedValues.siloname}`
    });

    if (!resultSetLocked) {
        let validateSimpleActionId = 'workflowaction88460';
        let rejectSimpleActionId = 'workflowaction88459';
        let validateLabActionId = 'workflowaction88463';
        let validateCustActionId = 'workflowaction88462';
        let actionsPath = [];
        switch (qualityStatus) {
            case 2: // rejected simple
                actionsPath.push(rejectSimpleActionId);
                break;
            case 3: // validate simple
                actionsPath.push(validateSimpleActionId);
                break;
            case 4: // passed lab
                actionsPath.push(rejectSimpleActionId);
                actionsPath.push(validateLabActionId);
                break;
            case 5: // passed customer
                actionsPath.push(rejectSimpleActionId);
                actionsPath.push(validateCustActionId);
                break;
        }

        // launch workflow
        WORKFLOWMODULE.initiate({
            recordId: taggedValues.resultsetid,
            recordType: 'customrecord_mob_qualityresultset',
            workflowId: 'customworkflow_mob_outgoingquality',
        });

        actionsPath.forEach(actionId => {
            // log.debug(`Action ${actionId}`);
            WORKFLOWMODULE.trigger({
                recordId: taggedValues.resultsetid,
                recordType: 'customrecord_mob_qualityresultset',
                workflowId: 'customworkflow_mob_outgoingquality',
                actionId: actionId
            });
        });
    }
    if (taggedValues.siloid){
        context.write({
            key: taggedValues.siloid,
            value: taggedValues.resultsetid
        });
    }

	//log.debug('map done');
}

/**
 * Defines the function that is executed when the reduce entry point is triggered. This entry point is triggered
 * automatically when the associated map stage is complete. This function is applied to each group in the provided context.
 * @param {Object} reduceContext - Data collection containing the groups to process in the reduce stage. This parameter is
 *     provided automatically based on the results of the map stage.
 * @param {Iterator} reduceContext.errors - Serialized errors that were thrown during previous attempts to execute the
 *     reduce function on the current group
 * @param {number} reduceContext.execut ionNo - Number of times the reduce function has been executed on the current group
 * @param {boolean} reduceContext.isRestarted - Indicates whether the current invocation of this function is the first
 *     invocation (if true, the current invocation is not the first invocation and this function has been restarted)
 * @param {string} reduceContext.key - Key to be processed during the reduce stage
 * @param {List<String>} reduceContext.values - All values associated with a unique key that was passed to the reduce stage
 *     for processing
 * @since 2015.2
 */
function _reduce(context) {
	// log.debug('reduce started');

    let siloId = context.key;
    let resultSetId = context.values[0]
    // logVar(siloId, 'siloId');
    // logVar(resultSetId, 'resultSetId');

    log.debug({
        title: 'Silo update',
        details: `Silo ${siloId} is affected to result set ${resultSetId}`
    });
    
    RECORDMODULE.submitFields({
        type: 'customrecord_mob_wo_productionsilo',
        id: siloId,
        values: {
            custrecord_mob_cmpl_qualresultset: resultSetId
        }
    });
	
	//log.debug('reduce done');
}

/**
 * Defines the function that is executed when the summarize entry point is triggered. This entry point is triggered
 * automatically when the associated reduce stage is complete. This function is applied to the entire result set.
 * @param {Object} summaryContext - Statistics about the execution of a map/reduce script
 * @param {number} summaryContext.concurrency - Maximum concurrency number when executing parallel tasks for the map/reduce
 *     script
 * @param {Date} summaryContext.dateCreated - The date and time when the map/reduce script began running
 * @param {boolean} summaryContext.isRestarted - Indicates whether the current invocation of this function is the first
 *     invocation (if true, the current invocation is not the first invocation and this function has been restarted)
 * @param {Iterator} summaryContext.output - Serialized keys and values that were saved as output during the reduce stage
 * @param {number} summaryContext.seconds - Total seconds elapsed when running the map/reduce script
 * @param {number} summaryContext.usage - Total number of governance usage units consumed when running the map/reduce
 *     script
 * @param {number} summaryContext.yields - Total number of yields when running the map/reduce script
 * @param {Object} summaryContext.inputSummary - Statistics about the input stage
 * @param {Object} summaryContext.mapSummary - Statistics about the map stage
 * @param {Object} summaryContext.reduceSummary - Statistics about the reduce stage
 * @since 2015.2
 */
function _summarize(summary) {
	log.debug('summary started');
//	log.debug('summary', summary);

	if (writeErrorsToFlatFile(summary, 1) > 0) {
	} else {
        summary.output.iterator().each((key, value) => {
            logRecord(key, 'key');
            logRecord(value, 'value');
            return true;
        });
	}
	
	log.debug('summary done');
}

function appendLineToErrorFile(fileObj, message) {
	if (!fileObj) {
		const scriptParams = retrieveScriptParameters();
		const fileName = `ResultSetMigrationErrors`;
		const folder = errorFolderPath();
		const folderId = retrieveFolderId(folder);
		fileObj = FILEMODULE.create({
			name: fileName,
			fileType: FILEMODULE.Type.PLAINTEXT,
			description: 'Silae Import Errors',
			encoding: FILEMODULE.Encoding.UTF8,
			folder: folderId
		});
	}
	fileObj.appendLine({
		value: message
	});
	return fileObj;
}

function writeErrorsToFlatFile(summary, extractionId, error) {
	let errorCount = 0;
	let fileObj;
	summary.mapSummary.errors.iterator().each(
		function(key, error, executionNo) {
			const errorObj = JSON.parse(error);
			let message;
			if (errorObj.message && errorObj.name && errorObj.name.startsWith('BWP_')) {
				message = errorObj.message;
			} else {
				message = error;
			}
			fileObj = appendLineToErrorFile(fileObj, message);
			errorCount++;
			return true;
		}
	);
	summary.reduceSummary.errors.iterator().each(
		function(key, error, executionNo) {
			const errorObj = JSON.parse(error);
			let message;
			if (errorObj.message && errorObj.name && errorObj.name.startsWith('BWP_')) {
				message = errorObj.message;
			} else {
				message = error;
			}
			fileObj = appendLineToErrorFile(fileObj, message);
			errorCount++;
			return true;
		}
	);
	if (fileObj) {
		fileObj.save();
		log.debug({
			title: 'File created',
			details: `${errorFolderPath()}/${fileObj.name}`
		});
	}
	return errorCount;
}

function retrieveScriptParameters() {
    return;
}

function mainFolderPath() { return 'DataMigration'; }
function workFolderPath() { return `${mainFolderPath()}`; }
function errorFolderPath() { return `${mainFolderPath()}`; }
function processedFolderPath() { return `${mainFolderPath()}`; }

function retrieveFolderId(fullPath) {
	let regex = /\//g;
	let splitResult = fullPath.split(regex);
	let parentFolderId = 0;
	let folderId = 0;
	for (let i = 0 ; i < splitResult.length ; i++) {
		parentFolderId = folderId;
		let folderName = splitResult[i];
		let nameFilter = SEARCHMODULE.createFilter({
			name: 'name',
			operator: SEARCHMODULE.Operator.IS,
			values: [folderName]
		});
		let parentFilter;
		if (parentFolderId > 0) {
			parentFilter = SEARCHMODULE.createFilter({
				name: 'parent',
				operator: SEARCHMODULE.Operator.IS,
				values: [parentFolderId]
			});
		} else {
			parentFilter = SEARCHMODULE.createFilter({
				name: 'parent',
				operator: SEARCHMODULE.Operator.ISEMPTY
			});				
		}
		let folderSearch = SEARCHMODULE.create({
			type: SEARCHMODULE.Type.FOLDER,
			title: 'Search Folder',
			columns: [{
				name: 'internalid'
			}, {
				name: 'parent'
			}],
			filters: [nameFilter, parentFilter]
		});
		let searchResult = folderSearch.run();
		let resultRange = searchResult.getRange({
			start: 0,
			end: 1
		});
		if (resultRange.length == 1) {
			folderId = resultRange[0].getValue({ name: 'internalid'});
		} else {
			folderId = 0;
			break;
		}
	}
	return parseInt(folderId, 10);
}

function logRecord(record, logTitle) {
	let logRecord = JSON.stringify(record) || 'UNDEFINED';
	let recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);		
	}
}

function logVar(value, logTitle) {
	log.debug({
		title: logTitle + ' (' + typeof value + ')',
		details: value 
	});	
}
