var CACHEMODULE, ERRORMODULE, FILEMODULE, FORMATMODULE, RECORDMODULE, RUNTIMEMODULE, SEARCHMODULE, QUALITYMODULE;

/**
 * This script is in charge to generate a temporary Customer_nnn file in Work forlder
 * @NApiVersion 2.1
 * @NScriptType MapReduceScript
 */
define(['N/cache', 'N/error', 'N/file', 'N/format', 'N/record', 'N/runtime', 'N/search', './qualityModule'], runMapReduce);

function runMapReduce(cache, error, file, format, record, runtime, search, qualityModule) {
	CACHEMODULE= cache;
	ERRORMODULE= error;
	FILEMODULE= file;
	FORMATMODULE= format;
	RECORDMODULE= record;
	RUNTIMEMODULE= runtime;
	SEARCHMODULE= search;
    QUALITYMODULE= qualityModule;
	
	let returnObj = {};
	returnObj['getInputData'] = _getInputData;
	returnObj['map'] = _map;
	returnObj['reduce'] = _reduce;
	returnObj['summarize'] = _summarize;
	return returnObj;
}

/**
 * Defines the function that is executed at the beginning of the map/reduce process and generates the input data.
 * @param {Object} inputContext
 * @param {boolean} inputContext.isRestarted - Indicates whether the current invocation of this function is the first
 *     invocation (if true, the current invocation is not the first invocation and this function has been restarted)
 * @param {Object} inputContext.ObjectRef - Object that references the input data
 * @typedef {Object} ObjectRef
 * @property {string|number} ObjectRef.id - Internal ID of the record instance that contains the input data
 * @property {string} ObjectRef.type - Type of the record instance that contains the input data
 * @returns {Array|Object|Search|ObjectRef|File|Query} The input data to use in the map/reduce process
 * @since 2015.2
 */
function _getInputData(inputContext) {	
	log.debug('getInputData started');

	// Read period to provoque an error in case it is not defined and stop the process
    let frozenDate = QUALITYMODULE.retrieveFrozenPeriod().period.endDate;
	logVar(frozenDate, 'frozenDate');

    let searchObj = SEARCHMODULE.create({
        type: 'customrecord_mob_wo_pre_completion',
        // filters: [
        //     ['custrecord_mob_cmpl_siloid.custrecord_mob_cmpl_silowonumber', 'is', 161411]
        // ],
		filters: [
			['custrecord_mob_cmpl_siloid.custrecord_mob_cmpl_wocompletionold', 'noneof', '@NONE@'],
			// 'AND',
			// ['custrecord_mob_cmpl_wonumber.status', 'anyof', 'WorkOrd:D', 'WorkOrd:A', 'WorkOrd:B'], 
			// 'AND', 
			// ['custrecord_mob_cmpl_wonumber.mainline', 'is', 'T'],
			// 'AND',
			// ['custrecord_mob_cmpl_siloid', 'is', 15799],
			// 'AND',
			// ['custrecord_mob_cmpl_siloid.custrecord_mob_cmpl_silowonumber', 'is', 52496]
		],
        columns: [
            'internalid', 'custrecord_mob_cmpl_quantityreceived', 'custrecord_mob_cmpl_lotnumber', 'custrecord_mob_cmpl_bin', 'custrecord_mob_cmpl_lotstatus', 
            'custrecord_mob_cmpl_wocompletion', 
            'custrecord_mob_cmpl_siloid', 'custrecord_mob_cmpl_siloid.custrecord_mob_cmpl_wocompletionold'
        ]
    });
	
	log.debug('getInputData done');
	return searchObj;
}

/**
 * Defines the function that is executed when the map entry point is triggered. This entry point is triggered automatically
 * when the associated getInputData stage is complete. This function is applied to each key-value pair in the provided
 * context.
 * @param {Object} mapContext - Data collection containing the key-value pairs to process in the map stage. This parameter
 *     is provided automatically based on the results of the getInputData stage.
 * @param {Iterator} mapContext.errors - Serialized errors that were thrown during previous attempts to execute the map
 *     function on the current key-value pair
 * @param {number} mapContext.executionNo - Number of times the map function has been executed on the current key-value
 *     pair
 * @param {boolean} mapContext.isRestarted - Indicates whether the current invocation of this function is the first
 *     invocation (if true, the current invocation is not the first invocation and this function has been restarted)
 * @param {string} mapContext.key - Key to be processed during the map stage
 * @param {string} mapContext.value - Value to be processed during the map stage
 * @since 2015.2
 */
function _map(context) {
	// log.debug('map started');

    let mapValue = JSON.parse(context.value);
    // logRecord(mapValue, 'map value');
    let values = mapValue.values;
    // logRecord(values, 'values');

    if (!values['custrecord_mob_cmpl_wocompletionold.custrecord_mob_cmpl_siloid']) { return; }
    if (!values.custrecord_mob_cmpl_siloid) { return; }
	// Don't process not received pre completions : there is no wo completion to create for them
    if (!values.custrecord_mob_cmpl_quantityreceived) { return; }
	// Don't process already migrated data
	if (values.custrecord_mob_cmpl_wocompletion) { return; }

    // logRecord(values, 'values');

    let qtyReceived = parseInt(values.custrecord_mob_cmpl_quantityreceived, 10);    
    let siloWoCompletionId = values['custrecord_mob_cmpl_wocompletionold.custrecord_mob_cmpl_siloid'].value;
	let siloWoCompletionText = values['custrecord_mob_cmpl_wocompletionold.custrecord_mob_cmpl_siloid'].text;
    let siloId = values.custrecord_mob_cmpl_siloid.value;

	let inventory = values.custrecord_mob_cmpl_lotnumber;

	let binId;
	if (values.custrecord_mob_cmpl_bin) {
		binId = values.custrecord_mob_cmpl_bin.value;
	}

	let statusId;
	if (values.custrecord_mob_cmpl_lotstatus) {
		statusId = values.custrecord_mob_cmpl_lotstatus.value;
	}

    let woCompletion;
    if (values.custrecord_mob_cmpl_wocompletion) {
        woCompletion = values.custrecord_mob_cmpl_wocompletion.value;
    }

    let reduceValue = {
        preCompletionId: values.internalid.value,
        qtyReceived: qtyReceived,
		inventory: inventory,
		binId: binId,
		statusId: statusId,
        woCompletion: woCompletion,
        siloId: siloId,
        siloWoCompletionId: siloWoCompletionId,
		siloWoCompletionText: siloWoCompletionText
    }

    context.write({
        key: `${siloWoCompletionId};${siloWoCompletionText};${siloId}`,
        value: reduceValue
    });

	//log.debug('map done');
}

/**
 * Defines the function that is executed when the reduce entry point is triggered. This entry point is triggered
 * automatically when the associated map stage is complete. This function is applied to each group in the provided context.
 * @param {Object} reduceContext - Data collection containing the groups to process in the reduce stage. This parameter is
 *     provided automatically based on the results of the map stage.
 * @param {Iterator} reduceContext.errors - Serialized errors that were thrown during previous attempts to execute the
 *     reduce function on the current group
 * @param {number} reduceContext.execut ionNo - Number of times the reduce function has been executed on the current group
 * @param {boolean} reduceContext.isRestarted - Indicates whether the current invocation of this function is the first
 *     invocation (if true, the current invocation is not the first invocation and this function has been restarted)
 * @param {string} reduceContext.key - Key to be processed during the reduce stage
 * @param {List<String>} reduceContext.values - All values associated with a unique key that was passed to the reduce stage
 *     for processing
 * @since 2015.2
 */
function _reduce(context) {
	// log.debug('reduce started');

    // logRecord(context.key, 'reduce key');
	log.debug({
		title: 'reduce',
		details: `${context.key} ; ${context.values.length}`
	});
	let keys = context.key.split(';');
    let siloWoCompletionId = keys[0];
	let siloWoCompletionText = keys[1];
	let siloId = keys[2];

    let woCompletionRecord = RECORDMODULE.load({
        type: RECORDMODULE.Type.WORK_ORDER_COMPLETION,
        id: siloWoCompletionId,
        isDynamic: true
    });
	let workOrderId = woCompletionRecord.getValue({ fieldId : 'createdfrom' });
	let originalQuantity = woCompletionRecord.getValue({ fieldId : 'completedquantity' });
    let trandate = woCompletionRecord.getValue({ fieldId : 'trandate' });
	let trandateFormatted = FORMATMODULE.format({ 
		value: trandate,
		type: FORMATMODULE.Type.DATE
	});
    let frozenDate = QUALITYMODULE.retrieveFrozenPeriod().period.endDate;
    let splitModeIsOn = (trandate > frozenDate);
    // logVar(trandate, 'trandate');
    // logVar(frozenDate, 'frozenDate');
    // logVar(splitModeIsOn, 'splitModeIsOn');

	let precompletions = [];
	for (let element of context.values) {
		precompletions.push(JSON.parse(element));
	}
	// logVar(precompletions, 'precompletions');

	let fieldsValues = SEARCHMODULE.lookupFields({
		type: SEARCHMODULE.Type.WORK_ORDER,
		id: workOrderId,
		columns: ['status', 'built']
	});
	// logVar(fieldsValues, 'fieldsValues');
	let woBuiltQty = fieldsValues.built;

	// In case splitMode is On, check work order is not closed - don't split wo completions on closed work order: 
	// creation of new wo completions is not authorized
	if (splitModeIsOn) {
		let woStatus;

		if ('status' in  fieldsValues && fieldsValues.status.length >0) {
			woStatus = fieldsValues.status[0].value;
		}
		if ((woStatus || 'closed') == 'closed') {
			// logVar(workOrderId, 'workOrderId');
			// logVar(fieldsValues, 'fieldsValues');
			// log.debug('WO is closed --> set split off');
			splitModeIsOn = false;
		}
	}

	// Control quantities - don't split wo completion when quantity does not match sum of pre completion quantities
	// (it would affect built quantity on WO)
	if (splitModeIsOn) {
		let qtyReceived = precompletions.reduce((x,y) =>  x + parseInt(y.qtyReceived, 10), 0);
		// logVar(qtyReceived, 'qtyReceived');
		// logVar(originalQuantity, 'originalQuantity');
		if (originalQuantity != qtyReceived) {
			let message = `WO completion ${siloWoCompletionText} (${siloWoCompletionId}) has a quantity of ${originalQuantity} ; sum of pre completions on silo ${siloId} is ${qtyReceived}`; 
			log.error({
				title: 'Quantities mismatch',
				details: message
			});
			throw message;
		}
	}

	let firstLine = false;

	let firstLineValues;
    for (let preCompletionValues of precompletions) {
        // let preCompletionValues = JSON.parse(element);
		let woCompletionId = siloWoCompletionId;
        // let value = JSON.parse(context.values[i]);
        // logRecord(preCompletionValues, 'reduce value');
		// woCompletion not empty ==> this line was already processed
		if (preCompletionValues.woCompletion) { continue; }

        if (splitModeIsOn) {
            // log.debug('Split wo completion');
			if (!firstLineValues) {
				firstLine = true;
				firstLineValues = preCompletionValues;
				// woCompletionId = updateWOCompletion(woCompletionRecord, preCompletionValues);
				// log.debug({
				// 	title: 'WO Completion Update',
				// 	details: `Update;WoCompletionFrom:${siloWoCompletionId};WoCompletionTo:${woCompletionId};TranDate:${trandateFormatted};
				// 	Silo:${preCompletionValues.siloId};
				// 	Precompletion:${preCompletionValues.preCompletionId};OriginalQty:${originalQuantity};NewQty${preCompletionValues.qtyReceived};
				// 	Bin:${preCompletionValues.binId};Status:${preCompletionValues.statusId}`
				// });
			} else {
				try {
					woCompletionId = splitWoCompletion(workOrderId, preCompletionValues, woCompletionRecord);
					log.debug({
						title: 'WO Completion Created',
						details: `Split;WoCompletionFrom:${siloWoCompletionText} (${siloWoCompletionId});WoCompletionTo:${woCompletionId};TranDate:${trandateFormatted};
						Silo:${preCompletionValues.siloId};
						Precompletion:${preCompletionValues.preCompletionId};OriginalQty:${originalQuantity};NewQty${preCompletionValues.qtyReceived};
						Bin:${preCompletionValues.binId};Status:${preCompletionValues.statusId}`
					});
				} catch (ex) {
					log.error({
						title: 'Split error',
						details: `Failed to split wo completion ${siloWoCompletionText} (${siloWoCompletionId}) for pre completion ${preCompletionValues.preCompletionId} ; 
							work order ${workOrderId} has an original built quantity of ${woBuiltQty}`
					});
					throw ex;
				}
			}
        } else {
			log.debug({
				title: 'WO Completion migrated',
				details: `Migration;WoCompletionFrom:${siloWoCompletionText} (${siloWoCompletionId});WoCompletionTo:${woCompletionId};TranDate:${trandateFormatted};
				Silo:${preCompletionValues.siloId};
				Precompletion:${preCompletionValues.preCompletionId};OriginalQty:${originalQuantity};NewQty${preCompletionValues.qtyReceived};
				Bin:${preCompletionValues.binId};Status:${preCompletionValues.statusId}`
			});
		}
		if (!firstLine) {
			RECORDMODULE.submitFields({
				type: 'customrecord_mob_wo_pre_completion',
				id: preCompletionValues.preCompletionId,
				values: {
					custrecord_mob_cmpl_wocompletion: woCompletionId
				}
			});
		}
		firstLine = false;
    }
	
	if (firstLineValues){
		try {
			let woCompletionId = updateWOCompletion(woCompletionRecord, firstLineValues);
			log.debug({
				title: 'WO Completion Update',
				details: `Update;WoCompletionFrom:${siloWoCompletionText} (${siloWoCompletionId});WoCompletionTo:${woCompletionId};TranDate:${trandateFormatted};
				Silo:${firstLineValues.siloId};
				Precompletion:${firstLineValues.preCompletionId};OriginalQty:${originalQuantity};NewQty${firstLineValues.qtyReceived};
				Bin:${firstLineValues.binId};Status:${firstLineValues.statusId}`
			});
			RECORDMODULE.submitFields({
				type: 'customrecord_mob_wo_pre_completion',
				id: firstLineValues.preCompletionId,
				values: {
					custrecord_mob_cmpl_wocompletion: woCompletionId
				}
			});
		} catch (ex) {			
			log.error({
				title: 'Update error',
				details: `UpdateFailure;WoCompletionFrom:${siloWoCompletionText} (${siloWoCompletionId});WoCompletionTo:${siloWoCompletionId};TranDate:${trandateFormatted};
				Silo:${firstLineValues.siloId};
				Precompletion:${firstLineValues.preCompletionId};OriginalQty:${originalQuantity};NewQty${firstLineValues.qtyReceived};
				Bin:${firstLineValues.binId};Status:${firstLineValues.statusId}`
			});
			throw ex;
		}
	}

	// Empty wo completion on silo record
	RECORDMODULE.submitFields({
		type: 'customrecord_mob_wo_productionsilo',
		id: siloId,
		values: {
			custrecord_mob_cmpl_wocompletionold: ''
		}
	});
	
	//log.debug('reduce done');
}

/**
 * Defines the function that is executed when the summarize entry point is triggered. This entry point is triggered
 * automatically when the associated reduce stage is complete. This function is applied to the entire result set.
 * @param {Object} summaryContext - Statistics about the execution of a map/reduce script
 * @param {number} summaryContext.concurrency - Maximum concurrency number when executing parallel tasks for the map/reduce
 *     script
 * @param {Date} summaryContext.dateCreated - The date and time when the map/reduce script began running
 * @param {boolean} summaryContext.isRestarted - Indicates whether the current invocation of this function is the first
 *     invocation (if true, the current invocation is not the first invocation and this function has been restarted)
 * @param {Iterator} summaryContext.output - Serialized keys and values that were saved as output during the reduce stage
 * @param {number} summaryContext.seconds - Total seconds elapsed when running the map/reduce script
 * @param {number} summaryContext.usage - Total number of governance usage units consumed when running the map/reduce
 *     script
 * @param {number} summaryContext.yields - Total number of yields when running the map/reduce script
 * @param {Object} summaryContext.inputSummary - Statistics about the input stage
 * @param {Object} summaryContext.mapSummary - Statistics about the map stage
 * @param {Object} summaryContext.reduceSummary - Statistics about the reduce stage
 * @since 2015.2
 */
function _summarize(summary) {
	log.debug('summary started');
//	log.debug('summary', summary);

	if (writeErrorsToFlatFile(summary, 1) > 0) {
	} else {
        summary.output.iterator().each((key, value) => {
            logRecord(key, 'key');
            logRecord(value, 'value');
            return true;
        });
	}
	
	log.debug('summary done');
}

function updateWOCompletion(woCompletionRecord, preCompletionValues) {
	let woCompletionId = '';

	woCompletionRecord.setValue({
		fieldId: 'completedquantity',
		value: preCompletionValues.qtyReceived
	});
	let inventoryDetailSubRec = woCompletionRecord.getSubrecord({
		fieldId: 'inventorydetail'
	});
	
	// Delete existing inventory assignments
	let lineCount = inventoryDetailSubRec.getLineCount({
		sublistId: 'inventoryassignment'
	});
	for (let i = 0 ; i < lineCount ; i++) {
		inventoryDetailSubRec.removeLine({
			sublistId: 'inventoryassignment',
			line: 0				
		});
	}
	
	inventoryDetailSubRec.selectNewLine({
		sublistId: 'inventoryassignment'
	});
	inventoryDetailSubRec.setCurrentSublistValue({
		sublistId: 'inventoryassignment',
		fieldId: 'receiptinventorynumber',
		value: preCompletionValues.inventory
	});
	if (preCompletionValues.binId) {
		inventoryDetailSubRec.setCurrentSublistValue({
			sublistId: 'inventoryassignment',
			fieldId: 'binnumber',
			value: preCompletionValues.binId
		});					
	}
	if (preCompletionValues.statusId) {
		inventoryDetailSubRec.setCurrentSublistValue({
			sublistId: 'inventoryassignment',
			fieldId: 'inventorystatus',
			value: preCompletionValues.statusId
		});					
	}
	inventoryDetailSubRec.setCurrentSublistValue({
		sublistId: 'inventoryassignment',
		fieldId: 'quantity',
		value: preCompletionValues.qtyReceived
	});
	inventoryDetailSubRec.commitLine({
		sublistId: 'inventoryassignment'
	});

	woCompletionId = woCompletionRecord.save();

	return woCompletionId;
}

function splitWoCompletion(workOrderId, preCompletionValues, originalWoCompletionRecord) {	
	let woCompletionRecord = RECORDMODULE.transform({
		fromType: RECORDMODULE.Type.WORK_ORDER,
		fromId: workOrderId,
		toType: RECORDMODULE.Type.WORK_ORDER_COMPLETION,
		isDynamic: true,
	});

	copyFromOriginal(woCompletionRecord, originalWoCompletionRecord, 'trandate');
	copyFromOriginal(woCompletionRecord, originalWoCompletionRecord, 'startoperation');
	copyFromOriginal(woCompletionRecord, originalWoCompletionRecord, 'endoperation');

	return updateWOCompletion(woCompletionRecord, preCompletionValues);
}

function copyFromOriginal(woCompletionRecord, originalWoCompletionRecord, fieldId) {
	woCompletionRecord.setValue({
		fieldId: fieldId,
		value: originalWoCompletionRecord.getValue({ fieldId: fieldId })
	});
}

function appendLineToErrorFile(fileObj, message) {
	if (!fileObj) {
		const scriptParams = retrieveScriptParameters();
		const fileName = `PreCompletionMigrationErrors`;
		const folder = errorFolderPath();
		const folderId = retrieveFolderId(folder);
		fileObj = FILEMODULE.create({
			name: fileName,
			fileType: FILEMODULE.Type.PLAINTEXT,
			description: 'Silae Import Errors',
			encoding: FILEMODULE.Encoding.UTF8,
			folder: folderId
		});
	}
	fileObj.appendLine({
		value: message
	});
	return fileObj;
}

function writeErrorsToFlatFile(summary, extractionId, error) {
	let errorCount = 0;
	let fileObj;
	summary.mapSummary.errors.iterator().each(
		function(key, error, executionNo) {
			const errorObj = JSON.parse(error);
			let message;
			if (errorObj.message && errorObj.name && errorObj.name.startsWith('BWP_')) {
				message = errorObj.message;
			} else {
				message = error;
			}
			fileObj = appendLineToErrorFile(fileObj, message);
			errorCount++;
			return true;
		}
	);
	summary.reduceSummary.errors.iterator().each(
		function(key, error, executionNo) {
			const errorObj = JSON.parse(error);
			let message;
			if (errorObj.message && errorObj.name && errorObj.name.startsWith('BWP_')) {
				message = errorObj.message;
			} else {
				message = error;
			}
			fileObj = appendLineToErrorFile(fileObj, message);
			errorCount++;
			return true;
		}
	);
	if (fileObj) {
		fileObj.save();
		log.debug({
			title: 'File created',
			details: `${errorFolderPath()}/${fileObj.name}`
		});
	}
	return errorCount;
}

function retrieveScriptParameters() {
    return;
}

function mainFolderPath() { return 'DataMigration'; }
function workFolderPath() { return `${mainFolderPath()}`; }
function errorFolderPath() { return `${mainFolderPath()}`; }
function processedFolderPath() { return `${mainFolderPath()}`; }

function retrieveFolderId(fullPath) {
	let regex = /\//g;
	let splitResult = fullPath.split(regex);
	let parentFolderId = 0;
	let folderId = 0;
	for (let i = 0 ; i < splitResult.length ; i++) {
		parentFolderId = folderId;
		let folderName = splitResult[i];
		let nameFilter = SEARCHMODULE.createFilter({
			name: 'name',
			operator: SEARCHMODULE.Operator.IS,
			values: [folderName]
		});
		let parentFilter;
		if (parentFolderId > 0) {
			parentFilter = SEARCHMODULE.createFilter({
				name: 'parent',
				operator: SEARCHMODULE.Operator.IS,
				values: [parentFolderId]
			});
		} else {
			parentFilter = SEARCHMODULE.createFilter({
				name: 'parent',
				operator: SEARCHMODULE.Operator.ISEMPTY
			});				
		}
		let folderSearch = SEARCHMODULE.create({
			type: SEARCHMODULE.Type.FOLDER,
			title: 'Search Folder',
			columns: [{
				name: 'internalid'
			}, {
				name: 'parent'
			}],
			filters: [nameFilter, parentFilter]
		});
		let searchResult = folderSearch.run();
		let resultRange = searchResult.getRange({
			start: 0,
			end: 1
		});
		if (resultRange.length == 1) {
			folderId = resultRange[0].getValue({ name: 'internalid'});
		} else {
			folderId = 0;
			break;
		}
	}
	return parseInt(folderId, 10);
}

function logRecord(record, logTitle) {
	let logRecord = JSON.stringify(record) || 'UNDEFINED';
	let recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);		
	}
}

function logVar(value, logTitle) {
	log.debug({
		title: logTitle + ' (' + typeof value + ')',
		details: value 
	});	
}
