var SEARCHMODULE, TRANSLATIONMODULE, UIDIALOGMODULE;
// Status can be changed manually only in reverse mode and only one step at a time. The folowwing changes are allowed
// - PassedSimple->WaitingLaboratory
// - RejectedSimple->WaitingLaboratory
// - PassedClient->RejectedSimple
// - PassedLaboratory->RejectedSimple
// - RejectedLaboratory->RejectedSimple
// - Passed72h->PassedSimple
// - Passed72h->PassedClient
// - Passed72h->PassedLaboratory
// - Passed72h->WaitingLaboratory
// - Rejected72h->PassedSimple
// - Rejected72h->PassedClient
// - Rejected72h->PassedLaboratory
// - Rejected72h->WaitingLaboratory
var AUTHORIZEDSTATUSCHANGESIMPLE = ['3->1', '2->1', '5->2', '4->2', '6->2', '8->3', '8->5', '8->4', '8->1', '7->3', '7->5', '7->4', '7->1'];

/**
 * @NApiVersion 2.1
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */
define(['N/search', 'N/translation', 'N/ui/dialog'], runClient);

function runClient(search, translation, uidialog) {
	SEARCHMODULE= search;
	TRANSLATIONMODULE = translation;
	UIDIALOGMODULE= uidialog;
	
	var returnObj = {};
	returnObj['pageInit'] = _pageInit;
//	returnObj['fieldChanged'] = _fieldChanged;
//	returnObj['postSourcing'] = _postSourcing;
//	returnObj['sublistChanged'] = _sublistChanged;
//	returnObj['lineInit'] = _lineInit;
	returnObj['validateField'] = _validateField;
//	returnObj['validateLine'] = _validateLine;
//	returnObj['validateInsert'] = _validateInsert;
//	returnObj['validateDelete'] = _validateDelete;
//	returnObj['saveRecord'] = _saveRecord;
//	returnObj['openResultSet'] = openResultSetCall;
	return returnObj;
}
    
/**
 * Function to be executed after page is initialized.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.mode - The mode in which the record is being accessed (create, copy, or edit)
 *
 * @since 2015.2
 */
function _pageInit(scriptContext) {
	let locked = scriptContext.currentRecord.getValue('custrecord_mob_resultsetlocked');
	if (locked) {
		scriptContext.currentRecord.getField({
			fieldId: 'custrecord_mob_resultsetstatus'
		}).isDisabled = true;
	}
}

/**
 * Function to be executed when field is changed.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 * @param {string} scriptContext.fieldId - Field name
 * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
 * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
 *
 * @since 2015.2
 */
function _fieldChanged(scriptContext) {
	
}

/**
 * Function to be executed when field is slaved.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 * @param {string} scriptContext.fieldId - Field name
 *
 * @since 2015.2
 */
function _postSourcing(scriptContext) {

}

/**
 * Function to be executed after sublist is inserted, removed, or edited.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @since 2015.2
 */
function _sublistChanged(scriptContext) {

}

/**
 * Function to be executed after line is selected.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @since 2015.2
 */
function _lineInit(scriptContext) {

}

/**
 * Validation function to be executed when field is changed.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 * @param {string} scriptContext.fieldId - Field name
 * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
 * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
 *
 * @returns {boolean} Return true if field is valid
 *
 * @since 2015.2
 */
function _validateField(scriptContext) {
	if (scriptContext.fieldId == 'custrecord_mob_resultsetstatus') {
        // TODO: use a dynamic field (created by code) to store originalstatus
		let previousStatus = scriptContext.currentRecord.getValue('custpage_mob_originalstatus');
		let actualStatus = scriptContext.currentRecord.getValue('custrecord_mob_resultsetstatus');
		if (previousStatus == actualStatus) {
			return true;
		}
		let flow = previousStatus.trim() + '->' + actualStatus.trim();
		if (AUTHORIZEDSTATUSCHANGESIMPLE.indexOf(flow) < 0) {
			let searchObj = SEARCHMODULE.create({
				type: 'customlist_mob_statuts_qualite',
				columns: ['internalid', 'name'],
				filters: [SEARCHMODULE.createFilter({
					name: 'internalid',
					operator: SEARCHMODULE.Operator.ANYOF,
					values: [previousStatus, actualStatus]
				})]
			});
			let searchResults = searchObj.run().getRange({ start: 0, end: 2});
			let previousStatusValue = '';
			let actualStatusValue = '';
			for (let i = 0 ; i < searchResults.length ; i++) {
				let result = searchResults[i];
				switch (result.getValue('internalid')) {
					case previousStatus:
						previousStatusValue = result.getValue('name');
						break;
					case actualStatus:
						actualStatusValue = result.getValue('name');
						break;
				}
			}
			let message = TRANSLATIONMODULE.get({collection: 'custcollection_mob_outgoingquality', key: 'STATUS_CHANGE_ERROR_MSG'})() + ' ' + previousStatusValue + " -> " + actualStatusValue;
			UIDIALOGMODULE.alert({
				title: TRANSLATIONMODULE.get({collection: 'custcollection_mob_outgoingquality', key: 'STATUS_CHANGE_ERROR_TITLE'})(),
				message: message
			});
			scriptContext.currentRecord.setValue({
				fieldId: 'custbody_mob_status_qualite',
				value: previousStatus
			});
			return false;
		}
	}
	return true;
}

/**
 * Validation function to be executed when sublist line is committed.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @returns {boolean} Return true if sublist line is valid
 *
 * @since 2015.2
 */
function _validateLine(scriptContext) {

}

/**
 * Validation function to be executed when sublist line is inserted.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @returns {boolean} Return true if sublist line is valid
 *
 * @since 2015.2
 */
function _validateInsert(scriptContext) {

}

/**
 * Validation function to be executed when record is deleted.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @returns {boolean} Return true if sublist line is valid
 *
 * @since 2015.2
 */
function _validateDelete(scriptContext) {

}

/**
 * Validation function to be executed when record is saved.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @returns {boolean} Return true if record is valid
 *
 * @since 2015.2
 */
function _saveRecord(scriptContext) {

}

function openResultSetCall(resultSetId) {
	// N/redirect is not available for client scriot
	REDIRECTMODULE.toRecord({
		id: resultSetId,
		type: 'customrecord_mob_qualityresultset'
	});
}
