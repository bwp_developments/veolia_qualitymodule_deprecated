var ERRORMODULE, FORMATMODULE, RECORDMODULE, RUNTIMEMODULE, SEARCHMODULE, URLMODULE;

/**
 * qualityModule.js
 * @NApiVersion 2.1
 * @NModuleScope Public
 */
define(['N/error', 'N/format', 'N/record', 'N/runtime', 'N/search', 'N/url'], runModule);

function runModule(error, format, record, runtime, search, url) {
	ERRORMODULE= error;
	FORMATMODULE= format;
	RECORDMODULE= record;
	RUNTIMEMODULE= runtime;
	SEARCHMODULE= search;
	URLMODULE= url;
	
	let returnObj = {};
	returnObj['getQualitySetup'] = getQualitySetupCall;
	returnObj['getToleranceSet'] = getToleranceSetCall;
	returnObj['completeToleranceSet'] = completeToleranceSetCall;
	returnObj['initializeResultSet'] = initializeResultSetCall;
//	returnObj['refreshResultSet'] = refreshResultSetCall;
	returnObj['finalizeResultSet'] = finalizeResultSetCall;
	returnObj['retrieveFrozenPeriod'] = retrieveFrozenPeriodCall;
	return returnObj;    
}

/**
 * 
 * @returns {object} Return object with the following properties
 * - outgoingQualityFlag {boolean}
 * - customerLevelFlag {boolean}
 * - multiSiloFlag {boolean}
 * - siloCapacityDefaultValue {integer}
 */
function getQualitySetupCall(subsidiary) {
	let qualitySetup = {outgoingQualityFlag: false, customerLevelFlag: false, multiSiloFlag: false, siloCapacityDefaultValue: 0};

	if (arguments.length == 0) {
		// Get user's subsidiary
		subsidiary = RUNTIMEMODULE.getCurrentUser().subsidiary;
	}
	subsidiary = parseInt(subsidiary || 0, 10);
	if (subsidiary > 0) {
		let fieldsValues = SEARCHMODULE.lookupFields({
			type: SEARCHMODULE.Type.SUBSIDIARY,
			id: subsidiary,
			columns: ['custrecord_mob_outgoingquality', 'custrecord_mob_toleranceatcustomerlevel', 'custrecord_mob_multisilo', 
				'custrecord_mob_silocapacity', 'custrecord_mob_72htesting']
		});
		// logRecord(fieldsValues, 'fieldsValues');
		qualitySetup.outgoingQualityFlag = fieldsValues['custrecord_mob_outgoingquality'];
		qualitySetup.customerLevelFlag = fieldsValues['custrecord_mob_toleranceatcustomerlevel'];
		qualitySetup.multiSiloFlag = fieldsValues['custrecord_mob_multisilo'];
		qualitySetup.siloCapacityDefaultValue = parseInt(fieldsValues['custrecord_mob_silocapacity'] || 0, 10);
		qualitySetup.h72testing = fieldsValues['custrecord_mob_72htesting'];
	}
	return qualitySetup;
}

/**
 * Creates a tolerance set record and all tolerance lines (one per available test)
 * @param itemId
 * @param customerId
 */
function getToleranceSetCall(itemId, customerId) {
	itemId = parseInt(itemId || 0, 10);
	customerId = parseInt(customerId || 0, 10);
	
	let toleranceSetId = retrieveToleranceSet(itemId, customerId);
	if (toleranceSetId == 0) {
		toleranceSetId = initializeToleranceSet(itemId, customerId);
	}
	
	return toleranceSetId;
}

function initializeToleranceSet(itemId, customerId) {	
	// Creation of the tolerance set record
	let toleranceSetRec = RECORDMODULE.create({
		type: 'customrecord_mob_qm_toleranceset'
	});
	toleranceSetRec.setValue({
		fieldId: 'custrecord_mob_tolerancesetitem',
		value: itemId
	});
	if (customerId != 0) {
		toleranceSetRec.setValue({
			fieldId: 'custrecord_mob_tolerancesetcustomer',
			value: customerId
		});		
	}
	let toleranceSetId = toleranceSetRec.save();
	
	// Creation of one tolerance record per test
	let searchObj = SEARCHMODULE.create({
		type: 'customrecord_mob_qm_testsetup',
		columns: ['internalid', 'custrecord_mob_testsetup_defaultnorm', 'custrecord_mob_testsetup_defaultopmode', 
		          'custrecord_mob_testsetup_unit', 'custrecord_mob_testsetup_displaysequence'],
  		filters: [SEARCHMODULE.createFilter({
			name: 'isinactive',
			operator: SEARCHMODULE.Operator.IS,
			values: false
		})]
	});
	searchObj.run().each(function(qualityTest) {
		let toleranceRec = RECORDMODULE.create({
			type: 'customrecord_mob_qm_tolerance'
		});
		toleranceRec.setValue({
			fieldId: 'custrecord_mob_toleranceset',
			value: toleranceSetId
		});
		toleranceRec.setValue({
			fieldId: 'custrecord_mob_tolerancetest',
			value: qualityTest.getValue('internalid')
		});
		toleranceRec.setValue({
			fieldId: 'custrecord_mob_tolerancenorm',
			value: qualityTest.getValue('custrecord_mob_testsetup_defaultnorm')
		});
		toleranceRec.setValue({
			fieldId: 'custrecord_mob_toleranceoperatingmode',
			value: qualityTest.getValue('custrecord_mob_testsetup_defaultopmode')
		});
		toleranceRec.setValue({
			fieldId: 'custrecord_mob_toleranceunit',
			value: qualityTest.getValue('custrecord_mob_testsetup_unit')
		});
		toleranceRec.setValue({
			fieldId: 'custrecord_mob_tolerancedisplaysequence',
			value: qualityTest.getValue('custrecord_mob_testsetup_displaysequence')
		});
		toleranceRec.save();
//		try {
//			toleranceRec.save();
//		} catch (ex) {
//			logRecord(qualityTest, 'qualityTest');
//			throw ex;
//		}
		return true;
	});
	
	return toleranceSetId;	
}

function completeToleranceSetCall(toleranceSetId) {
	// Get the list of used tests
	let toleranceSearchObj = SEARCHMODULE.create({
		type: 'customrecord_mob_qm_tolerance',
		columns: ['internalid', 'custrecord_mob_tolerancetest'],
		filters: [['custrecord_mob_toleranceset', 'is', toleranceSetId]]
	});
	let toleranceTestIds = [];
	toleranceSearchObj.run().each(function(result) {
		toleranceTestIds.push(result.getValue('custrecord_mob_tolerancetest'));
		return true;
	});
	
	// Creation of one tolerance record per test that is not already used
	let searchObj = SEARCHMODULE.create({
		type: 'customrecord_mob_qm_testsetup',
		columns: ['internalid', 'custrecord_mob_testsetup_defaultnorm', 'custrecord_mob_testsetup_defaultopmode', 
		          'custrecord_mob_testsetup_unit', 'custrecord_mob_testsetup_displaysequence'],
		filters: [SEARCHMODULE.createFilter({
			name: 'isinactive',
			operator: SEARCHMODULE.Operator.IS,
			values: false
			})]
	});
	
	searchObj.run().each(function(qualityTest) {
		let qualityTestId = qualityTest.getValue('internalid');
		try {
			if (toleranceTestIds.indexOf(qualityTestId) < 0){
				let toleranceRec = RECORDMODULE.create({
					type: 'customrecord_mob_qm_tolerance'
				});
				toleranceRec.setValue({
					fieldId: 'custrecord_mob_toleranceset',
					value: toleranceSetId
				});
				toleranceRec.setValue({
					fieldId: 'custrecord_mob_tolerancetest',
					value: qualityTestId
				});
				toleranceRec.setValue({
					fieldId: 'custrecord_mob_tolerancenorm',
					value: qualityTest.getValue('custrecord_mob_testsetup_defaultnorm')
				});
				toleranceRec.setValue({
					fieldId: 'custrecord_mob_toleranceoperatingmode',
					value: qualityTest.getValue('custrecord_mob_testsetup_defaultopmode')
				});
				toleranceRec.setValue({
					fieldId: 'custrecord_mob_toleranceunit',
					value: qualityTest.getValue('custrecord_mob_testsetup_unit')
				});
				toleranceRec.setValue({
					fieldId: 'custrecord_mob_tolerancedisplaysequence',
					value: qualityTest.getValue('custrecord_mob_testsetup_displaysequence')
				});
				toleranceRec.save();				
			}		
		} catch (ex) {
			alert(ex);
		}
		
		return true;
	});
}

function initializeResultSetCall(itemId, customerId, woNumber, subsidiary, silo, siloName, qualityResultSet) {
	log.debug('initializeResultSetCall started');
	itemId = parseInt(itemId || 0, 10);
	customerId = parseInt(customerId || 0, 10);
	silo = parseInt(silo || 0, 10);
	woNumber = parseInt(woNumber || 0, 10);
	qualityResultSet = parseInt(qualityResultSet || 0, 10);
	
	let returnValue;
	
	// Exit if any mandatory parameter is missing
	if (itemId * silo * woNumber == 0) {
		log.debug('QM invalid parameters');
		return 0;
	}

	let qualitySetup = getQualitySetupCall(subsidiary);
	
	// If Quality Result Set is already set, update it
	if (qualityResultSet > 0) {
		let qualityFieldsValues = SEARCHMODULE.lookupFields({
			type: 'customrecord_mob_qualityresultset',
			id: qualityResultSet,
			columns: ['custrecord_mob_resultsetcustomer']
		});

		if ('custrecord_mob_resultsetcustomer' in qualityFieldsValues) {
			let qualityCustomerId = 0;
			if (qualityFieldsValues.custrecord_mob_resultsetcustomer.length > 0) {
				qualityCustomerId = qualityFieldsValues.custrecord_mob_resultsetcustomer[0].value;				
			}
			if (qualitySetup.customerLevelFlag && qualityCustomerId != customerId) {
				log.debug('new customer, refresh resultset');
				// Refresh the existing Quality Result Set because the customer has changed: the objective is to not loose measures in case some of them have been registered
				returnValue = refreshResultSet(itemId, customerId, qualityResultSet);
			} else {
				log.debug('no change on resultset');
				returnValue = qualityResultSet;
			}
		} else {
			log.debug('invalid resultset ==> create new');
			// No record corresponds to the given qualityResultSet: maybe it was deleted
			// ==> Initialize a new Quality Result Set
			returnValue = initializeNewResultSet(itemId, customerId, siloName, woNumber, subsidiary);
		}
	} else {
		log.debug('create new resultset');	
		// Initialize a new Quality Result Set
		returnValue = initializeNewResultSet(itemId, customerId, siloName, woNumber, subsidiary);
	}

	log.debug('resultSetId: ' + returnValue);
	log.debug('initializeResultSetCall completed');
	
	return parseInt(returnValue, 10);
}

function refreshResultSet(itemId, customerId, qualityResultSet) {
	log.debug('refreshResultSet started');
	let toleranceSetId = retrieveToleranceSet(itemId, customerId);
	updateQualityResultSet(toleranceSetId, customerId, qualityResultSet);

	if(toleranceSetId == 0) {
		// Refresh results based on the list of tests
		refreshResultLinesWithTests(qualityResultSet);
	} else {
		// Refresh results based on the tolerances
		refreshResultLinesWithTolerances(qualityResultSet, toleranceSetId);
	}
	log.debug('refreshResultSet completed');
	return qualityResultSet;
}

function initializeNewResultSet(itemId, customerId, siloName, woNumber, subsidiary) {
	log.debug('initializeNewResultSet started');
	let qualitySetup = getQualitySetupCall(subsidiary);
	
	// Exit if quality is not enabled on subsidiary
	if (!qualitySetup.outgoingQualityFlag) {
		log.debug('Outgoing quality disabled for subsidiary ' + subsidiary);
		return 0;
	}
	
	if (!qualitySetup.customerLevelFlag) {
		customerId = 0;
	}
	
	// Exit if item does not support outgoing quality
	let itemFieldsValues = SEARCHMODULE.lookupFields({
		type: SEARCHMODULE.Type.ITEM,
		id: itemId,
		columns: ['displayname', 'custitem_mob_outgoing_quality']
	});
	if (!itemFieldsValues.custitem_mob_outgoing_quality) {
		log.debug('Outgoing quality disabled for item ' + itemFieldsValues.displayname);
		return 0;
	}
	
	let toleranceSetId = 0;
	// Exit if no tolerance set is defined
	if (qualitySetup.customerLevelFlag && customerId > 0 || !qualitySetup.customerLevelFlag) {
		toleranceSetId = retrieveToleranceSet(itemId, customerId);
		if ( toleranceSetId == 0) {
			let message = 'Tolerance set not found for item ' + itemFieldsValues.displayname;
			if (customerId > 0) {				
				let customerFieldsValues;
				if (customerId > 0) {
					customerFieldsValues = SEARCHMODULE.lookupFields({
						type: SEARCHMODULE.Type.CUSTOMER,
						id: customerId,
						columns: ['entityid']
					});				}
				message += ' and customer ' + customerFieldsValues.entityid;
			}
			log.debug(message);
			return 0;
			/*
			throw ERRORMODULE.create({
				name: 'BWK_TOLERANCE_NOTFOUND',
				message: message
			});
			*/
		}
	}
	let resultSetId = createQualityResultSet(toleranceSetId, itemId, customerId, siloName, woNumber, qualitySetup);
	
	if (qualitySetup.customerLevelFlag && customerId == 0) {
		// When quality requires to be defined at customer level and customer is unknown
		// if so intitiate the results based on the list of tests (like for initiation of tolerance set lines)
		// result informations will be completed once the customer is populated
		initializeResultLinesWithTests();
	} else {
		// Initiate results based on the tolerance set
		initializeResultLinesWithTolerances(resultSetId, toleranceSetId);
	}
	
	log.debug('resultSetId: ' + resultSetId);
	log.debug('initializeNewResultSet completed');
	
	return resultSetId;
}

function retrieveInitialStatus() {
	let initialStatus = {
		status: '',
		globalStatus: ''
	};
	
    let statusSearchObj = SEARCHMODULE.create({
        type: 'customlist_mob_statuts_qualite',
        filters: [['scriptid', 'is', 'val_260495_5028481_sb1_104']], // Status 'En Attente Laboratoire'
        columns: ['internalid', 'name']
    });
    initialStatus.status = searchResult = statusSearchObj.run().getRange({ start: 0, end: 1 })[0].getValue({ name: 'internalid' });

    let gblStatusSearchObj = SEARCHMODULE.create({
        type: 'customlist_mob_qualityglobalstatus',
        filters: [['scriptid', 'is', 'val_260841_5028481_sb1_804']], // Status 'In Progress'
        columns: ['internalid', 'name']
    });
    initialStatus.globalStatus = searchResult = gblStatusSearchObj.run().getRange({ start: 0, end: 1 })[0].getValue({ name: 'internalid' });

    return initialStatus;
}
	
function createQualityResultSet(toleranceSetId, itemId, customerId, siloName, woNumber, qualitySetup) {
	log.debug('createQualityResultSet started');
	
	// let initialStatus = retrieveInitialStatus();
	
	// Creation of the quality result set record
	let resultSetRec = RECORDMODULE.create({
		type: 'customrecord_mob_qualityresultset'
	});
	resultSetRec.setValue({
		fieldId: 'custrecord_mob_resultsetitem',
		value: itemId
	});
	// resultSetRec.setValue({
	// 	fieldId: 'custrecord_mob_resultsetstatus',
	// 	value: initialStatus.status
	// });
	// resultSetRec.setValue({
	// 	fieldId: 'custrecord_mob_resultsetglobalstatus',
	// 	value: initialStatus.globalStatus
	// });
	resultSetRec.setValue({
		fieldId: 'custrecord_mob_resultset72htesting',
		value: qualitySetup.h72testing
	});
	if (toleranceSetId != 0) {
		resultSetRec.setValue({
			fieldId: 'custrecord_mob_resultsettoleranceset',
			value: toleranceSetId
		});		
	}
	/*
	 * Do not populate WO number: result set must be accessed from wo completion to activate workflow
	 * if wo number is populated the result set could be accessed from the work order because of field 
	 * defined with 'record is parent' property set to true
	resultSetRec.setValue({
		fieldId: 'custrecord_mob_resultsetwonumber',
		value: woNumber
	});
	*/
	resultSetRec.setValue({
		fieldId: 'custrecord_mob_resultsetsiloname',
		value: siloName
	});
	if (customerId != 0) {
		resultSetRec.setValue({
			fieldId: 'custrecord_mob_resultsetcustomer',
			value: customerId
		});		
	}
	let resultSetId = resultSetRec.save();
	log.debug('createQualityResultSet completed');
	return resultSetId;
}

function updateQualityResultSet(toleranceSetId, customerId, qualityResultSet) {
	// Update toleranceset and customer ; itemId should not change: it is the assembly item of the work order
	log.debug('updateQualityResultSet started');
	let submitValues = {};
	if (toleranceSetId != 0) {
		submitValues.custrecord_mob_resultsettoleranceset = toleranceSetId;
	}
	if (customerId != 0) {
		submitValues.custrecord_mob_resultsetcustomer = customerId;	
	}
	RECORDMODULE.submitFields({
		type: 'customrecord_mob_qualityresultset',
		id: qualityResultSet,
		values: submitValues
	});
	log.debug('updateQualityResultSet completed');
}

function initializeResultLinesWithTests(resultSetId) {
	log.debug('initializeResultLinesWithTests started');
	// Creation of one result record per test
	let displaySequenceColumn = SEARCHMODULE.createColumn({
		name: 'custrecord_mob_testsetup_displaysequence',
		sort: SEARCHMODULE.Sort.ASC
	});
	let nameColumn = SEARCHMODULE.createColumn({
		name: 'name',
		sort: SEARCHMODULE.Sort.ASC
	});
	let searchObj = SEARCHMODULE.create({
		type: 'customrecord_mob_qm_testsetup',
		columns: [
			'internalid', 'custrecord_mob_testsetup_defaultnorm', 'custrecord_mob_testsetup_defaultopmode', 
			'custrecord_mob_testsetup_unit', 
			// 'custrecord_mob_testsetup_displaysequence',
			displaySequenceColumn, nameColumn
		],
		filters: [SEARCHMODULE.createFilter({
			name: 'isinactive',
			operator: SEARCHMODULE.Operator.IS,
			values: false
			})]
	});
	searchObj.run().each(function(qualityTest) {
		let resultRec = RECORDMODULE.create({
			type: 'customrecord_mob_qualityresult'
		});
		resultRec.setValue({
			fieldId: 'custrecord_mob_qualityresultset',
			value: resultSetId
		});
		let testId = qualityTest.getValue('internalid');
		resultRec.setValue({
			fieldId: 'custrecord_mob_resulttest',
			value: testId
		});
		let resultLineId;
		try {
			resultLineId = resultRec.save();			
		} catch (ex) {
			let test = testid + '-' + qualityTest.getValue('name');
			log.error({
				title: 'Failed to create result line',
				details: 'Failed to create test: ' + test + ' for result set ' + resultSetId + ' ' + ex
			});
		}
		if (resultLineId) {
			try {
				RECORDMODULE.submitFields({
					type: 'customrecord_mob_qualityresult',
					id: resultLineId,
					values: {
						custrecord_mob_resultnorm: qualityTest.getValue({ name: 'custrecord_mob_testsetup_defaultnorm' }),
						custrecord_mob_resultoperatingmode: qualityTest.getValue({ name: 'custrecord_mob_testsetup_defaultopmode' }),
						custrecord_mob_resultunit: qualityTest.getValue({ name: 'custrecord_mob_testsetup_unit' }),
						custrecord_mob_resultdisplaysequence: qualityTest.getValue({ name: 'custrecord_mob_testsetup_displaysequence' })
					}
				});				
			} catch (ex){
				let test = testid + '-' + qualityTest.getValue('name');
				log.error({
					title: 'Failed to update result line',
					details: 'Failed to update test: ' + test + ' for result set ' + resultSetId + ' ' + ex
				});				
			}
		}
//		resultRec.setValue({
//			fieldId: 'custrecord_mob_resultnorm',
//			value: qualityTest.getValue('custrecord_mob_testsetup_defaultnorm')
//		});
//		resultRec.setValue({
//			fieldId: 'custrecord_mob_resultoperatingmode',
//			value: qualityTest.getValue('custrecord_mob_testsetup_defaultopmode')
//		});
//		resultRec.setValue({
//			fieldId: 'custrecord_mob_resultunit',
//			value: qualityTest.getValue('custrecord_mob_testsetup_unit')
//		});
//		try {
//			resultRec.save();			
//		} catch (ex) {
//			log.debug({
//				title: 'testId',
//				details: testId
//			});
//			log.debug({
//				title: 'Norm',
//				details: qualityTest.getValue('custrecord_mob_testsetup_defaultnorm')
//			});
//			log.debug({
//				title: 'operating mode',
//				details: qualityTest.getValue('custrecord_mob_testsetup_defaultopmode')
//			});
//			log.debug({
//				title: 'result unit',
//				details: qualityTest.getValue('custrecord_mob_testsetup_unit')
//			});
//		}
		return true;
	});
	log.debug('initializeResultLinesWithTests completed');
}

function initializeResultLinesWithTolerances(resultSetId, toleranceSetId) {
	log.debug('initializeResultLinesWithTolerances started');
	// Creation of one result record per tolerance
	let displaySequenceColumn = SEARCHMODULE.createColumn({
		name: 'custrecord_mob_tolerancedisplaysequence',
		sort: SEARCHMODULE.Sort.ASC
	});
	let toleranceTestColumn = SEARCHMODULE.createColumn({
		name: 'custrecord_mob_tolerancetest',
		sort: SEARCHMODULE.Sort.ASC
	});
	let searchObj = SEARCHMODULE.create({
		type: 'customrecord_mob_qm_tolerance',
		columns: [
			'custrecord_mob_tolerancenorm', 'custrecord_mob_toleranceoperatingmode', 
			'custrecord_mob_tolerancelowervalue', 'custrecord_mob_tolerancehighervalue', 'custrecord_mob_toleranceunit',
			// 'custrecord_mob_tolerancedisplaysequence',
			displaySequenceColumn,
			toleranceTestColumn
		],
		filters: [SEARCHMODULE.createFilter({
			name: 'custrecord_mob_toleranceset',
			operator: SEARCHMODULE.Operator.IS,
			values: toleranceSetId
		})]
	});
	searchObj.run().each(function(tolerance) {
		let resultRec = RECORDMODULE.create({
			type: 'customrecord_mob_qualityresult'
		});
		resultRec.setValue({
			fieldId: 'custrecord_mob_qualityresultset',
			value: resultSetId
		});
		let testId = tolerance.getValue('custrecord_mob_tolerancetest');
		resultRec.setValue({
			fieldId: 'custrecord_mob_resulttest',
			value: testId
		});
		let resultLineId;
		try {
			resultLineId = resultRec.save();			
		} catch (ex) {
			log.error({
				title: 'Failed to create result line',
				details: 'Failed to create test: ' + testId + ' for result set ' + resultSetId + ' ' + ex
			});
		}
		if (resultLineId) {
			try {
				RECORDMODULE.submitFields({
					type: 'customrecord_mob_qualityresult',
					id: resultLineId,
					values: {
						custrecord_mob_resultnorm: tolerance.getValue({ name: 'custrecord_mob_tolerancenorm' }),
						custrecord_mob_resultoperatingmode: tolerance.getValue({ name: 'custrecord_mob_toleranceoperatingmode' }),
						custrecord_mob_resultlowervalue: tolerance.getValue({ name: 'custrecord_mob_tolerancelowervalue' }),
						custrecord_mob_resulthighervalue: tolerance.getValue({ name: 'custrecord_mob_tolerancehighervalue' }),
						custrecord_mob_resultunit: tolerance.getValue({ name: 'custrecord_mob_toleranceunit' }),
						custrecord_mob_resultdisplaysequence: tolerance.getValue({ name: 'custrecord_mob_tolerancedisplaysequence' })
					}
				});				
			} catch (ex){
				let fieldsValues = SEARCHMODULE.lookupFields({
					type: 'customrecord_mob_qm_testsetup',
					id: testId,
					columns: 'name'
				});
//				log.debug ({
//					title: 'fieldsValues',
//					details: fieldsValues
//				});
				let test = testId;
				if ('name' in fieldsValues) {
					test += '-' + fieldsValues.name;
				}
				log.error({
					title: 'Failed to update result line',
					details: 'Failed to update test: ' + test + ' for result set ' + resultSetId + ' ' + ex
				});				
			}
		}
//		resultRec.setValue({
//			fieldId: 'custrecord_mob_resultnorm',
//			value: tolerance.getValue('custrecord_mob_tolerancenorm')
//		});
//		resultRec.setValue({
//			fieldId: 'custrecord_mob_resultoperatingmode',
//			value: tolerance.getValue('custrecord_mob_toleranceoperatingmode')
//		});
//		resultRec.setValue({
//			fieldId: 'custrecord_mob_resultlowervalue',
//			value: tolerance.getValue('custrecord_mob_tolerancelowervalue')
//		});
//		resultRec.setValue({
//			fieldId: 'custrecord_mob_resulthighervalue',
//			value: tolerance.getValue('custrecord_mob_tolerancehighervalue')
//		});
//		resultRec.setValue({
//			fieldId: 'custrecord_mob_resultunit',
//			value: tolerance.getValue('custrecord_mob_toleranceunit')
//		});
//		try {
//			resultRec.save();			
//		} catch (ex) {
//			log.debug({
//				title: 'toleranceTest',
//				details: toleranceTest
//			});
//			log.debug({
//				title: 'Norm',
//				details: tolerance.getValue('custrecord_mob_tolerancenorm')
//			});
//			log.debug({
//				title: 'operating mode',
//				details: tolerance.getValue('custrecord_mob_toleranceoperatingmode')
//			});
//			log.debug({
//				title: 'result unit',
//				details: tolerance.getValue('custrecord_mob_toleranceunit')
//			});
//		}
		return true;
	});
	log.debug('initializeResultLinesWithTolerances completed');	
}

function refreshResultLinesWithTests(resultSetId) {
	log.debug('refreshResultLinesWithTests started');
	// Get the list of used tests
	let resultSearchObj = SEARCHMODULE.create({
		type: 'customrecord_mob_qualityresult',
		columns: ['internalid', 'custrecord_mob_resulttest'],
		filters: [SEARCHMODULE.createFilter({
			name: 'custrecord_mob_qualityresultset',
			operator: SEARCHMODULE.Operator.IS,
			values: resultSetId
			})]
	});
	let resultTestIds = [];
	let resultIds = [];
	resultSearchObj.run().each(function(result) {
		resultTestIds.push(result.getValue('custrecord_mob_resulttest'));
		resultIds.push(result.getValue('internalid'));		
		return true;
	});
	
	// Creation of one result record per test that is not already used
	// Update tolerance range to empty
	let searchObj = SEARCHMODULE.create({
		type: 'customrecord_mob_qm_testsetup',
		columns: ['internalid', 'custrecord_mob_testsetup_defaultnorm', 'custrecord_mob_testsetup_defaultopmode', 
		          'custrecord_mob_testsetup_unit', 'custrecord_mob_testsetup_displaysequence'],
		filters: [SEARCHMODULE.createFilter({
			name: 'isinactive',
			operator: SEARCHMODULE.Operator.IS,
			values: false
			})]
	});
	
	searchObj.run().each(function(qualityTest) {
		let qualityTestId = qualityTest.getValue('internalid');
		try {
			let resultIndex = resultTestIds.indexOf(qualityTestId);
			if (resultIndex < 0){
				let resultRec = RECORDMODULE.create({
					type: 'customrecord_mob_qualityresult'
				});
				resultRec.setValue({
					fieldId: 'custrecord_mob_qualityresultset',
					value: resultSetId
				});
				resultRec.setValue({
					fieldId: 'custrecord_mob_resulttest',
					value: qualityTestId
				});
				resultRec.setValue({
					fieldId: 'custrecord_mob_resultnorm',
					value: qualityTest.getValue('custrecord_mob_testsetup_defaultnorm')
				});
				resultRec.setValue({
					fieldId: 'custrecord_mob_resultoperatingmode',
					value: qualityTest.getValue('custrecord_mob_testsetup_defaultopmode')
				});
				resultRec.setValue({
					fieldId: 'custrecord_mob_resultunit',
					value: qualityTest.getValue('custrecord_mob_testsetup_unit')
				});
				resultRec.setValue({
					fieldId: 'custrecord_mob_resultdisplaysequence',
					value: qualityTest.getValue('custrecord_mob_testsetup_displaysequence')
				});
				resultRec.save();				
			} else {
				RECORDMODULE.submitFields({
					type: 'customrecord_mob_qualityresult',
					id: resultIds[resultIndex],
					values: {
						custrecord_mob_resultnorm: qualityTest.getValue('custrecord_mob_testsetup_defaultnorm'),
						custrecord_mob_resultoperatingmode: qualityTest.getValue('custrecord_mob_testsetup_defaultopmode'),
						custrecord_mob_resultlowervalue: '', 
						custrecord_mob_resulthighervalue: '',
						custrecord_mob_resultunit: qualityTest.getValue('custrecord_mob_testsetup_unit'),
						custrecord_mob_resultdisplaysequence: qualityTest.getValue('custrecord_mob_testsetup_displaysequence')
					}
				});	
			}	
		} catch (ex) {
			alert(ex);
		}
		
		return true;
	});
}

function refreshResultLinesWithTolerances(resultSetId, toleranceSetId) {
	log.debug('refreshResultLinesWithTolerances started');
	// Get the list of used tests
	let resultSearchObj = SEARCHMODULE.create({
		type: 'customrecord_mob_qualityresult',
		columns: ['internalid', 'custrecord_mob_resulttest'],
		filters: [SEARCHMODULE.createFilter({
			name: 'custrecord_mob_qualityresultset',
			operator: SEARCHMODULE.Operator.IS,
			values: resultSetId
			})]
	});
	let resultTestIds = [];
	let resultIds = [];
	resultSearchObj.run().each(function(result) {
		resultTestIds.push(result.getValue('custrecord_mob_resulttest'));
		resultIds.push(result.getValue('internalid'));
		
		return true;
	});
	
	// Creation of one result record per tolerance that is not already used
	// Update tolerance range
	let searchObj = SEARCHMODULE.create({
		type: 'customrecord_mob_qm_tolerance',
		columns: ['custrecord_mob_tolerancetest', 'custrecord_mob_tolerancenorm', 'custrecord_mob_toleranceoperatingmode', 
		          'custrecord_mob_tolerancelowervalue', 'custrecord_mob_tolerancehighervalue', 'custrecord_mob_toleranceunit',
		          'custrecord_mob_tolerancedisplaysequence'],
		filters: [SEARCHMODULE.createFilter({
			name: 'custrecord_mob_toleranceset',
			operator: SEARCHMODULE.Operator.IS,
			values: toleranceSetId
		})]
	});
	searchObj.run().each(function(tolerance) {
		let qualityTestId = tolerance.getValue('custrecord_mob_tolerancetest');
		try {
			let resultIndex = resultTestIds.indexOf(qualityTestId);
			if (resultIndex < 0){
				let resultRec = RECORDMODULE.create({
					type: 'customrecord_mob_qualityresult'
				});
				resultRec.setValue({
					fieldId: 'custrecord_mob_qualityresultset',
					value: resultSetId
				});
				resultRec.setValue({
					fieldId: 'custrecord_mob_resulttest',
					value: qualityTestId
				});
				resultRec.setValue({
					fieldId: 'custrecord_mob_resultnorm',
					value: tolerance.getValue('custrecord_mob_tolerancenorm')
				});
				resultRec.setValue({
					fieldId: 'custrecord_mob_resultoperatingmode',
					value: tolerance.getValue('custrecord_mob_toleranceoperatingmode')
				});
				resultRec.setValue({
					fieldId: 'custrecord_mob_resultlowervalue',
					value: tolerance.getValue('custrecord_mob_tolerancelowervalue')
				});
				resultRec.setValue({
					fieldId: 'custrecord_mob_resulthighervalue',
					value: tolerance.getValue('custrecord_mob_tolerancehighervalue')
				});
				resultRec.setValue({
					fieldId: 'custrecord_mob_resultunit',
					value: tolerance.getValue('custrecord_mob_toleranceunit')
				});
				resultRec.setValue({
					fieldId: 'custrecord_mob_resultdisplaysequence',
					value: tolerance.getValue('custrecord_mob_tolerancedisplaysequence')
				});
				resultRec.save();				
			} else {
				RECORDMODULE.submitFields({
					type: 'customrecord_mob_qualityresult',
					id: resultIds[resultIndex],
					values: {
						custrecord_mob_resultnorm: tolerance.getValue('custrecord_mob_tolerancenorm'),
						custrecord_mob_resultoperatingmode: tolerance.getValue('custrecord_mob_toleranceoperatingmode'),
						custrecord_mob_resultlowervalue: tolerance.getValue('custrecord_mob_tolerancelowervalue'), 
						custrecord_mob_resulthighervalue: tolerance.getValue('custrecord_mob_tolerancehighervalue'),
						custrecord_mob_resultunit: tolerance.getValue('custrecord_mob_toleranceunit'),
						custrecord_mob_resultdisplaysequence: tolerance.getValue('custrecord_mob_tolerancedisplaysequence')
					}
				});					
			}
		} catch (ex) {
			alert(ex);
		}
		
		return true;		
	});	
	log.debug('refreshResultLinesWithTolerances completed');
}

function refreshResultSetCall(toleranceSetId, resultSetId) {
	
}

/**
 * 
 * @returns {integer} Return the id of the tolerance set or 0 when it does not exists
 */
function retrieveToleranceSet(itemId, customerId) {
	itemId = parseInt(itemId || 0, 10);
	customerId = parseInt(customerId || 0, 10);
	
	// Search on Tolerance Set
	let itemFilter = SEARCHMODULE.createFilter({
		name: 'custrecord_mob_tolerancesetitem',
		operator: SEARCHMODULE.Operator.IS,
		values: itemId
	});
	let customerFilter;
	if (customerId != 0) {
		customerFilter = SEARCHMODULE.createFilter({
			name: 'custrecord_mob_tolerancesetcustomer',
			operator: SEARCHMODULE.Operator.IS,
			values: customerId
		});		
	} else {
		customerFilter = SEARCHMODULE.createFilter({
			name: 'custrecord_mob_tolerancesetcustomer',
			operator: SEARCHMODULE.Operator.ISEMPTY
		});		
	}
	let searchObj = SEARCHMODULE.create({
		type: 'customrecord_mob_qm_toleranceset',
		columns: ['internalid'],
		filters: [itemFilter, customerFilter]
	});
	let searchResultSet = searchObj.run();		
	let searchResult = searchResultSet.getRange({
		start: 0,
		end: 1
	});	
	
	let toleranceSetId = 0;
	if (searchResult.length > 0) {
		toleranceSetId = parseInt(searchResult[0].getValue('internalid'), 10);
	}
	
	return toleranceSetId;	
}

function finalizeResultSetCall(resultSetId) {
	log.debug({
		title: 'Function started',
		details: 'finalizeResultSetCall'
	});
	let workOrder;
	resultSetId = parseInt(resultSetId, 10);
	
    let searchObj = SEARCHMODULE.create({
        type: 'customrecord_mob_wo_productionsilo',
        filters: [['custrecord_mob_cmpl_qualresultset', 'is', resultSetId]],
        columns: ['internalid', 'custrecord_mob_cmpl_silowonumber']
    });
	// One result set is referenced by one production silo
    let results = searchObj.run().getRange({ start: 0, end: 1 });
    if (results.length > 0) {
		workOrder = results[0].getValue({ name: 'custrecord_mob_cmpl_silowonumber' });
    }

	// Populate the resultset with the work order number ==> it makes the resultset visible on the work order form
	if (workOrder) {
		RECORDMODULE.submitFields({
			type: 'customrecord_mob_qualityresultset',
			id: resultSetId,
			values: {
				custrecord_mob_resultsetwonumber: workOrder,
				custrecord_mob_resultsetlocked: true
			}
		});
	}
	log.debug({
		title: 'Function done',
		details: 'finalizeResultSetCall'
	});
}

/**
 * 
 * @returns {Object}
 * 	.internalid: internalid of the customrecord_mob_frozenperiod record that stores the frozen period
 * 	.period: information about the frozen period 
 * 	.period.value: internalid of the accounting period that is frozen
 * 	.period.text: the name of tne accounting period that is frozen
 * 	.period.endDate: the end date of the accounting period that is frozen
 */
function retrieveFrozenPeriodCall() {
    let frozenPeriod;
    // This search should return only one line, it is not planned to insert more than one line in this record
    let searchObj = SEARCHMODULE.create({
        type: 'customrecord_mob_frozenperiod',
        // columns: ['internalid', 'custrecord_mob_frozenperiod', 'custrecord_mob_frozenperiodenddate', 'custrecord_mob_frozenperiod.periodname']
        columns: ['internalid', 'custrecord_mob_frozenperiod', 'custrecord_mob_frozenperiodenddate']
    });

    searchObj.run().each(result => {
		frozenPeriod = {
			internalId: result.getValue({ name: 'internalid' }),
			period: {
				value: result.getValue({ name: 'custrecord_mob_frozenperiod' }),
				// text: result.getValue({ name: 'periodname', join: 'custrecord_mob_frozenperiod' }),
				endDate: FORMATMODULE.parse({
					value: result.getValue({ name: 'custrecord_mob_frozenperiodenddate' }),
					type: FORMATMODULE.Type.DATE
				})
			}
		};
        return false;
    });

	// Complete period name - it is performed outside the initial search to avoid a crash in case user's don't have
	// permission on 'Manage Accounting Periods'
	try {
		let fieldsValues = SEARCHMODULE.lookupFields({
			type: SEARCHMODULE.Type.ACCOUNTING_PERIOD,
			id: frozenPeriod.period.value,
			columns: ['periodname']
		});
		frozenPeriod.period.text = fieldsValues.periodname;
	} catch (ex) {
		// Do nothing, frozenPeriod.period.text is not populated
	}

    return frozenPeriod;
}

function logRecord(record, logTitle) {
	let stringRecord = JSON.stringify(record) || 'UNDEFINED';
	let recNum = 0;
	while (stringRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: stringRecord
		});
		stringRecord = stringRecord.substring(3900);		
	}
}

function logVar(value, title) {
	log.debug({
		title: title + ' (' + typeof value + ')',
		details: value 
	});
}

function dummy() {
	// TODO: remove this line - it is for testing purpose
	let results = '{ "results": [{"recordType": "workorder","id": "33375","values": {"internalid": [{"value": "33375","text": "33375"}],"item.upccode": "40026177",'
	    		+	'"item.displayname": "TEST maj manuelle results","item.stockdescription": "PPC10NM BB_TEST",'
	    		+	'"CUSTRECORD_MOB_CMPL_WONUMBER.custrecord_mob_cmpl_packagenumber": "EMO20200429_3-001x",'
	    		+	'"custbody_lienarticleclient": [],"formulatext": "001","custbody_mob_wo_lot": "EMO20200429_3","startdate": "28/5/2020",'
	    		+	'"trandate": "29/4/2020","enddate": "28/5/2020","CUSTRECORD_MOB_CMPL_WONUMBER.custrecord_mob_cmpl_lotnumber": "EMO20200429_3"}}]}';
	let resultsx = JSON.parse(results);
	results = { "results": [
        	{
        		"recordType": "na",
        		"id": "0",
        		"values": {
        			"internalid": [
        				{
        					"value": "33375",
        					"text": "33375"
        				}
        			],
        			"item.upccode": "40026177",
        			"item.displayname": "PPC10NM BB",
        			"item.stockdescription": "PPC10NM BB",
        			"CUSTRECORD_MOB_CMPL_WONUMBER.custrecord_mob_cmpl_packagenumber": "EMO20200429_3-001",
        			"custbody_lienarticleclient": [],
        			"formulatext": "001",
        			"custbody_mob_wo_lot": "EMO20200429_3",
        			"startdate": "28/5/2020",
        			"trandate": "29/4/2020",
        			"enddate": "28/5/2020",
        			"CUSTRECORD_MOB_CMPL_WONUMBER.custrecord_mob_cmpl_lotnumber": "EMO20200429_3"
        		}
        	},
        	{
        		"recordType": "na",
        		"id": "0",
        		"values": {
        			"internalid": [
        				{
        					"value": "33375",
        					"text": "33375"
        				}
        			],
        			"item.upccode": "40026177",
        			"item.displayname": "PPC10NM BB",
        			"item.stockdescription": "PPC10NM BB",
        			"CUSTRECORD_MOB_CMPL_WONUMBER.custrecord_mob_cmpl_packagenumber": "EMO20200429_3-001",
        			"custbody_lienarticleclient": [],
        			"formulatext": "001",
        			"custbody_mob_wo_lot": "EMO20200429_3",
        			"startdate": "28/5/2020",
        			"trandate": "29/4/2020",
        			"enddate": "28/5/2020",
        			"CUSTRECORD_MOB_CMPL_WONUMBER.custrecord_mob_cmpl_lotnumber": "EMO20200429_3"
        		}
        	}
        ]        			
	};
	let jsonResults = JSON.stringify(results);
	logRecord(jsonResults, 'jsonResults');
	renderer.addCustomDataSource({
		alias: 'results',
		format: 'JSON',
		data: jsonResults
	});
}