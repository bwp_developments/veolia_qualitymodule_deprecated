var RECORDMODULE, REDIRECTMODULE, RUNTIMEMODULE, SEARCHMODULE;

/**
 * @NApiVersion 2.1
 * @NScriptType workflowactionscript
 */
define(['N/record', 'N/redirect', 'N/runtime', 'N/search', './qualityModule'], runWorkflowAction);

function runWorkflowAction(record, redirect, runtime, search, qualityModule) {
	RECORDMODULE= record;
	REDIRECTMODULE= redirect;
	RUNTIMEMODULE= runtime;
	SEARCHMODULE= search;
	QUALITYMODULE = qualityModule;
	
	var returnObj = {};
	returnObj['onAction'] = _onAction;
	return returnObj;
}

function _onAction(scriptContext) {
	log.debug({
		title: 'Workflow script started'
	});
	var scriptObj = RUNTIMEMODULE.getCurrentScript();
	var userObj = RUNTIMEMODULE.getCurrentUser();
	
	// Read setup on user's subsidiary
	
	/*
	 * The use of search.lookupFields requires less governance units
	var userSubsidiary = RECORDMODULE.load({
		type: RECORDMODULE.Type.SUBSIDIARY,
		id: currentUser.subsidiary
	});
	var customerLevel = userSubsidiary.getValue('custrecord_mob_toleranceatcustomerlevel');
	*/
	
	var fieldsValues = SEARCHMODULE.lookupFields({
		type: SEARCHMODULE.Type.SUBSIDIARY,
		id: userObj.subsidiary,
		columns: 'custrecord_mob_toleranceatcustomerlevel'
	});
	var customerLevel = fieldsValues['custrecord_mob_toleranceatcustomerlevel'];
	
	if (customerLevel) {
		log.debug('Customer Level enabled');
	} else {
		log.debug('Customer Level disabled');		
	}
	
	currentRecord = scriptContext.newRecord;
	var recordType = '';
	var itemId = 0;
	var customerId = 0;
	// Maybe there is a better way to identify that a lotnumberedassemblyitem record corresponds to an ITEM
	switch (currentRecord.type) {
		case 'lotnumberedassemblyitem':
			recordType = 'ITEM';
			itemId = currentRecord.getValue({
				fieldId: 'id'
			});
			break;
		case 'customer':
			recordType = 'CUSTOMER';
			customerId = currentRecord.getValue({
				fieldId: 'id'
			});
			break;		
	}
	log.debug({
		title: 'Record type',
		details: currentRecord.type 
	});
	
	if (recordType == 'ITEM' && !customerLevel) {		
		if (itemId == 0) {
			log.Error({
				title: 'Item not populated',
				details: scriptContext
			})
			return;
		}
		
		redirectToToleranceSet(itemId);
	} else {
		redirecttoItemCustomerChoice(itemId, customerId);
	}
	
	log.debug("Remaining governance units: " + scriptObj.getRemainingUsage());
	
	log.debug({
		title: 'Workflow script completed'
	});
}

function redirectToToleranceSet(itemId) {
	var toleranceSetId = QUALITYMODULE.getToleranceSet(itemId, 0);
	
	log.debug('Tolerance Set is: ' + toleranceSetId);
	
	REDIRECTMODULE.toRecord({
		id: toleranceSetId,
		type: 'customrecord_mob_qm_toleranceset'
	});
}

function redirecttoItemCustomerChoice(item, customer) {
	item = item || 0;
	customer = customer || 0;
	/*
	 * It is useless. toSuitelet works with the scriptId but it is not documented. Documentation only deals with script internalid
	// TODO: code to retrieve internalid based on scriptid 'customscript_mob_sl_customertoleranceset'
	var searchObj = SEARCHMODULE.create({
		type: SEARCHMODULE.Type.SUITELET,
		columns: ['internalid'],
		filters: [SEARCHMODULE.createFilter({
			name: 'scriptid',
			operator: SEARCHMODULE.Operator.IS,
			values: 'customscript_mob_sl_customertoleranceset'
		})]
	});
	var searchResultSet = searchObj.run();		
	var searchResult = searchResultSet.getRange({
		start: 0,
		end: 1
	});
	var scriptInternalId = searchResult[0].getValue('internalid');
	log.debug({
		title: 'Script search result',
		details: searchResult[0].getValue('internalid')
	});
	*/
	REDIRECTMODULE.toSuitelet({
		scriptId: 'customscript_mob_sl_qm_itemcustchoice',
		deploymentId: 'customdeploy_mob_sl_qm_itemcustchoice',
		parameters: {'mob_item':item, 'mob_customer':customer}
	});	
}

function logRecord(record, logTitle) {
	var logRecord = JSON.stringify(record);
	var recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);		
	}
}