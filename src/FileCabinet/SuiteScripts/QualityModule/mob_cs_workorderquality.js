var CURRENTRECORDMODULE, SEARCHMODULE, TRANSLATIONMODULE, UIDIALOGMODULE, URLMODULE;

/**
 * @NApiVersion 2.1
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */
define(['N/currentRecord', 'N/search', 'N/translation', 'N/ui/dialog', 'N/url'], runClient);

function runClient(currentRecord, search, translation, uidialog, url) {
	CURRENTRECORDMODULE= currentRecord;
	SEARCHMODULE= search;
	TRANSLATIONMODULE= translation;
	UIDIALOGMODULE= uidialog;
	URLMODULE= url;
	
	var returnObj = {};
//	returnObj['pageInit'] = _pageInit;
	returnObj['fieldChanged'] = _fieldChanged;
//	returnObj['postSourcing'] = _postSourcing;
//	returnObj['sublistChanged'] = _sublistChanged;
//	returnObj['lineInit'] = _lineInit;
//	returnObj['validateField'] = _validateField;
//	returnObj['validateLine'] = _validateLine;
//	returnObj['validateInsert'] = _validateInsert;
//	returnObj['validateDelete'] = _validateDelete;
//	returnObj['saveRecord'] = _saveRecord;
	returnObj['openWithForceClose'] = openWithForceCloseCall;
	returnObj['printLabels'] = printLabelsCall;
	returnObj['receive'] = receiveCall;
	returnObj['quality'] = qualityCall;
	return returnObj;
}
    
/**
 * Function to be executed after page is initialized.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.mode - The mode in which the record is being accessed (create, copy, or edit)
 *
 * @since 2015.2
 */
function _pageInit(scriptContext) {

}

/**
 * Function to be executed when field is changed.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 * @param {string} scriptContext.fieldId - Field name
 * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
 * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
 *
 * @since 2015.2
 */
function _fieldChanged(scriptContext) {
	// Automatically populate custbody_lienarticleclient field
	if (['entity', 'assemblyitem'].includes(scriptContext.fieldId)) {		
		var woRecord = scriptContext.currentRecord;
		var entityId = woRecord.getValue({
			fieldId : 'entity'
		});
		var assemblyItem = woRecord.getValue({
			fieldId : 'assemblyitem'
		});
		
		if (entityId && assemblyItem) {
			var searchObj = SEARCHMODULE.create({
				type: 'customrecord_scm_customerpartnumber',
				columns: ['internalid', 'name'],
				filters: [SEARCHMODULE.createFilter({
					name: 'custrecord_scm_cpn_customer',
					operator: SEARCHMODULE.Operator.IS,
					values: entityId
					}),
					SEARCHMODULE.createFilter({
						name: 'custrecord_scm_cpn_item',
						operator: SEARCHMODULE.Operator.IS,
						values: assemblyItem
						})
				]
			});			
			var searchResults = searchObj.run().getRange({
				start: 0,
				end: 2
			});
			if (searchResults.length == 1) {
				var customerPart = searchResults[0].getValue('internalid');
				woRecord.setValue({
					fieldId: 'custbody_lienarticleclient',
					value: customerPart
				});					
			}			
		}	
	}
	
	// Fields related to quality
	if (['subsidiary', 'assemblyitem'].includes(scriptContext.fieldId)) {
		var subsidiary = scriptContext.currentRecord.getValue({ fieldId: 'subsidiary' });
		var subsidiaryFieldsValues;
		if (subsidiary) {
			subsidiaryFieldsValues = SEARCHMODULE.lookupFields({
				type: SEARCHMODULE.Type.SUBSIDIARY,
				id: subsidiary,
				columns: ['custrecord_mob_outgoingquality', 'custrecord_mob_toleranceatcustomerlevel', 'custrecord_mob_multisilo', 'custrecord_mob_silocapacity']
			});			
		} else {
			subsidiaryFieldsValues = {
				custrecord_mob_outgoingquality: false, 
				custrecord_mob_toleranceatcustomerlevel: false,
				custrecord_mob_multisilo: false,
				custrecord_mob_silocapacity: 0
			};
		}
		
		var outgoingQualityField = scriptContext.currentRecord.getField({
			fieldId: 'custbody_mob_outgoing_quality_'
		});
		var capacityOfSiloField = scriptContext.currentRecord.getField({
			fieldId: 'custbody_mob_silocapacity'
		});
		var capacityOfContainerField = scriptContext.currentRecord.getField({
			fieldId: 'custbody_mob_containercapacity'
		});
		var numberOfPackagingField = scriptContext.currentRecord.getField({
			fieldId: 'custbodyrvd_number_of_packaging'
		});
		
		var assemblyItem = scriptContext.currentRecord.getValue({ fieldId : 'assemblyitem' });
		var itemFieldsValues;
		if (assemblyItem) {
			itemFieldsValues = SEARCHMODULE.lookupFields({
				type: SEARCHMODULE.Type.ITEM,
				id: assemblyItem,
				columns: ['custitem_mob_outgoing_quality']
			});			
		} else {
			itemFieldsValues = {
				custitem_mob_outgoing_quality: false
			};
		}
		
		/*
		// Display / Hide fields depending on outgoing quality setup		
		if (subsidiaryFieldsValues.custrecord_mob_outgoingquality && itemFieldsValues.custitem_mob_outgoing_quality) {
			numberOfPackagingField.isDisplay = false;
			scriptContext.currentRecord.setValue({
				fieldId: 'custbody_mob_outgoing_quality_',
				value: true
			});
			outgoingQualityField.isDisplay = true;
			capacityOfContainerField.isDisplay = true;
			if (!subsidiaryFieldsValues.custrecord_mob_multisilo) {
				capacityOfSiloField.isDisplay = false;
			} else {
				capacityOfSiloField.isDisplay = true;				
			}
		} else {
			numberOfPackagingField.isDisplay = true;
			outgoingQualityField.isDisplay = false;
			scriptContext.currentRecord.setValue({
				fieldId: 'custbody_mob_outgoing_quality_',
				value: false
			});
			capacityOfContainerField.isDisplay = false;
			capacityOfSiloField.isDisplay = false;
		}
		*/
		
		// Display / Hide fields depending on outgoing quality setup		
		if (subsidiaryFieldsValues.custrecord_mob_outgoingquality) {
			numberOfPackagingField.isDisplay = false;
			scriptContext.currentRecord.setValue({
				fieldId: 'custbody_mob_outgoing_quality_',
				value: true
			});
			outgoingQualityField.isDisplay = true;
			capacityOfContainerField.isDisplay = true;
			if (!subsidiaryFieldsValues.custrecord_mob_multisilo) {
				capacityOfSiloField.isDisplay = false;
			} else {
				capacityOfSiloField.isDisplay = true;				
			}
		} else {
			numberOfPackagingField.isDisplay = true;
			outgoingQualityField.isDisplay = false;
			scriptContext.currentRecord.setValue({
				fieldId: 'custbody_mob_outgoing_quality_',
				value: false
			});
			capacityOfContainerField.isDisplay = false;
			capacityOfSiloField.isDisplay = false;
		}
	}
}

/**
 * Function to be executed when field is slaved.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 * @param {string} scriptContext.fieldId - Field name
 *
 * @since 2015.2
 */
function _postSourcing(scriptContext) {
	
}

/**
 * Function to be executed after sublist is inserted, removed, or edited.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @since 2015.2
 */
function _sublistChanged(scriptContext) {

}

/**
 * Function to be executed after line is selected.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @since 2015.2
 */
function _lineInit(scriptContext) {

}

/**
 * Validation function to be executed when field is changed.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 * @param {string} scriptContext.fieldId - Field name
 * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
 * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
 *
 * @returns {boolean} Return true if field is valid
 *
 * @since 2015.2
 */
function _validateField(scriptContext) {

}

/**
 * Validation function to be executed when sublist line is committed.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @returns {boolean} Return true if sublist line is valid
 *
 * @since 2015.2
 */
function _validateLine(scriptContext) {

}

/**
 * Validation function to be executed when sublist line is inserted.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @returns {boolean} Return true if sublist line is valid
 *
 * @since 2015.2
 */
function _validateInsert(scriptContext) {

}

/**
 * Validation function to be executed when record is deleted.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @returns {boolean} Return true if sublist line is valid
 *
 * @since 2015.2
 */
function _validateDelete(scriptContext) {

}

/**
 * Validation function to be executed when record is saved.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @returns {boolean} Return true if record is valid
 *
 * @since 2015.2
 */
function _saveRecord(scriptContext) {

}

function openWithForceCloseCall() {
	var woRecord = CURRENTRECORDMODULE.get();
	var woRecordId = woRecord.getValue('id');
	//get the url for the suitelet - add woRecordId parameter
	var woRecordURL = URLMODULE.resolveRecord({
		recordType: 'workorder',
		recordId: woRecordId,
		params: {custparam_mob_forceclose: true}
	});
	
	var confirmOptions = {
		title: TRANSLATIONMODULE.get({collection: 'custcollection_mob_outgoingquality', key: 'CONFIRM_CLOSE_DIAL'})(),
		message: TRANSLATIONMODULE.get({collection: 'custcollection_mob_outgoingquality', key: 'CONFIRM_CLOSE_MSG'})()
	};
	UIDIALOGMODULE.confirm(confirmOptions).then(function(result) {
		if (result) {
			window.location = woRecordURL;
		}
	});	
}

function printLabelsCall() {
	var woRecord = CURRENTRECORDMODULE.get();
	var woRecordId = woRecord.getValue('id');
	//get the url for the suitelet - add woRecordId parameter
	var suiteletURL = URLMODULE.resolveScript({
		scriptId: 'customscript_mob_sl_display_prod_labels',
		deploymentId: 'customdeploy_mob_sl_display_prod_labels',
		params: {custparam_mob_workorderid: woRecordId},
		returnExternalUrl: false
	});
	
	window.open(suiteletURL);
}

function receiveCall() {
	var woRecord = CURRENTRECORDMODULE.get();
	var woRecordId = woRecord.getValue('id');
	//get the url for the suitelet - add woRecordId parameter
	var suiteletURL = URLMODULE.resolveScript({
		scriptId: 'customscript_mob_sl_receivewo',
		deploymentId: 'customdeploy_mob_sl_receivewo',
		params: {custparam_mob_workorder: woRecordId},
		returnExternalUrl: false
	});
	
	window.open(suiteletURL);
}

function qualityCall() {
	var woRecord = CURRENTRECORDMODULE.get();
	var woRecordId = woRecord.getValue('id');
	//get the url for the suitelet - add woRecordId parameter
	var suiteletURL = URLMODULE.resolveScript({
		scriptId: 'customscript_mob_sl_qualityoverview',
		deploymentId: 'customdeploy_mob_sl_qualityoverview',
		params: {custparam_mob_workorder: woRecordId},
		returnExternalUrl: false
	});
	
	window.open(suiteletURL);
}

