var QUERYMODULE, RECORDMODULE, REDIRECTMODULE, RUNTIMEMODULE, SEARCHMODULE, TRANSLATIONMODULE, UISERVERWIDGETMODULE, URLMODULE, QUALITYMODULE;

/**
 * @NApiVersion 2.1
 * @NScriptType Suitelet
 */
define(['N/query', 'N/record', 'N/redirect', 'N/runtime', 'N/search', 'N/translation', 'N/ui/serverWidget', 'N/url', './qualityModule'], runSuitelet);

function runSuitelet(query, record, redirect, runtime, search, translation, uiserverWidget, url, qualityModule) {
    QUERYMODULE= query;
	RECORDMODULE= record;
	REDIRECTMODULE= redirect;
	RUNTIMEMODULE= runtime;
	SEARCHMODULE= search;
	TRANSLATIONMODULE= translation;
	UISERVERWIDGETMODULE= uiserverWidget;
    URLMODULE= url;
    QUALITYMODULE= qualityModule;
	
	let returnObj = {};
	returnObj['onRequest'] = _onRequest;
	return returnObj;
}

/**
 * Defines the Suitelet script trigger point.
 * @param {Object} scriptContext
 * @param {ServerRequest} scriptContext.request - Incoming request
 * @param {ServerResponse} scriptContext.response - Suitelet response
 * @since 2015.2
 */
function _onRequest(context) {
    let method = context.request.method;
    logRecord(context, 'context');

	log.debug(method);
    if (method == 'GET') {
    	getFunction(context);
    } else if (method == 'POST') {
    	postFunction(context);
    }
}

function getFunction(context) {
	let workOrderId = context.request.parameters.custparam_mob_workorder;
  	let form = getFormTemplate(context, workOrderId);
  	
  	context.response.writePage(form);
}

function postFunction(context) {
	// let form = getFormTemplate(context, workOrderId, pageInitMessage, processResult.labelsToPrint);
  	// context.response.writePage(form);
}

function getFormTemplate(context, workOrderId) {
	let translationInfo = getTranslationInfo(context);

    let form = UISERVERWIDGETMODULE.createForm({
        title : applyTranslation(translationInfo, 'formTitle')
    });
    form.clientScriptModulePath = 'SuiteScripts/QualityModule/mob_cs_qualityoverview.js';

    getFormTemplateHeader(form, translationInfo);

    let silos = retrieveData(workOrderId, translationInfo);

    if (silos.length > 1) {
        getFormtemplateSummary(form, translationInfo);
    }
    getFormTemplateDetails(form, silos, translationInfo);

    populateFormTemplateHeader(form, workOrderId, translationInfo);
    populateFormtemplateSummary(form, silos);
    populateFormtemplateDetails(form, silos);

    // form.addSubmitButton({
    //     label : applyTranslation(translationInfo, 'submitButton')
    // });

    let printResultsButton = form.addButton({
        id: 'custpage_mob_btn_printresults',
        label: 'custpage_mob_btn_printresults',
        functionName: 'printMeasures'
    });
    printResultsButton.label = applyTranslation(translationInfo, 'custpage_mob_btn_printresults');
    
    return form;
}

function getTranslationInfo(context) {
    let auditMode = context.request.parameters.bwp_audit == 'T';
    // otis: objects translation infos
    let otis = { };
    otis.formTitle = { alias: 'quality', key: 'SL_QUALITYOVERVIEW_TITLE', defaultValue: 'no title', type: 'formTitle'  };
    addOtisForHeader(otis);
    addOtisForTabs(otis);
    addOtisForSiloSubtab(otis);
    addOtisForSublists(otis);
    addOtisForButtons(otis);
    addOtisForFreeText(otis);
    let translationHandle = TRANSLATIONMODULE.load({
        collections: [
            {
                collection: 'custcollection_mob_outgoingquality', 
                alias: 'quality', 
                keys: retrieveTranslationKeys(otis, 'quality')
            }
        ]
    });

    return { otis: otis, translationHandle: translationHandle, auditMode: auditMode };
}

function addOtisForHeader(otis) {
    otis.custpage_mob_workorderid = { alias: 'quality', key: 'ORDER_FLD', defaultValue: 'WorkOrder', type: 'field' };
    otis.custpage_mob_customer = { alias: 'quality', key: 'CUSTOMER_FLD', defaultValue: 'Customer', type: 'field'  };
    otis.custpage_mob_assemblyitem = { alias: 'quality', key: 'ASSEMBLYITEM_FLD', defaultValue: 'Assembly', type: 'field' };
    otis.custpage_mob_manufrouting = { alias: 'quality', key: 'MANUFROUTING_FLD', defaultValue: 'Routing', type: 'field' };
    otis.custpage_mob_quantity = { alias: 'quality', key: 'QUANTITY_FLD', defaultValue: 'Quantity', type: 'field' };
    otis.custpage_mob_built = { alias: 'quality', key: 'BUILT_FLD', defaultValue: 'Built', type: 'field' };
}

function addOtisForTabs(otis) {
    otis.custpage_mob_tab_summary = { alias: 'quality', key: 'SUMMARY_TAB', defaultValue: 'Summary', type: 'tab' };
    otis.custpage_mob_tab_detail = { alias: 'quality', key: 'DETAIL_TAB', defaultValue: 'Detail', type: 'tab' };
    otis.custpage_mob_subtab_silo = { alias: 'quality', key: 'SILO_SUBTAB', defaultValue: 'Silo', type: 'subtab' };
}

function addOtisForSiloSubtab(otis) {
    otis.custpage_mob_detailsilonumber = { alias: 'quality', key: 'SILO_FLD', defaultValue: 'Silo', type: 'field' };
    otis.custpage_mob_detailinventorynumber = { alias: 'quality', key: 'INVENTORY_FLD', defaultValue: 'Batch', type: 'field' };
    otis.custpage_mob_detailquantity = { alias: 'quality', key: 'PLANNEDQTY_FLD', defaultValue: 'Quantity Planned', type: 'field' };
    otis.custpage_mob_detailbuilt = { alias: 'quality', key: 'BUILT_FLD', defaultValue: 'Quantity Built', type: 'field' };
    otis.custpage_mob_detailqualitystatus = { alias: 'quality', key: 'QUALITYSTATUS_FLD', defaultValue: 'Quality Status', type: 'field' };
    otis.custpage_mob_detailqualitylink = { alias: 'quality', key: 'RESULTSETLINK_FLD', defaultValue: 'Link to Quality Form', type: 'field' };
}

function addOtisForSublists(otis) {
    // Summary sublist
    otis.custpage_mob_list_summary = { alias: 'quality', key: 'SUMMARY_LIST', defaultValue: 'Summary', type: 'field' };
    otis.custpage_mob_fld_summarysilonumber = { alias: 'quality', key: 'SILO_COL', defaultValue: 'Silo', type: 'field', sublist: 'custpage_mob_list_summary' };
    otis.custpage_mob_fld_summaryinventorynumber = { alias: 'quality', key: 'BATCH_COL', defaultValue: 'Batch', type: 'field', sublist: 'custpage_mob_list_summary' };
    otis.custpage_mob_fld_summaryquantity = { alias: 'quality', key: 'PLANNEDQTY_COL', defaultValue: 'Planned Qty', type: 'field', sublist: 'custpage_mob_list_summary' };
    otis.custpage_mob_fld_summarybuilt = { alias: 'quality', key: 'BUILTQTY_COL', defaultValue: 'Built Qty', type: 'field', sublist: 'custpage_mob_list_summary' };
    otis.custpage_mob_fld_summaryqualitystatus = { alias: 'quality', key: 'QUALITYSTATUS_COL', defaultValue: 'Quality Status', type: 'field', sublist: 'custpage_mob_list_summary' };
    otis.custpage_mob_fld_summaryqualitylink = { alias: 'quality', key: 'RESULTSETLINK_COL', defaultValue: 'Link to Quality', type: 'field', sublist: 'custpage_mob_list_summary' };

    // Detail sublist
    otis.custpage_mob_list_packages = { alias: 'quality', key: 'PACKAGES_LIST', defaultValue: 'Summary', type: 'field' };
    otis.custpage_mob_fld_packagenumber = { alias: 'quality', key: 'PACKAGE_COL', defaultValue: 'Package', type: 'field', sublist: 'custpage_mob_list_packages' };
    otis.custpage_mob_fld_packageinventorystatus = { alias: 'quality', key: 'STATUS_COL', defaultValue: 'Status', type: 'field', sublist: 'custpage_mob_list_packages' };
    otis.custpage_mob_fld_packagebin = { alias: 'quality', key: 'BIN_COL', defaultValue: 'Bin', type: 'field', sublist: 'custpage_mob_list_packages' };
    otis.custpage_mob_fld_packagequantity = { alias: 'quality', key: 'PLANNEDQTY_COL', defaultValue: 'Planned Qty', type: 'field', sublist: 'custpage_mob_list_packages' };
    otis.custpage_mob_fld_packagebuilt = { alias: 'quality', key: 'BUILTQTY_COL', defaultValue: 'Built Qty', type: 'field', sublist: 'custpage_mob_list_packages' };
    otis.custpage_mob_fld_packagedate = { alias: 'quality', key: 'BUILTDATE_COL', defaultValue: 'Built Date', type: 'field', sublist: 'custpage_mob_list_packages' };
    otis.custpage_mob_fld_packagewocompletion = { alias: 'quality', key: 'WOCOMPLETION_COL', defaultValue: 'WO Completion', type: 'field', sublist: 'custpage_mob_list_packages' };
}

function addOtisForButtons(otis) {
    otis.submitButton = { alias: 'quality', key: 'CONTINUE_BTN', defaultValue: 'submitButton', type: 'button' };
    otis.custpage_mob_btn_printresults = { alias: 'quality', key: 'PRINT_MEASURES_BTN', defaultValue: 'print results', type: 'button' };
}

function addOtisForFreeText(otis) {
    otis.startQuality = { alias: 'quality', key: 'START_QUALITY_TEXT', defaultValue: 'Start', type: 'freetext' };
    otis.continueQuality = { alias: 'quality', key: 'CONTINUE_QUALITY_TEXT', defaultValue: 'Continue', type: 'freetext' };
    otis.view = { alias: 'quality', key: 'VIEW_TEXT', defaultValue: 'View', type: 'freetext' };
    otis.edit = { alias: 'quality', key: 'EDIT_TEXT', defaultValue: 'Edit', type: 'freetext' };
}

/**
 * Create header fields
 * @param {*} form 
 */
function getFormTemplateHeader(form, translationInfo) {
    // Field custpage_mob_workorderid
    let workOrderField = form.addField({
    	id: 'custpage_mob_workorderid',
    	label: 'custpage_mob_workorderid',
    	type: UISERVERWIDGETMODULE.FieldType.SELECT,
    	source: 'transaction'
    });
    workOrderField.label = applyTranslation(translationInfo, workOrderField.id);
    workOrderField.updateDisplayType({ displayType: UISERVERWIDGETMODULE.FieldDisplayType.INLINE });

    // Field custpage_mob_customer
	let customerField = form.addField({
		id: 'custpage_mob_customer',
		label: 'custpage_mob_customer',
		type: UISERVERWIDGETMODULE.FieldType.SELECT,
		source: 'customer'
	});
    customerField.label = applyTranslation(translationInfo, customerField.id);
	customerField.updateDisplayType({
		displayType: UISERVERWIDGETMODULE.FieldDisplayType.INLINE
	});
	
    // Field custpage_mob_assemblyitem
	let assemblyItemField = form.addField({
		id: 'custpage_mob_assemblyitem',
		label: 'custpage_mob_assemblyitem',
		type: UISERVERWIDGETMODULE.FieldType.SELECT,
		source: 'item'
	});
    assemblyItemField.label = applyTranslation(translationInfo, assemblyItemField.id);
	assemblyItemField.updateDisplayType({ displayType: UISERVERWIDGETMODULE.FieldDisplayType.INLINE });
	
    // Field custpage_mob_manufrouting
	let manufRoutingField = form.addField({
		id: 'custpage_mob_manufrouting',
		label: 'custpage_mob_manufrouting',
		type: UISERVERWIDGETMODULE.FieldType.SELECT,
		source: 'manufacturingrouting'
	});
    manufRoutingField.label = applyTranslation(translationInfo, manufRoutingField.id);
	manufRoutingField.updateDisplayType({ displayType: UISERVERWIDGETMODULE.FieldDisplayType.INLINE });

    // Field custpage_mob_quantity
	let quantityField = form.addField({
		id: 'custpage_mob_quantity',
		label: 'custpage_mob_quantity',
		type: UISERVERWIDGETMODULE.FieldType.INTEGER
	});
    quantityField.label = applyTranslation(translationInfo, quantityField.id);
	quantityField.updateDisplayType({ displayType: UISERVERWIDGETMODULE.FieldDisplayType.INLINE });
    
    // Field custpage_mob_built
	let builtField = form.addField({
		id: 'custpage_mob_built',
		label: 'custpage_mob_built',
		type: UISERVERWIDGETMODULE.FieldType.INTEGER
	});
    builtField.label = applyTranslation(translationInfo, builtField.id);
	builtField.updateDisplayType({ displayType: UISERVERWIDGETMODULE.FieldDisplayType.INLINE });

    // Hidden fields for process    
	let disableQualityLinksField = form.addField({
		id: 'custpage_mob_disablequalitylinks',
		label: 'disable quality links',
		type: UISERVERWIDGETMODULE.FieldType.CHECKBOX
	});
	disableQualityLinksField.updateDisplayType({ displayType: UISERVERWIDGETMODULE.FieldDisplayType.HIDDEN });
}

function getFormtemplateSummary(form, translationInfo) {
    log.debug({
        title: 'function started',
        details: 'getFormtemplateSummary'
    });

    // Summary sublist displayed in the summary tab
    // Add Tab
    let summaryTab = form.addTab({
        id: 'custpage_mob_tab_summary',
        label: 'summary'
    });
    summaryTab.label = applyTranslation(translationInfo, 'custpage_mob_tab_summary');
    // Add Sublist
    let summarySublist = form.addSublist({
        id: 'custpage_mob_list_summary',
        label: 'summary',
        tab: 'custpage_mob_tab_summary',
        type: UISERVERWIDGETMODULE.SublistType.LIST
    });
    summarySublist.label = applyTranslation(translationInfo, 'custpage_mob_list_summary');

    // Add Sublist Fields
    let siloIdField = summarySublist.addField({
        id: 'custpage_mob_fld_summarysiloid',
        label: 'custpage_mob_fld_summarysiloid',
        type: UISERVERWIDGETMODULE.FieldType.TEXT
    });
    siloIdField.label = applyTranslation(translationInfo, 'custpage_mob_fld_summarysiloid');
    siloIdField.updateDisplayType({ displayType: UISERVERWIDGETMODULE.FieldDisplayType.HIDDEN });

    let siloNumberField = summarySublist.addField({
        id: 'custpage_mob_fld_summarysilonumber',
        label: 'custpage_mob_fld_summarysilonumber',
        type: UISERVERWIDGETMODULE.FieldType.TEXT
    });
    siloNumberField.label = applyTranslation(translationInfo, 'custpage_mob_fld_summarysilonumber');

    let inventoryNumberField = summarySublist.addField({
        id: 'custpage_mob_fld_summaryinventorynumber',
        label: 'custpage_mob_fld_summaryinventorynumber',
        type: UISERVERWIDGETMODULE.FieldType.TEXT
    });
    inventoryNumberField.label = applyTranslation(translationInfo, 'custpage_mob_fld_summaryinventorynumber');

    let summaryQuantityField = summarySublist.addField({
        id: 'custpage_mob_fld_summaryquantity',
        label: 'custpage_mob_fld_summaryquantity',
        type: UISERVERWIDGETMODULE.FieldType.TEXT
    });
    summaryQuantityField.label = applyTranslation(translationInfo, 'custpage_mob_fld_summaryquantity');

    let summaryBuiltField = summarySublist.addField({
        id: 'custpage_mob_fld_summarybuilt',
        label: 'custpage_mob_fld_summarybuilt',
        type: UISERVERWIDGETMODULE.FieldType.TEXT
    });
    summaryBuiltField.label = applyTranslation(translationInfo, 'custpage_mob_fld_summarybuilt');

    let summaryQualityStatusField = summarySublist.addField({
        id: 'custpage_mob_fld_summaryqualitystatus',
        label: 'custpage_mob_fld_summaryqualitystatus',
        type: UISERVERWIDGETMODULE.FieldType.SELECT,
        source: 'customlist_mob_qualityglobalstatus'
    });
    summaryQualityStatusField.label = applyTranslation(translationInfo, 'custpage_mob_fld_summaryqualitystatus');
    summaryQualityStatusField.updateDisplayType({ displayType: UISERVERWIDGETMODULE.FieldDisplayType.INLINE});

    let summaryQualityLinkField = summarySublist.addField({
        id: 'custpage_mob_fld_summaryqualitylink',
        label: 'custpage_mob_fld_summaryqualitylink',
        // type: UISERVERWIDGETMODULE.FieldType.URL
        type: UISERVERWIDGETMODULE.FieldType.TEXTAREA
    });
    summaryQualityLinkField.label = applyTranslation(translationInfo, 'custpage_mob_fld_summaryqualitylink');

    log.debug({
        title: 'function done',
        details: 'getFormtemplateSummary'
    });
}

function getFormTemplateDetails(form, silos, translationInfo) {
    // Detail sublists displayed in the summary tab - one detail per silo
    form.addTab({
        id: 'custpage_mob_tab_detail',
        label: 'detail'
    });
    form.getTab({ id : 'custpage_mob_tab_detail' }).label = applyTranslation(translationInfo, 'custpage_mob_tab_detail');
    silos.map(x => x.siloNumber).forEach(siloNumber => {
        getFormTemplateSiloDetails(form, siloNumber, translationInfo);
    })
}

function getFormTemplateSiloDetails(form, siloNumber, translationInfo) {
    // Add Subtab
    let subtabId = `custpage_mob_subtab_silo${siloNumber}`;
    let subtab = form.addSubtab({
        id: subtabId,
        label: siloNumber,
        tab: 'custpage_mob_tab_detail'
    });
    subtab.label = `${applyTranslation(translationInfo, 'custpage_mob_subtab_silo')} ${siloNumber}`;

    // Add Subtab Fields
    let siloIdField = form.addField({
        id: `custpage_mob_detailsiloid${siloNumber}`,
        label: 'custpage_mob_detailsiloid',
        type: UISERVERWIDGETMODULE.FieldType.TEXT,
        container: subtabId
    });
    siloIdField.label = applyTranslation(translationInfo, 'custpage_mob_detailsiloid');
    siloIdField.updateDisplayType({ displayType: UISERVERWIDGETMODULE.FieldDisplayType.HIDDEN });

    let siloNumberField = form.addField({
        id: `custpage_mob_detailsilonumber${siloNumber}`,
        label: 'custpage_mob_detailsilonumber',
        type: UISERVERWIDGETMODULE.FieldType.TEXT,
        container: subtabId
    });
    siloNumberField.label = applyTranslation(translationInfo, 'custpage_mob_detailsilonumber');
	siloNumberField.updateDisplayType({ displayType: UISERVERWIDGETMODULE.FieldDisplayType.INLINE });

    let inventoryNumberField = form.addField({
        id: `custpage_mob_detailinventorynumber${siloNumber}`,
        label: 'custpage_mob_detailinventorynumber',
        type: UISERVERWIDGETMODULE.FieldType.TEXT,
        container: subtabId
    });
    inventoryNumberField.label = applyTranslation(translationInfo, 'custpage_mob_detailinventorynumber');
	inventoryNumberField.updateDisplayType({ displayType: UISERVERWIDGETMODULE.FieldDisplayType.INLINE });

    let quantityPlannedField = form.addField({
        id: `custpage_mob_detailquantity${siloNumber}`,
        label: 'custpage_mob_detailquantity',
        type: UISERVERWIDGETMODULE.FieldType.TEXT,
        container: subtabId
    });
    quantityPlannedField.label = applyTranslation(translationInfo, 'custpage_mob_detailquantity');
	quantityPlannedField.updateDisplayType({ displayType: UISERVERWIDGETMODULE.FieldDisplayType.INLINE });

    let quantityBuiltField = form.addField({
        id: `custpage_mob_detailbuilt${siloNumber}`,
        label: 'custpage_mob_detailbuilt',
        type: UISERVERWIDGETMODULE.FieldType.TEXT,
        container: subtabId
    });
    quantityBuiltField.label = applyTranslation(translationInfo, 'custpage_mob_detailbuilt');
	quantityBuiltField.updateDisplayType({ displayType: UISERVERWIDGETMODULE.FieldDisplayType.INLINE });

    let qualityStatusField = form.addField({
        id: `custpage_mob_detailqualitystatus${siloNumber}`,
        label: 'custpage_mob_detailqualitystatus',
        // type: UISERVERWIDGETMODULE.FieldType.SELECT,
        // source: 'customlist_mob_qualityglobalstatus',
        type: UISERVERWIDGETMODULE.FieldType.TEXT,
        container: subtabId
    });
    qualityStatusField.label = applyTranslation(translationInfo, 'custpage_mob_detailqualitystatus');
	qualityStatusField.updateDisplayType({ displayType: UISERVERWIDGETMODULE.FieldDisplayType.INLINE });

    let qualityLinkField = form.addField({
        id: `custpage_mob_detailqualitylink${siloNumber}`,
        label: 'custpage_mob_detailqualitylink',
        // type: UISERVERWIDGETMODULE.FieldType.URL,
        type: UISERVERWIDGETMODULE.FieldType.TEXTAREA,
        container: subtabId
    });
    qualityLinkField.label = applyTranslation(translationInfo, 'custpage_mob_detailqualitylink');
	qualityLinkField.updateDisplayType({ displayType: UISERVERWIDGETMODULE.FieldDisplayType.INLINE });

    getFormTemplatePackages(form, siloNumber, translationInfo);
}

function getFormTemplatePackages(form, siloNumber, translationInfo) {
    let subtabId = `custpage_mob_subtab_silo${siloNumber}`;

    // Add Sublist
    let sublistId = `custpage_mob_list_packages${siloNumber}`;
    let detailSublist = form.addSublist({
        id: sublistId,
        label: siloNumber,
        tab: subtabId,
        type: UISERVERWIDGETMODULE.SublistType.LIST
    });
    detailSublist.label = `${applyTranslation(translationInfo, 'custpage_mob_list_packages')} ${siloNumber}`;

    // Add Sublist Fields
    let packageIdField = detailSublist.addField({
        id: 'custpage_mob_fld_packageid',
        label: 'custpage_mob_fld_packageid',
        type: UISERVERWIDGETMODULE.FieldType.TEXT
    });
    packageIdField.label = applyTranslation(translationInfo, 'custpage_mob_fld_packageid');
    packageIdField.updateDisplayType({ displayType: UISERVERWIDGETMODULE.FieldDisplayType.HIDDEN });

    let packageNumberField = detailSublist.addField({
        id: 'custpage_mob_fld_packagenumber',
        label: 'custpage_mob_fld_packagenumber',
        type: UISERVERWIDGETMODULE.FieldType.TEXT
    });
    packageNumberField.label = applyTranslation(translationInfo, 'custpage_mob_fld_packagenumber');

    let inventoryStatusField = detailSublist.addField({
        id: 'custpage_mob_fld_packageinventorystatus',
        label: 'custpage_mob_fld_packageinventorystatus',
    	type: UISERVERWIDGETMODULE.FieldType.SELECT,
    	source: 'inventorystatus'
    });
    inventoryStatusField.label = applyTranslation(translationInfo, 'custpage_mob_fld_packageinventorystatus');
    inventoryStatusField.updateDisplayType({ displayType: UISERVERWIDGETMODULE.FieldDisplayType.INLINE });

    let binField = detailSublist.addField({
        id: 'custpage_mob_fld_packagebin',
        label: 'custpage_mob_fld_packagebin',
    	type: UISERVERWIDGETMODULE.FieldType.SELECT,
    	source: 'bin'
    });
    binField.label = applyTranslation(translationInfo, 'custpage_mob_fld_packagebin');
    binField.updateDisplayType({ displayType: UISERVERWIDGETMODULE.FieldDisplayType.INLINE });

    let quantityPlannedField = detailSublist.addField({
        id: 'custpage_mob_fld_packagequantity',
        label: 'custpage_mob_fld_packagequantity',
        type: UISERVERWIDGETMODULE.FieldType.TEXT
    });
    quantityPlannedField.label = applyTranslation(translationInfo, 'custpage_mob_fld_packagequantity');

    let quantityBuiltField = detailSublist.addField({
        id: 'custpage_mob_fld_packagebuilt',
        label: 'custpage_mob_fld_packagebuilt',
        type: UISERVERWIDGETMODULE.FieldType.TEXT
    });
    quantityBuiltField.label = applyTranslation(translationInfo, 'custpage_mob_fld_packagebuilt');

    let dateField = detailSublist.addField({
        id: 'custpage_mob_fld_packagedate',
        label: 'custpage_mob_fld_packagedate',
        type: UISERVERWIDGETMODULE.FieldType.DATE
    });
    dateField.label = applyTranslation(translationInfo, 'custpage_mob_fld_packagedate');

    let woCompletionField = detailSublist.addField({
        id: 'custpage_mob_fld_packagewocompletion',
        label: 'custpage_mob_fld_packagewocompletion',
        type: UISERVERWIDGETMODULE.FieldType.SELECT,
        source: 'transaction'
    });
    woCompletionField.label = applyTranslation(translationInfo, 'custpage_mob_fld_packagewocompletion');
    woCompletionField.updateDisplayType({ displayType: UISERVERWIDGETMODULE.FieldDisplayType.INLINE });
}

function retrieveData(workOrderId, translationInfo) {
    let defaultStatus = retrieveNotStartedStatus();

    let suiteQLResults = QUERYMODULE.runSuiteQL({
		query: `select t2.id as silo, 
                    t2.custrecord_mob_cmpl_silo as silonumber, 
                    t3.id as qualityresultset, 
                    t3.custrecord_mob_resultsetglobalstatus as qualitystatus, t4.name as qualitystatustext, 
                    t1.id as package, 
                    t1.custrecord_mob_cmpl_packagenumber as packagenumber, 
                    t1.custrecord_mob_cmpl_lotnumber as inventorynumber, 
                    t1.custrecord_mob_cmpl_traceability as traceability, 
                    t1.custrecord_mob_cmpl_lotstatus as inventorystatus, t6.name as inventorystatustext, 
                    t1.custrecord_mob_cmpl_bin as bin, t7.binnumber as bintext, 
                    t1.custrecord_mob_cmpl_quantity as quantityplanned, 
                    t1.custrecord_mob_cmpl_quantityreceived as quantitybuilt, 
                    t5.id as wocompletion, 
                    t5.trandate as wocompletiondate           
                from customrecord_mob_wo_pre_completion t1
                    Inner join customrecord_mob_wo_productionsilo t2
                      on t2.id = t1.custrecord_mob_cmpl_siloid
                    left outer join customrecord_mob_qualityresultset t3
                      on t3.id = t2.custrecord_mob_cmpl_qualresultset
                    left outer join customlist_mob_qualityglobalstatus t4
                      on t4.id = t3.custrecord_mob_resultsetglobalstatus
                    left outer join transaction t5
                      on t5.id = t1.custrecord_mob_cmpl_wocompletion
                    left outer join inventorystatus t6
                      on t6.id = t1.custrecord_mob_cmpl_lotstatus
                    left outer join bin t7
                      on t7.id = t1.custrecord_mob_cmpl_bin
                where t2.custrecord_mob_cmpl_silowonumber = ${workOrderId}`
	}).asMappedResults();
    // logRecord(suiteQLResults, 'suiteQLResults');
    let silos = [];
    suiteQLResults.forEach(result => {
        let siloId = result.silo;
        let precompletion = {
            internalId: result.package,
            inventoryNumber: result.inventorynumber,
            packageNumber: result.packagenumber,
            traceability: result.traceability,
            inventoryStatus: result.inventorystatus,
            inventoryStatusText: result.inventorystatustext,
            bin: result.bin,
            binText: result.bintext,
            quantityPlanned: parseInt(result.quantityplanned || 0, 10),
            quantityBuilt: parseInt(result.quantitybuilt || 0, 10),
            woCompletion: result.wocompletion,
            woCompletionDate: result.wocompletiondate
        };
        let silo = findSilo(silos, siloId);
        if (silo) {
            silo.precompletions.push(precompletion);
            silo.quantityPlanned += precompletion.quantityPlanned;
            silo.quantityBuilt += precompletion.quantityBuilt;
        } else {
            let qualityResultSet = result.qualityresultset;
            let qualityStatus = result.qualitystatus || defaultStatus.value;
            let qualityNotStarted = qualityStatus == defaultStatus.value;
            let linkTexts = [];
            if (qualityNotStarted) {
                linkTexts.push({ text: applyTranslation(translationInfo, 'startQuality'), editMode: true });
            } else {
                linkTexts.push({ text: applyTranslation(translationInfo, 'view'), editMode: false });
                linkTexts.push({ text: applyTranslation(translationInfo, 'edit'), editMode: true });
            }
            silo = {
                siloId: siloId,
                siloNumber: result.silonumber,
                inventoryNumber: precompletion.inventoryNumber,
                quantityPlanned: precompletion.quantityPlanned,
                quantityBuilt: precompletion.quantityBuilt,
                qualityResultSet: qualityResultSet,
                qualityStatus: qualityStatus,
                qualityStatusText: result.qualitystatustext || defaultStatus.text,
                qualityNotStarted: qualityNotStarted,
                qualityLinks: generateQualityResultsLinks(siloId, linkTexts),
                qualityLinkTexts: linkTexts,
                precompletions: [precompletion]
            };
            silos.push(silo);
        }
    });

    // logRecord(silos, 'silos');
    return silos;
}

/**
 * Obsolete because search does not allow all required joins
 * @param {*} workOrderId 
 * @returns 
 */
function retrieveDataOld(workOrderId) {
    let defaultStatus = retrieveNotStartedStatus();
    // logVar(defaultStatus, 'defaultStatus');

	let precompletionSearchObj = SEARCHMODULE.create({
		type: 'customrecord_mob_wo_pre_completion',
		columns: ['internalid', 'custrecord_mob_cmpl_lotnumber', 'custrecord_mob_cmpl_packagenumber', 'custrecord_mob_cmpl_traceability', 
			'custrecord_mob_cmpl_bin', 'custrecord_mob_cmpl_quantity', 'custrecord_mob_cmpl_quantityreceived', 'custrecord_mob_cmpl_siloid', 
			'custrecord_mob_cmpl_siloid.custrecord_mob_cmpl_silo', 'custrecord_mob_cmpl_wonumber', 'custrecord_mob_cmpl_lotstatus', 
			'custrecord_mob_cmpl_wocompletion', 'custrecord_mob_cmpl_qualityresultset', 'custrecord_mob_cmpl_qualityresultset.custrecord_mob_resultsetglobalstatus',
            'custrecord_mob_cmpl_wocompletion.trandate'
        ],
		filters: [
            ["custrecord_mob_cmpl_wonumber","anyof",workOrderId],
            "AND", 
            ["custrecord_mob_cmpl_wonumber.mainline","is","T"], 
            "AND", 
            [
                ["custrecord_mob_cmpl_wocompletion","noneof","@NONE@"],
                "AND",["custrecord_mob_cmpl_wocompletion.mainline","is","T"],
                "OR",["custrecord_mob_cmpl_wocompletion","anyof","@NONE@"]
            ]
        ]
	});
    let silos = [];
    precompletionSearchObj.run().each(function(result) {
        // logRecord(result, 'result');
        let siloId = result.getValue({ name: 'custrecord_mob_cmpl_siloid' });
        let precompletion = {
            internalId: result.getValue({ name: 'internalid' }),
            inventoryNumber: result.getValue({ name: 'custrecord_mob_cmpl_lotnumber' }),
            packageNumber: result.getValue({ name: 'custrecord_mob_cmpl_packagenumber' }),
            traceability: result.getValue({ name: 'custrecord_mob_cmpl_traceability' }),
            inventoryStatus: result.getValue({ name: 'custrecord_mob_cmpl_lotstatus' }),
            inventoryStatusText: result.getText({ name: 'custrecord_mob_cmpl_lotstatus' }),
            bin: result.getValue({ name: 'custrecord_mob_cmpl_bin' }),
            binText: result.getText({ name: 'custrecord_mob_cmpl_bin' }),
            quantityPlanned: parseInt(result.getValue({ name: 'custrecord_mob_cmpl_quantity' }) || 0, 10),
            quantityBuilt: parseInt(result.getValue({ name: 'custrecord_mob_cmpl_quantityreceived' }) || 0, 10),
            woCompletion: result.getValue({ name: 'custrecord_mob_cmpl_wocompletion' }),
            woCompletionDate: result.getValue({ name: 'trandate', join: 'custrecord_mob_cmpl_wocompletion' })
        };
        let silo = findSilo(silos, siloId);
        if (silo) {
            silo.precompletions.push(precompletion);
            silo.quantityPlanned += precompletion.quantityPlanned;
            silo.quantityBuilt += precompletion.quantityBuilt;
        } else {
            let qualityResultSet = result.getValue({ name: 'custrecord_mob_cmpl_qualityresultset' });
            silo = {
                siloId: siloId,
                siloNumber: result.getValue({ name: 'custrecord_mob_cmpl_silo', join: 'custrecord_mob_cmpl_siloid' }),
                inventoryNumber: precompletion.inventoryNumber,
                quantityPlanned: precompletion.quantityPlanned,
                quantityBuilt: precompletion.quantityBuilt,
                qualityResultSet: qualityResultSet,
                qualityStatus: result.getValue({ name: 'custrecord_mob_resultsetglobalstatus', join: 'custrecord_mob_cmpl_qualityresultset' }) || defaultStatus.value,
                qualityStatusText: result.getText({ name: 'custrecord_mob_resultsetglobalstatus', join: 'custrecord_mob_cmpl_qualityresultset' }) || defaultStatus.text,
                qualityResultsLink: generateQualityResultsLinks(siloId),
                precompletions: [precompletion]
            }
            silos.push(silo);
        }
		return true;
	});
    return silos;
}

function generateQualityResultsLinks(productionSilo, linkTexts) {
    // Generate the link to the suitelet in charge to initialise/update the resultset and to redirect to it
    return linkTexts.map( x => { 
        return generateQualityResultsLink(productionSilo, x.editMode);
    });
}

function generateQualityResultsLink(productionSilo, editMode) {
    return URLMODULE.resolveScript({
        scriptId: 'customscript_mob_sl_intializeresultset',
        deploymentId: 'customdeploy_mob_sl_intializeresultset',
        params: { 
            custparam_mob_productionsilo: productionSilo,
            custparam_mob_editmode: editMode ? 'T' : 'F'
        }
    });
}

function retrieveNotStartedStatus() {
    let searchObj = SEARCHMODULE.create({
        type: 'customlist_mob_qualityglobalstatus',
        filters: [['scriptid', 'is', 'VAL_260840_5028481_SB1_408']],
        columns: ['internalid', 'name']
    });
    let searchResult = searchObj.run().getRange({ start: 0, end: 1 })[0];
    return {
        value: searchResult.getValue({ name: 'internalid' }),
        text: searchResult.getValue({ name: 'name' })
    };
}

function findSilo(silos, siloId) {
	for (let i = 0 ; i < silos.length ; i++) {
		if (silos[i].siloId == siloId) {
			return silos[i];
		}		
	}
}

function populateFormTemplateHeader(form, workOrderId, translationInfo) {
    log.debug({
        title: 'function started',
        details: 'populateFormTemplateHeader'
    });

    // Read informations from work order
	let woFieldsValues = SEARCHMODULE.lookupFields({
		type: SEARCHMODULE.Type.WORK_ORDER,
		id: workOrderId,
		columns: ['tranid', 'item', 'quantity', 'built', 'location', 'entity', 'manufacturingrouting', 
        'status', 'subsidiary'] //['assemblyitem', 'quantity']
	});
	// log.debug({
	// 	title: 'woFieldsValues',
	// 	details: woFieldsValues
	// });
    form.getField({ id: 'custpage_mob_workorderid' }).defaultValue = workOrderId;
    form.getField({ id: 'custpage_mob_quantity' }).defaultValue = woFieldsValues.quantity;
    form.getField({ id: 'custpage_mob_built' }).defaultValue = woFieldsValues.built;

    let entity;
	if ('entity' in woFieldsValues && woFieldsValues.entity.length > 0) {
        entity = woFieldsValues.entity[0].value;
        form.getField({ id: 'custpage_mob_customer' }).defaultValue = entity;
	}
	if ('item' in woFieldsValues && woFieldsValues.item.length > 0) {
        form.getField({ id: 'custpage_mob_assemblyitem' }).defaultValue = woFieldsValues.item[0].value;
	}
	if ('manufacturingrouting' in woFieldsValues && woFieldsValues.manufacturingrouting.length > 0) {
        form.getField({ id: 'custpage_mob_manufrouting' }).defaultValue = woFieldsValues.manufacturingrouting[0].value;
	}
	let status = {};
	if ('status' in woFieldsValues && woFieldsValues.status.length > 0) {
		status.value = woFieldsValues.status[0].value;
		status.text = woFieldsValues.status[0].text;
		// logVar(woFieldsValues.status, 'orderstatus');
	}

    let qualitySetup = QUALITYMODULE.getQualitySetup(woFieldsValues.subsidiary[0].value);
    if (qualitySetup.customerLevelFlag && !entity) {
        let warningMsgTitleField = form.addField({
            id: 'custpage_mob_warningmsgtitle',
            label: 'warning message title',
            type: UISERVERWIDGETMODULE.FieldType.TEXTAREA
        });
        warningMsgTitleField.updateDisplayType({
            displayType: UISERVERWIDGETMODULE.FieldDisplayType.HIDDEN
        });
        warningMsgTitleField.defaultValue = applyTranslation(translationInfo, 'MISSING_INFO_MSGTTL');

        let warningMsgTextField = form.addField({
            id: 'custpage_mob_warningmsgtext',
            label: 'warning message text',
            type: UISERVERWIDGETMODULE.FieldType.TEXTAREA
        });
        warningMsgTextField.updateDisplayType({
            displayType: UISERVERWIDGETMODULE.FieldDisplayType.HIDDEN
        });
        warningMsgTextField.defaultValue = applyTranslation(translationInfo, 'MISSING_INFO_MSGTXT');

        form.getField({ id: 'custpage_mob_disablequalitylinks' }).defaultValue = 'T';
    }

    log.debug({
        title: 'function done',
        details: 'populateFormTemplateHeader'
    });
}

function populateFormtemplateSummary(form, silos) {
    log.debug({
        title: 'function started',
        details: 'populateFormtemplateSummary'
    });

    let sublist = form.getSublist({ id: 'custpage_mob_list_summary' });
    if (!sublist) {
        log.debug({
            title: 'function done',
            details: 'populateFormtemplateSummary - No summary to display'
        });
        return;
    }
    let line = 0;
    silos.forEach(silo => {
        sublist.setSublistValue({
            id: 'custpage_mob_fld_summarysiloid',
            line: line,
            value: silo.siloId
        });
        sublist.setSublistValue({
            id: 'custpage_mob_fld_summarysilonumber',
            line: line,
            value: silo.siloNumber
        });
        sublist.setSublistValue({
            id: 'custpage_mob_fld_summaryinventorynumber',
            line: line,
            value: silo.inventoryNumber
        });        
        sublist.setSublistValue({
            id: 'custpage_mob_fld_summaryquantity',
            line: line,
            value: silo.quantityPlanned
        });
        sublist.setSublistValue({
            id: 'custpage_mob_fld_summarybuilt',
            line: line,
            value: silo.quantityBuilt
        });
        if (silo.qualityStatus) {
            sublist.setSublistValue({
                id: 'custpage_mob_fld_summaryqualitystatus',
                line: line,
                value: silo.qualityStatus
            });
        }
        if (silo.qualityLinks && form.getField({ id: 'custpage_mob_disablequalitylinks' }).defaultValue != 'T') {
            // Open in a new window : target="_blank"
            // `<a href= "${silo.qualityLinks}" target="_blank">Quality</a>`
            let fieldValue = '';
            let index = 0;
            silo.qualityLinkTexts.forEach(linkText => {
                if (fieldValue) {
                    fieldValue += ' / ';
                }
                fieldValue += `<a class="dottedlink" href= "${silo.qualityLinks[index++]}">${linkText.text}</a>`;

            });
            sublist.setSublistValue({
                id: 'custpage_mob_fld_summaryqualitylink',
                line: line,
                value: fieldValue
            });
        }
        line++;
    });

    log.debug({
        title: 'function done',
        details: 'populateFormtemplateSummary'
    });
}

function populateFormtemplateDetails(form, silos) {
    log.debug({
        title: 'function started',
        details: 'populateFormtemplateDetails'
    });
    
    silos.forEach(silo => {
        let siloNumber = silo.siloNumber;
        form.getField({ id: `custpage_mob_detailsiloid${siloNumber}` }).defaultValue = silo.siloId;
        form.getField({ id: `custpage_mob_detailsilonumber${siloNumber}` }).defaultValue = silo.siloNumber;
        form.getField({ id: `custpage_mob_detailinventorynumber${siloNumber}` }).defaultValue = silo.inventoryNumber;     
        form.getField({ id: `custpage_mob_detailquantity${siloNumber}` }).defaultValue = silo.quantityPlanned;
        form.getField({ id: `custpage_mob_detailbuilt${siloNumber}` }).defaultValue = silo.quantityBuilt;
        if (silo.qualityStatus) {
            form.getField({ id: `custpage_mob_detailqualitystatus${siloNumber}` }).defaultValue = silo.qualityStatusText;
        }
        if(silo.qualityLinks && form.getField({ id: 'custpage_mob_disablequalitylinks' }).defaultValue != 'T') {
            let fieldValue = '';
            let index = 0;
            silo.qualityLinkTexts.forEach(linkText => {
                if (fieldValue) {
                    fieldValue += ' / ';
                }
                fieldValue += `<a class="dottedlink" href= "${silo.qualityLinks[index++]}">${linkText.text}</a>`;
            });
            form.getField({ id: `custpage_mob_detailqualitylink${siloNumber}` }).defaultValue = fieldValue;
                // `<a class="dottedlink" href= "${silo.qualityLinks}">${silo.qualityLinkTexts}</a>`;
        }
        populateFormtemplatePackages(form, silo);
    });

    log.debug({
        title: 'function done',
        details: 'populateFormtemplateDetails'
    });
}

function populateFormtemplatePackages(form, silo) {
    log.debug({
        title: 'function started',
        details: 'populateFormtemplatePackages'
    });

    let sublist = form.getSublist({ id: `custpage_mob_list_packages${silo.siloNumber}` });
    let line = 0;
    silo.precompletions.forEach(package => {
        sublist.setSublistValue({
            id: 'custpage_mob_fld_packageid',
            line: line,
            value: package.internalId
        });
        sublist.setSublistValue({
            id: 'custpage_mob_fld_packagenumber',
            line: line,
            value: package.packageNumber
        });
        if (package.inventoryStatus) {
            sublist.setSublistValue({
                id: 'custpage_mob_fld_packageinventorystatus',
                line: line,
                value: package.inventoryStatus
            });
        }
        if (package.bin) {
            sublist.setSublistValue({
                id: 'custpage_mob_fld_packagebin',
                line: line,
                value: package.bin
            });
        }
        sublist.setSublistValue({
            id: 'custpage_mob_fld_packagequantity',
            line: line,
            value: package.quantityPlanned
        });
        sublist.setSublistValue({
            id: 'custpage_mob_fld_packagebuilt',
            line: line,
            value: package.quantityBuilt
        });
        if(package.woCompletionDate) {
            sublist.setSublistValue({
                id: 'custpage_mob_fld_packagedate',
                line: line,
                value: package.woCompletionDate
            });
        }
        if (package.woCompletion) {
            sublist.setSublistValue({
                id: 'custpage_mob_fld_packagewocompletion',
                line: line,
                value: package.woCompletion
            });
        }
        line++;
    });

    log.debug({
        title: 'function done',
        details: 'populateFormtemplatePackages'
    });
}

/**
 * 
 * @param {*} translationInfo 
 * @param {*} objectKey the scriptid of the ui object
 * @returns 
 */
function applyTranslation(translationInfo, objectKey) {
    let ti = translationInfo;
    let oti = ti.otis[objectKey];
    let translation;
    if (!oti) return `${objectKey}**`
    try {
        translation = ti.translationHandle[oti['alias']][oti['key']]();
    } catch (ex) {
        return `${oti['key']}*`;
        return `${oti['defaultValue']}*`;
    }
    if (ti.auditMode) {
        translation = `${oti['alias']}.${oti['key']}`;
    }
    return translation;
}

function retrieveTranslationKeys(otis, alias) {
    let keys = new Set();
    // for (let property in otis.filter(oti => oti.alias == alias)) {
    for (let property in otis) {
        // if (otis.hasOwnProperty(property)) {
        if (otis.hasOwnProperty(property) && otis[property]['alias'] == alias) {
            keys.add(otis[property]['key']);
        }
    }
    logRecord(keys, 'keys');
    return [...keys];
}

function logRecord(record, logTitle) {
	let logRecord = JSON.stringify(record) || 'UNDEFINED';
	let recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);		
	}
}

function logVar(value, logTitle) {
	log.debug({
		title: logTitle + ' (' + typeof value + ')',
		details: value 
	});	
}
