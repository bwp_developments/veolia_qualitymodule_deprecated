var RECORDMODULE, REDIRECTMODULE, RUNTIMEMODULE, SEARCHMODULE, UISERVERWIDGETMODULE, QUALITYMODULE;

/**
 * @NApiVersion 2.1
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/redirect', 'N/runtime', 'N/search', 'N/ui/serverWidget', './qualityModule'], runSuitelet);

function runSuitelet(record, redirect, runtime, search, uiserverWidget, qualityModule) {
	RECORDMODULE= record;
	REDIRECTMODULE= redirect;
	RUNTIMEMODULE= runtime;
	SEARCHMODULE= search;
	UISERVERWIDGETMODULE= uiserverWidget;
	QUALITYMODULE = qualityModule;
	
	var returnObj = {};
	returnObj['onRequest'] = _onRequest;
	return returnObj;	
}
   
/**
 * Definition of the Suitelet script trigger point.
 *
 * @param {Object} context
 * @param {ServerRequest} context.request - Encapsulation of the incoming request
 * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
 * @Since 2015.2
 */
function _onRequest(context) {
    var method = context.request.method;
    //logRecord(context, 'context');

	log.debug(method);
    if (method == 'GET') {
    	getFunction(context);
    } else if (method == 'POST') {
    	postFunction(context);
    }
    
  	//return;
}

function getFunction(context) {
  	var itemId = context.request.parameters.mob_item || 0;
  	var customerId = context.request.parameters.mob_customer || 0;
  	var form = getFormTemplate(customerId, itemId);
  	
  	context.response.writePage(form);
}

function postFunction(context) {
  	var itemId = context.request.parameters.custpage_mob_tolerancesetitem || 0;
  	var customerId = context.request.parameters.custpage_mob_tolerancesetcustomer || 0;
//  	var displayMode = context.request.parameters.custpage_mob_displaymode || '';
  	log.debug({
  		title: 'POST customer',
  		details: customerId
  	});
  	
	var toleranceSetId = QUALITYMODULE.getToleranceSet(itemId, customerId);
	
	log.debug('Tolerance Set is: ' + toleranceSetId);
	
  	// Redirect to the Tolerance Set form
	REDIRECTMODULE.toRecord({
		id: toleranceSetId,
		type: 'customrecord_mob_qm_toleranceset'
	});
	
	/*
	 * Test de redirect vers un Suitelet pour gérer le toleranceSet de manière personnalisée
	 * ==> problème : la liste des normes ne peut pas être personnalisée ligne par ligne en fonction du test
	 * */
//	REDIRECTMODULE.toSuitelet({
//		scriptId: 'customscript_mob_sl_qualitytoleranceset',
//		deploymentId: 'customdeploy_mob_sl_qualitytoleranceset',
//		parameters: {'mob_toleranceset':toleranceSetId}
//	});
}
 
function getFormTemplate(customerId, itemId) {
    var form = UISERVERWIDGETMODULE.createForm({
        title : 'Item Customer Choice'
    });
    form.clientScriptModulePath = 'SuiteScripts/QualityModule/mob_cs_itemcustomerchoice.js';
    
    var displayMode = '';
    
    var customerField = form.addField({
    	id: 'custpage_mob_tolerancesetcustomer',
    	label: 'Customer',
    	type: UISERVERWIDGETMODULE.FieldType.SELECT,
    	source: 'customer'
    });
    if (customerId != 0) {
    	customerField.defaultValue = customerId;
    	displayMode = 'EnterItem';
    }
    
    var itemField = form.addField({
    	id: 'custpage_mob_tolerancesetitem',
    	label: 'Item',
    	type: UISERVERWIDGETMODULE.FieldType.SELECT,
    	source: 'item'
    });
    if (itemId != 0) {
    	itemField.defaultValue = itemId;
    	displayMode = 'EnterCustomer';
    }
    
    var displayModeField = form.addField({
    	id: 'custpage_mob_displaymode',
    	label: 'Display Mode',
    	type: UISERVERWIDGETMODULE.FieldType.TEXT	
    });
    displayModeField.updateDisplayType({
		displayType: UISERVERWIDGETMODULE.FieldDisplayType.HIDDEN
	});
    displayModeField.defaultValue = displayMode;
    
    switch (displayMode) {
    	case 'EnterCustomer':
    		itemField.updateDisplayType({
    			displayType: UISERVERWIDGETMODULE.FieldDisplayType.INLINE
    		});
        	customerField.isMandatory = true;
        	break;
    	case 'EnterItem':
    		customerField.updateDisplayType({
    			displayType: UISERVERWIDGETMODULE.FieldDisplayType.INLINE
    		});
        	itemField.isMandatory = true;
        	break;
    }
    
    if (displayMode) {
        form.addSubmitButton({
            label : 'Continue'
        });    	
    }
    
    return form;
}

function logRecord(record, logTitle) {
	var logRecord = JSON.stringify(record);
	var recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);		
	}
}
