var CURRENTRECORDMODULE, SEARCHMODULE, UIDIALOGMODULE, URLMODULE;

/**
 * @NApiVersion 2.1
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */
define(['N/currentRecord', 'N/search', 'N/ui/dialog', 'N/url'], runClient);

function runClient(currentRecord, search, uidialog, url) {
	CURRENTRECORDMODULE= currentRecord;
	SEARCHMODULE= search;
	UIDIALOGMODULE= uidialog;
	URLMODULE= url;
	
	var returnObj = {};
	returnObj['pageInit'] = _pageInit;
	returnObj['fieldChanged'] = _fieldChanged;
//	returnObj['postSourcing'] = _postSourcing;
//	returnObj['sublistChanged'] = _sublistChanged;
//	returnObj['lineInit'] = _lineInit;
//	returnObj['validateField'] = _validateField;
//	returnObj['validateLine'] = _validateLine;
//	returnObj['validateInsert'] = _validateInsert;
//	returnObj['validateDelete'] = _validateDelete;
//	returnObj['saveRecord'] = _saveRecord;
	returnObj['cancelForm'] = cancelFormCall;
	returnObj['markAll'] = markAllCall;
	returnObj['unmarkAll'] = unmarkAllCall;
	return returnObj;
}
    
/**
 * Function to be executed after page is initialized.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.mode - The mode in which the record is being accessed (create, copy, or edit)
 *
 * @since 2015.2
 */
function _pageInit(scriptContext) {
	/*
	var currentRec = scriptContext.currentRecord;
	// In Suitelet script the checkbox is populated with 'T' or 'F' but the getValue method returns a boolean !!
	var woCompletionAreCreated = currentRec.getValue('custpage_mob_wocompletionarecreated');
	if (woCompletionAreCreated) {
		UIDIALOGMODULE.alert({
			title: 'Process completed',
			message: 'Work Order completion(s) are successfully created/updated'
		});
	}
	*/
//	alert('pageInit ' + scriptContext);
}

/**
 * Function to be executed when field is changed.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 * @param {string} scriptContext.fieldId - Field name
 * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
 * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
 *
 * @since 2015.2
 */
function _fieldChanged(scriptContext) {
	var message = '';
//	message = inventoryLots.length;
//	UIDIALOGMODULE.alert({
//		title: 'fieldChanged',
//		message: message
//	});
	var lineIndex = scriptContext.currentRecord.getCurrentSublistIndex({
		sublistId: 'custpage_mob_packinglist'
	});
	
	var field = scriptContext.fieldId;
	if (field != 'custpage_mob_fld_select') {
		return;
	}
	
	var selected = scriptContext.currentRecord.getCurrentSublistValue({
		sublistId: 'custpage_mob_packinglist',
		fieldId: 'custpage_mob_fld_select',
		line: lineIndex
	});
	var selectedOriginalValue = scriptContext.currentRecord.getCurrentSublistValue({
		sublistId: 'custpage_mob_packinglist',
		fieldId: 'custpage_mob_fld_selectoriginalvalue',
		line: lineIndex
	});
	var selectedOriginal = false;
	if (selectedOriginalValue) {
		selectedOriginal = true;
	}
	
	if (selected == selectedOriginal) {
		return;
	}
	
	var packingSummary = scriptContext.currentRecord.getValue('custpage_mob_packingsummary');
	var inventoryLots = JSON.parse(packingSummary.replace(/\/"/gi, '"'));
	var fulfillmentQuantity = scriptContext.currentRecord.getValue('custpage_mob_fulfillmentquantity');
	var selectedQuantity = scriptContext.currentRecord.getValue('custpage_mob_selectedquantity');
	selectedQuantity = parseInt(selectedQuantity || 0, 10);
	
	var inventoryNumber = scriptContext.currentRecord.getSublistValue({
		sublistId: 'custpage_mob_packinglist',
		fieldId: 'custpage_mob_fld_inventorynumber',
		line: lineIndex
	});
	var binNumber = scriptContext.currentRecord.getSublistValue({
		sublistId: 'custpage_mob_packinglist',
		fieldId: 'custpage_mob_fld_binnumber',
		line: lineIndex
	});
	
	var quantity = scriptContext.currentRecord.getSublistValue({
		sublistId: 'custpage_mob_packinglist',
		fieldId: 'custpage_mob_fld_packingquantity',
		line: lineIndex
	});
	quantity = parseInt(quantity || 0, 10);
	var remainingQuantity = getRemainingQuantity(inventoryLots, inventoryNumber, binNumber);
	
	var updateDisplayFlag = false;
	if (selected) {
//		// Validate quantity does not overflow the required quantity ==> this validation is no longer required
//		if (quantity > remainingQuantity) {
//			
//			UIDIALOGMODULE.alert({
//				title: 'This line can\'t be selected',
//				message: 'Total quantity on lot ' + inventoryNumber + ' is too large'
//			});
//
//			scriptContext.currentRecord.setValue({
//				fieldId: 'custpage_mob_fld_selectoriginalvalue',
//				value: ''
//			});
//			scriptContext.currentRecord.setCurrentSublistValue({
//				sublistId: 'custpage_mob_packinglist',
//				fieldId: 'custpage_mob_fld_select',
//				value: false
//			});
//		} else {
//			selectedQuantity += quantity;
//			increasePackingQuantity(inventoryLots, inventoryNumber, quantity);
//			updateDisplayFlag = true;
//			scriptContext.currentRecord.setCurrentSublistValue({
//				sublistId: 'custpage_mob_packinglist',
//				fieldId: 'custpage_mob_fld_selectoriginalvalue',
//				value: 'true'
//			});
//		}
		selectedQuantity += quantity;
		increasePackingQuantity(inventoryLots, inventoryNumber, binNumber, quantity);
		updateDisplayFlag = true;
		scriptContext.currentRecord.setCurrentSublistValue({
			sublistId: 'custpage_mob_packinglist',
			fieldId: 'custpage_mob_fld_selectoriginalvalue',
			value: 'true'
		});
	} else {
		selectedQuantity -= quantity;
		decreasePackingQuantity(inventoryLots, inventoryNumber, binNumber, quantity);
		updateDisplayFlag = true;
		scriptContext.currentRecord.setCurrentSublistValue({
			sublistId: 'custpage_mob_packinglist',
			fieldId: 'custpage_mob_fld_selectoriginalvalue',
			value: ''
		});
	}
	if (updateDisplayFlag) {
		scriptContext.currentRecord.setValue({
			fieldId: 'custpage_mob_packingsummary',
			value: JSON.stringify(inventoryLots)
		});
		scriptContext.currentRecord.setValue({
			fieldId: 'custpage_mob_packingsummarydisplay',
			value: formatPackingSummary(inventoryLots)
		});
		scriptContext.currentRecord.setValue({
			fieldId: 'custpage_mob_selectedquantity',
			value: selectedQuantity
		});
		scriptContext.currentRecord.setValue({
			fieldId: 'custpage_mob_quantity',
			value: selectedQuantity + '/' + parseInt(fulfillmentQuantity, 10).toString()
		});	
	}
}

/**
 * Function to be executed when field is slaved.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 * @param {string} scriptContext.fieldId - Field name
 *
 * @since 2015.2
 */
function _postSourcing(scriptContext) {

}

/**
 * Function to be executed after sublist is inserted, removed, or edited.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @since 2015.2
 */
function _sublistChanged(scriptContext) {
	alert('sublistChanged ' + scriptContext);
}

/**
 * Function to be executed after line is selected.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @since 2015.2
 */
function _lineInit(scriptContext) {

}

/**
 * Validation function to be executed when field is changed.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 * @param {string} scriptContext.fieldId - Field name
 * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
 * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
 *
 * @returns {boolean} Return true if field is valid
 *
 * @since 2015.2
 */
function _validateField(scriptContext) {
	alert('validateField ' + scriptContext);
}

/**
 * Validation function to be executed when sublist line is committed.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @returns {boolean} Return true if sublist line is valid
 *
 * @since 2015.2
 */
function _validateLine(scriptContext) {
	alert('validateLine ' + scriptContext);
}

/**
 * Validation function to be executed when sublist line is inserted.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @returns {boolean} Return true if sublist line is valid
 *
 * @since 2015.2
 */
function _validateInsert(scriptContext) {

}

/**
 * Validation function to be executed when record is deleted.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @returns {boolean} Return true if sublist line is valid
 *
 * @since 2015.2
 */
function _validateDelete(scriptContext) {

}

/**
 * Validation function to be executed when record is saved.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @returns {boolean} Return true if record is valid
 *
 * @since 2015.2
 */
function _saveRecord(scriptContext) {
	
}

function cancelFormCall(itemFulfillmentId) {
	var fulfillmentLinesUrl = URLMODULE.resolveScript({
		scriptId: 'customscript_mob_sl_itemfulfillmentlines',
		deploymentId: 'customdeploy_mob_sl_itemfulfillmentlines',
		params: {custparam_mob_itemfulfillment: itemFulfillmentId},
		returnExternalUrl: false
	});
	window.location = fulfillmentLinesUrl;
}

function markAllCall() {
	var rec = CURRENTRECORDMODULE.get();
	var lineCount = rec.getLineCount({
		sublistId: 'custpage_mob_packinglist'
	});
	for (var i = 0 ; i < lineCount ; i++) {
		rec.selectLine({
			sublistId: 'custpage_mob_packinglist',
			line: i	
		});
		var isLineSelected = rec.getCurrentSublistValue({
			sublistId: 'custpage_mob_packinglist',
			fieldId: 'custpage_mob_fld_select'
		});
		if (!isLineSelected) {
			rec.setCurrentSublistValue({
				sublistId: 'custpage_mob_packinglist',
				fieldId: 'custpage_mob_fld_select',
				value: true
			});			
		}		
	}
}

function unmarkAllCall() {
	var rec = CURRENTRECORDMODULE.get();
	var lineCount = rec.getLineCount({
		sublistId: 'custpage_mob_packinglist'
	});
	for (var i = 0 ; i < lineCount ; i++) {
		rec.selectLine({
			sublistId: 'custpage_mob_packinglist',
			line: i	
		});
		var isLineSelected = rec.getCurrentSublistValue({
			sublistId: 'custpage_mob_packinglist',
			fieldId: 'custpage_mob_fld_select'
		});
		if (isLineSelected) {
			rec.setCurrentSublistValue({
				sublistId: 'custpage_mob_packinglist',
				fieldId: 'custpage_mob_fld_select',
				value: false
			});			
		}		
	}
}

function getRemainingQuantity(packingSummary, inventoryNumber, binNumber) {
	var lotPacking = getLotPacking(packingSummary, inventoryNumber, binNumber);
	return parseInt(lotPacking.originalQuantity - lotPacking.packingQuantity, 10);
}

function increasePackingQuantity(packingSummary, inventoryNumber, binNumber, quantity) {
	updatePackingQuantity(packingSummary, inventoryNumber, binNumber, quantity);
}

function decreasePackingQuantity(packingSummary, inventoryNumber, binNumber, quantity) {
	updatePackingQuantity(packingSummary, inventoryNumber, binNumber, quantity*(-1));
}

function updatePackingQuantity(packingSummary, inventoryNumber, binNumber, quantity) {
	var lotPacking = getLotPacking(packingSummary, inventoryNumber, binNumber);
	lotPacking.packingQuantity += quantity;
}

function getLotPacking(packingSummary, inventoryNumber, binNumber) {
	for (var i = 0 ; i < packingSummary.length ; i++) {
		if (packingSummary[i].inventoryNumber == inventoryNumber &&
				packingSummary[i].binNumber == binNumber) {
			return packingSummary[i];
		}
	}
	return;
}

function formatPackingSummary(packingSummary) {
	var formattedText = '';
	var packingArray = packingSummary || [];
	for (var i = 0 ; i < packingArray.length ; i++) {
		if (i > 0) {
			formattedText += '<br>';
		}
		formattedText += packingArray[i].inventoryNumberDisplay + ' | ' + packingArray[i].binNumberDisplay + ' : ' + packingArray[i].packingQuantity + '/' + packingArray[i].originalQuantity;
	}
	return formattedText;
}

function logRecord(record, logTitle) {
	var logRecord = JSON.stringify(record);
	var recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);		
	}
}
