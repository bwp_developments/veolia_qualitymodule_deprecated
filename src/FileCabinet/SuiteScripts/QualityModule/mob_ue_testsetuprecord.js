var RECORDMODULE, SEARCHMODULE;

/**
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/search'], runUserEvent);

function runUserEvent(record, search) {
	RECORDMODULE= record;
	SEARCHMODULE= search;
	
	var returnObj = {};
	//returnObj['beforeLoad'] = _beforeLoad;
	returnObj['beforeSubmit'] = _beforeSubmit;
	//returnObj['afterSubmit'] = _afterSubmit;
	return returnObj;
}
   
/**
 * Function definition to be triggered before record is loaded.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {string} scriptContext.type - Trigger type
 * @param {Form} scriptContext.form - Current form
 * @Since 2015.2
 */
function _beforeLoad(scriptContext) {

}

/**
 * Function definition to be triggered before record is loaded.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {Record} scriptContext.oldRecord - Old record
 * @param {string} scriptContext.type - Trigger type
 * @Since 2015.2
 */
function _beforeSubmit(scriptContext) {
	log.debug({
		title: 'BeforeSubmit event started'
	});
	
	qualityTestRecord = scriptContext.newRecord;
	
	// Read values for norms field. It is a multi select field. In case it contains a unique value, update the 
	// default norm field with this value
	var norms = qualityTestRecord.getValue('custrecord_mob_testsetup_norm');
	
	/*
	log.debug({
		title: 'Norms',
		details: norms
	});
	log.debug({
		title: 'Number of Norms',
		details: norms.length
	});
	*/
	
	if (norms.length == 1) {
		qualityTestRecord.setValue({
			fieldId: 'custrecord_mob_testsetup_defaultnorm',
			value: norms[0]
		});
		
		// Read values for operating modes field on norm record. It is a multi select field. In case it contains a unique value, update the 
		// default operating mode field with this value

		/*
		 * The use of search.lookupFields requires less governance units
		normRecord = RECORDMODULE.load({
			type: 'customrecord_mob_qm_normsetup',
			id: norms[0]
		});
		var operatingModes = normRecord.getValue('custrecord_mob_normsetup_operatingmode');
		if (operatingModes.length == 1) {
			qualityTestRecord.setValue({
				fieldId: 'custrecord_mob_testsetup_defaultopmode',
				value: operatingModes[0]
			});			
		}
		*/
		var fieldsValues = SEARCHMODULE.lookupFields({
			type: 'customrecord_mob_qm_normsetup',
			id: norms[0],
			columns: 'custrecord_mob_normsetup_operatingmode'
		});
		var operatingModes = fieldsValues['custrecord_mob_normsetup_operatingmode'];
		var defaultValue = '';
		if (operatingModes.length == 1) {
			defaultValue = operatingModes[0].value;		
		}
		qualityTestRecord.setValue({
			fieldId: 'custrecord_mob_testsetup_defaultopmode',
			value: defaultValue
		});	
	}
	
	log.debug({
		title: 'BeforeSubmit event completed'
	});
}

/**
 * Function definition to be triggered before record is loaded.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {Record} scriptContext.oldRecord - Old record
 * @param {string} scriptContext.type - Trigger type
 * @Since 2015.2
 */
function _afterSubmit(scriptContext) {

}
