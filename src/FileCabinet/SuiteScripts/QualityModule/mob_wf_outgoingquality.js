var SEARCHMODULE, QUALITYMODULE;

/*
 * Worflow script for WO Completion record. It is in charge to initialize the quality result set
 */

/**
 * @NApiVersion 2.1
 * @NScriptType workflowactionscript
 */
define(['N/search', './qualityModule'], runWorkflow)

function runWorkflow(search, qualityModule) {
	SEARCHMODULE= search;
	QUALITYMODULE= qualityModule;
	
	var returnObj = {};
	returnObj['onAction'] = _onAction;
	return returnObj;    
}

/**
 * Definition of the Suitelet script trigger point.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {Record} scriptContext.oldRecord - Old record
 * @Since 2016.1
 */
function _onAction(scriptContext) {
	// Read step to launch form parameters
	var script = RUNTIMEMODULE.getCurrentScript();
	var requestedAction = script.getParameter({
		name: 'custscript_mob_wf_requestedaction'
	});
	log.debug({
		title: 'Requested action',
		details: requestedAction
	});
	
	var resultSetRecord = scriptContext.newRecord;
	var qualityResultSet = resultSetRecord.getValue('id');
	
	var returnResultSetId;
	
	switch (requestedAction) {
	case 'FINALIZE_RESULTSET':
		returnResultSetId = qualityResultSet;
		QUALITYMODULE.finalizeResultSet(qualityResultSet);
		break;   		
	}
	
	return returnResultSetId;	
}

function logRecord(record, logTitle) {
	var logRecord = JSON.stringify(record);
	var recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);		
	}
}

function logVar(value, title) {
	log.debug({
		title: title + ' (' + typeof value + ')',
		details: value 
	});	
}
