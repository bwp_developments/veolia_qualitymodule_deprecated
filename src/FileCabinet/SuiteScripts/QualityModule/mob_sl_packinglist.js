var HTTPSMODULE, RECORDMODULE, RUNTIMEMODULE, SEARCHMODULE, TRANSLATIONMODULE, UISERVERWIDGETMODULE;

/**
 * @NApiVersion 2.1
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['N/https', 'N/record', 'N/runtime', 'N/search', 'N/translation', 'N/ui/serverWidget'], runSuitelet);

function runSuitelet(https, record, runtime, search, translation, uiserverWidget) {
	HTTPSMODULE= https;
	RECORDMODULE= record;
	RUNTIMEMODULE= runtime;
	SEARCHMODULE= search;
	TRANSLATIONMODULE= translation;
	UISERVERWIDGETMODULE= uiserverWidget;
//	QUALITYMODULE = qualityModule;
	
	var returnObj = {};
	returnObj['onRequest'] = _onRequest;
	return returnObj;
}
   
/**
 * Definition of the Suitelet script trigger point.
 *
 * @param {Object} context
 * @param {ServerRequest} context.request - Encapsulation of the incoming request
 * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
 * @Since 2015.2
 */
function _onRequest(context) {
    var method = context.request.method;
    //logRecord(context, 'context');

	log.debug(method);
    if (method == 'GET') {
    	getFunction(context);
    } else if (method == 'POST') {
    	postFunction(context);
    }
}

function getFunction(context) {
	// Parameters are 
	// - internalid of item fulfillment
	// - fulfillment line number
	var itemFulfillmentId = context.request.parameters.custparam_mob_itemfulfillment || 0;
	var fulfillmentLine = context.request.parameters.custparam_mob_fulfillmentline || -1;
	
	// For testing purpose
//	itemFulfillmentId = 35093;
//	fulfillmentLine = 0;
  	var form = getFormTemplate(itemFulfillmentId, fulfillmentLine, false);
  	
  	context.response.writePage(form);
}

function postFunction(context) {
	log.debug('postFunction started');
	var delimiter = /\u0001/;
	var lineDelimiter = /\u0002/;
	
	var fulfillmentId = context.request.parameters.custpage_mob_fulfillmentid;
	var fulfillmentLine = parseInt(context.request.parameters.custpage_mob_fulfillmentline || 0, 10);
	
	var resultColumns = context.request.parameters.custpage_mob_packinglistfields.split(delimiter);
	var packingListLines = context.request.parameters.custpage_mob_packinglistdata.split(lineDelimiter);

	var fieldsValues = SEARCHMODULE.lookupFields({
		type: SEARCHMODULE.Type.ITEM_FULFILLMENT,
		id: fulfillmentId,
		columns: ['entity', 'createdfrom']
	});
	var customerId = '';
	if ('entity' in fieldsValues && fieldsValues.entity.length > 0) {
		customerId = fieldsValues.entity[0].value;		
	}
	
	var toLocation = '';
	if ('createdfrom' in fieldsValues && fieldsValues['createdfrom'].length > 0) {
		var createdFromId = fieldsValues['createdfrom'][0].value;
		if (createdFromId) {
			var createdFromFieldsValues = SEARCHMODULE.lookupFields({
				type: SEARCHMODULE.Type.TRANSACTION,
				id: createdFromId,
				columns: ['recordtype', 'transferlocation']
			});
//			logRecord(createdFromFieldsValues, 'createdFromFieldsValues');
			if ('recordtype' in createdFromFieldsValues && createdFromFieldsValues['recordtype'] == RECORDMODULE.Type.TRANSFER_ORDER) {
				if ('transferlocation' in createdFromFieldsValues && createdFromFieldsValues['transferlocation'].length > 0) {
					toLocation = createdFromFieldsValues['transferlocation'][0].value;
				}
			}
			logRecord(toLocation, 'toLocation');
		}
	}

	var effectiveFulfillment = {
		fulfillmentId: fulfillmentId,
		fulfillmentLine: fulfillmentLine,
		quantity: 0,
		inventoryDetails: []
	};
	
	// - selected lines with empty fulfillment ==> update wo precompletion (fulfillmnentnumber, fulfillmentline)
	// - non selected lines with non empty fulfillment ==> update wo precompletion (empty fulfillmnentnumber, empty fulfillmentline)
	var woPreCompletionUpdates = [];
	for (var i = 0 ; i < packingListLines.length; i++) {
		var selectedFlag = getSublistValue({
			resultColumns: resultColumns,
			resultLine: packingListLines[i],
			fieldId: 'custpage_mob_fld_select'		
		});
		var internalid = getSublistValue({
			resultColumns: resultColumns,
			resultLine: packingListLines[i],
			fieldId: 'custpage_mob_fld_internalid'			
		});
		var packingFulfillment = getSublistValue({
			resultColumns: resultColumns,
			resultLine: packingListLines[i],
			fieldId: 'custpage_mob_fld_packingfulfillment'			
		});
		var updateFlag = false;
		var newFulfillment = '';
		var newLine = 0;
		var newCustomer = '';
		var newToLocation = '';
//		log.debug({
//			title: 'Wo Precompletion line',
//			details: 'ID: ' + internalid + ' Flag: ' + selectedFlag + ' Fulfillment:' + packingFulfillment
//		});
		if (selectedFlag == 'T') {
			var quantity = getSublistValue({
				resultColumns: resultColumns,
				resultLine: packingListLines[i],
				fieldId: 'custpage_mob_fld_packingquantity'		
			});
			var inventoryNumber = getSublistValue({
				resultColumns: resultColumns,
				resultLine: packingListLines[i],
				fieldId: 'custpage_mob_fld_inventorynumber'		
			});
			var binNumber = getSublistValue({
				resultColumns: resultColumns,
				resultLine: packingListLines[i],
				fieldId: 'custpage_mob_fld_binnumber'		
			});
			var inventoryDetail = {
				inventoryNumber: inventoryNumber,
				binNumber: binNumber,
				quantity: parseInt(quantity, 10)
			};
			addToEffectiveFulfillment(effectiveFulfillment, inventoryDetail);
			
			updateFlag = true;
			newFulfillment = fulfillmentId;
			newLine = fulfillmentLine;
			newCustomer = customerId;
			newToLocation = toLocation;
		} else {
			if (packingFulfillment) {
				log.debug('Reset woPrecompletion');
				updateFlag = true;				
			}			
		}
		if (updateFlag) {
			woPreCompletionUpdates.push({
				internalId: internalid,
				values: {
					custrecord_mob_cmpl_shipmentnumber: newFulfillment,
					custrecord_mob_cmpl_shipmentline: newLine,
					custrecord_mob_cmpl_shipementcustomer: newCustomer,
					custrecord_mob_cmpl_tolocation: newToLocation				
				}
			});
			// The woPreCompletion record is updated after the item fulfillment is saved
//			var recordId = RECORDMODULE.submitFields({
//				type: 'customrecord_mob_wo_pre_completion',
//				id: internalid,
//				values: {
//					custrecord_mob_cmpl_shipmentnumber: newFulfillment,
//					custrecord_mob_cmpl_shipmentline: newLine,
//					custrecord_mob_cmpl_shipementcustomer: newCustomer,
//					custrecord_mob_cmpl_tolocation: newToLocation
//				}
//			});
		}
	}
	
	// !!! Updating the item fulfillment fires the 'after submit' endpoint of the user entry script deployed on item fulfillment
	// This user entry script checks the consistency of the data and can reset wo_pre_completion fields in case of invalid data
	// So when modifying this script, be sure the code of the user entry script implements the same logic to avoid aside effects
	updateItemFulfillment(effectiveFulfillment);
	
	// Update woPreCompletions once the item fulfillment is successfully updated
	for (var i = 0 ; i < woPreCompletionUpdates.length ; i++) {
		var recordId = RECORDMODULE.submitFields({
			type: 'customrecord_mob_wo_pre_completion',
			id: woPreCompletionUpdates[i].internalId,
			values: woPreCompletionUpdates[i].values
		});
	}

//  	var form = getFormTemplate(fulfillmentId, fulfillmentLine, false);
//  	var str = '<html><head><script type="text/javascript">';
//	str += 'window.opener.nlapiSetCurrentLineItemValue("item","item"';
//	str += ',"' + selecteditem + '");';
//	str += 'window.close(); </script></head></html>';
//
//  	context.response.writePage(str);
	
  	context.response.sendRedirect({
  		type: HTTPSMODULE.RedirectType.SUITELET,
  		identifier: 'customscript_mob_sl_itemfulfillmentlines',
  		id: 'customdeploy_mob_sl_itemfulfillmentlines',
  		parameters: {custparam_mob_itemfulfillment: fulfillmentId}
  	});

	log.debug('postFunction completed');
}

function updateItemFulfillment(effectiveFulfillment) {
	log.debug('updateItemFulfillment started');
	logRecord(effectiveFulfillment, 'effectiveFulfillment');
	
	// No change if quantity is zero
	if (effectiveFulfillment.quantity == 0) { return; }
	
	var itemFulfillmentRecord = RECORDMODULE.load({
		type: RECORDMODULE.Type.ITEM_FULFILLMENT,
		id: effectiveFulfillment.fulfillmentId,
		isDynamic: true
	});
	
	var lineNumber = itemFulfillmentRecord.findSublistLineWithValue({
		sublistId: 'item',
		fieldId: 'line',
		value: effectiveFulfillment.fulfillmentLine
	});
	itemFulfillmentRecord.selectLine({
		sublistId: 'item',
		line: lineNumber
	});
	
	itemFulfillmentRecord.setCurrentSublistValue({
		sublistId: 'item',
		fieldId: 'quantity',
		value: effectiveFulfillment.quantity
	});
	
	inventoryDetailRecord = itemFulfillmentRecord.getCurrentSublistSubrecord({
		sublistId: 'item',
		fieldId: 'inventorydetail'		
	});
	logRecord(inventoryDetailRecord, 'inventoryDetailRecord');
	
	var lineCount = inventoryDetailRecord.getLineCount({
		sublistId: 'inventoryassignment'
	});
	var lineNumber = 0;
	for (var i = 0 ; i < lineCount ; i++) {
		inventoryDetailRecord.selectLine({
			sublistId: 'inventoryassignment',
			line: lineNumber
		});
		var inventoryNumber = inventoryDetailRecord.getCurrentSublistValue({
			sublistId: 'inventoryassignment',
			fieldId: 'issueinventorynumber'
		});
		var binNumber = inventoryDetailRecord.getCurrentSublistValue({
			sublistId: 'inventoryassignment',
			fieldId: 'binnumber'
		});
		var quantity = retrieveQuantityFromEffectiveFulfillment(effectiveFulfillment, inventoryNumber, binNumber);
		log.debug({
			title: 'quantity',
			details: quantity
		});
		
		if (quantity == 0) {
			inventoryDetailRecord.removeLine({
				sublistId: 'inventoryassignment',
				line: lineNumber	
			});			
		} else {
			inventoryDetailRecord.setCurrentSublistValue({
				sublistId: 'inventoryassignment',
				fieldId: 'quantity',
				value: quantity
			});
			
			inventoryDetailRecord.commitLine({
				sublistId: 'inventoryassignment'
			});
			
			lineNumber++;
		}
	}
	
	/*
		// Delete existing inventory assignments
		var lineCount = inventoryDetailRecord.getLineCount({
			sublistId: 'inventoryassignment'
		});
		for (var i = 0 ; i < lineCount ; i++) {
			var inventoryNumber = inventoryDetailRecord.getSublistValue({
				sublistId: 'inventoryassignment',
				fieldId: 'issueinventorynumber',
				line: 0
			});
			var binNumber = inventoryDetailRecord.getSublistValue({
				sublistId: 'inventoryassignment',
				fieldId: 'binnumber',
				line: 0
			});
			var inventoryStatus = inventoryDetailRecord.getSublistValue({
				sublistId: 'inventoryassignment',
				fieldId: 'inventorystatus',
				line: 0
			});
			updateEffectiveFulfillment(effectiveFulfillment, inventoryNumber, binNumber, inventoryStatus);
			
			inventoryDetailRecord.removeLine({
				sublistId: 'inventoryassignment',
				line: 0				
			});
		}

		logRecord(inventoryDetailRecord, 'inventoryDetailRecord');
		logRecord(effectiveFulfillment, 'effectiveFulfillment');
		for (var i = 0 ; i < effectiveFulfillment.inventoryDetails.length ; i++) {
			var inventoryDetail = effectiveFulfillment.inventoryDetails[i];
			logRecord(inventoryDetail, 'inventoryDetail');
			inventoryDetailRecord.selectNewLine({
				sublistId: 'inventoryassignment'
			});
			inventoryDetailRecord.setCurrentSublistValue({
				sublistId: 'inventoryassignment',
				fieldId: 'issueinventorynumber',
				value: inventoryDetail.inventoryNumber
			});
			
			var inventoryBin = '';
			if (inventoryDetail.binNumber) {
				inventoryBin = inventoryDetail.binNumber;
			}
			inventoryDetailRecord.setCurrentSublistValue({
				sublistId: 'inventoryassignment',
				fieldId: 'binnumber',
				value: inventoryBin
			});
			
//			var inventoryStatus = '';
//			if (inventoryDetail.status) {
//				inventoryStatus = inventoryDetail.status;
//			}
//			inventoryDetailRecord.setCurrentSublistValue({
//				sublistId: 'inventoryassignment',
//				fieldId: 'inventorystatus',
//				value: inventoryStatus
//			});
			
			inventoryDetailRecord.setCurrentSublistValue({
				sublistId: 'inventoryassignment',
				fieldId: 'quantity',
				value: inventoryDetail.quantity
			});
			
			inventoryDetailRecord.commitLine({
				sublistId: 'inventoryassignment'
			});
		}
		*/
	
	itemFulfillmentRecord.commitLine({
		sublistId: 'item'
	});
	itemFulfillmentRecord.save();
	
	log.debug('updateItemFulfillment completed');
}

function retrieveInventoryDetails(itemFulfillmentId, fulfillmentLine) {
	var inventoryDetails = { fulfillment: itemFulfillmentId, line: 0, itemId: 0, quantity: 0, packingSummary: []};
	var inventoryDetailRec;
	if (itemFulfillmentId > 0 && fulfillmentLine >= 0) {
		// Read informations from the inventory detail of the item fulfillment line
	    var itemFulfillmentRec = RECORDMODULE.load({
				type: RECORDMODULE.Type.ITEM_FULFILLMENT,
				id: itemFulfillmentId,
				isDynamic: false
	    });
	    fulfillmentLine = parseInt(fulfillmentLine, 10);
		inventoryDetails.line = itemFulfillmentRec.getSublistValue({
	    	sublistId: 'item',
	    	fieldId: 'line',
	    	line: fulfillmentLine			
		});
	    inventoryDetailRec = itemFulfillmentRec.getSublistSubrecord({
	    	sublistId: 'item',
	    	fieldId: 'inventorydetail',
	    	line: fulfillmentLine
	    });
//		logRecord(inventoryDetailRec, 'inventoryDetailRec');
	}

	if (inventoryDetailRec) {
		inventoryDetails.itemId = inventoryDetailRec.getValue('item');
		inventoryDetails.quantity = inventoryDetailRec.getValue('quantity');
		var lineCount = inventoryDetailRec.getLineCount({
			sublistId: 'inventoryassignment'
		});
		for (var i = 0 ; i < lineCount ; i++) {
			var packing = {};
			packing.inventoryNumber = inventoryDetailRec.getSublistValue({
				sublistId: 'inventoryassignment',
				fieldId: 'issueinventorynumber',
				line: i
			});
			packing.inventoryNumberDisplay = inventoryDetailRec.getSublistValue({
				sublistId: 'inventoryassignment',
				fieldId: 'issueinventorynumber_display',
				line: i
			});
			packing.binNumber = inventoryDetailRec.getSublistValue({
				sublistId: 'inventoryassignment',
				fieldId: 'binnumber',
				line: i
			});
			packing.binNumberDisplay = inventoryDetailRec.getSublistText({
				sublistId: 'inventoryassignment',
				fieldId: 'binnumber',
				line: i
			});
			packing.originalQuantity = inventoryDetailRec.getSublistValue({
				sublistId: 'inventoryassignment',
				fieldId: 'quantity',
				line: i
			});
			packing.packingQuantity = 0;
			packing.remainingQuantity = packing.originalQuantity;
			addToPackingSummary(inventoryDetails.packingSummary, packing);
		}
	}
	return inventoryDetails;
}

function addToPackingSummary(packingSummary, packing) {
	var found = false;
	for (var i = 0 ; i < packingSummary.length ; i++) {
		if (packingSummary[i].inventoryNumber == packing.inventoryNumber && 
				packingSummary[i].binNumber == packing.binNumber) {
			packingSummary[i].packingQuantity += parseInt(packing.packingQuantity, 10);
			packingSummary[i].originalQuantity += parseInt(packing.originalQuantity, 10);
			packingSummary[i].remainingQuantity += parseInt(packing.remainingQuantity, 10);
			found = true;
			break;
		}
	}
	if (!found) {
		packingSummary.push(packing);		
	}
}

function addToEffectiveFulfillment(effectiveFulfillment, inventoryDetail) {
	var found = false;
	effectiveFulfillment.quantity += parseInt(inventoryDetail.quantity, 10);
	for (var i = 0 ; i < effectiveFulfillment.inventoryDetails.length ; i++) {
		if (effectiveFulfillment.inventoryDetails[i].inventoryNumber == inventoryDetail.inventoryNumber && 
				effectiveFulfillment.inventoryDetails[i].binNumber == inventoryDetail.binNumber) {
			effectiveFulfillment.inventoryDetails[i].quantity += parseInt(inventoryDetail.quantity, 10);
			found = true;
			break;
		}
	}
	if (!found) {
		effectiveFulfillment.inventoryDetails.push(inventoryDetail);		
	}
}

function retrieveQuantityFromEffectiveFulfillment(effectiveFulfillment, inventoryNumber, binNumber) {
	var quantity = 0;
	for (var i = 0 ; i < effectiveFulfillment.inventoryDetails.length ; i++) {
		if (effectiveFulfillment.inventoryDetails[i].inventoryNumber == inventoryNumber && 
				effectiveFulfillment.inventoryDetails[i].binNumber == binNumber) {
			quantity = effectiveFulfillment.inventoryDetails[i].quantity;
			effectiveFulfillment.inventoryDetails[i].quantity = 0;
			break;
		}
	}
	return quantity;
}

//function updateEffectiveFulfillment(effectiveFulfillment, inventoryNumber, binNumber, status) {
//	log.debug('updateEffectiveFulfillment started');
//	log.debug({
//		title: 'inventoryNumber',
//		details: inventoryNumber
//	});
//	log.debug({
//		title: 'binNumber',
//		details: binNumber
//	});
//	log.debug({
//		title: 'status',
//		details: status
//	});
//	for (var i = 0 ; i < effectiveFulfillment.inventoryDetails.length ; i++) {
//		if (effectiveFulfillment.inventoryDetails[i].inventoryNumber == inventoryNumber) {
//			effectiveFulfillment.inventoryDetails[i].binNumber = binNumber;
//			effectiveFulfillment.inventoryDetails[i].status = status;
//			break;
//		}
//	}
//	log.debug('updateEffectiveFulfillment completed');
//}

function OLDretrieveAvailablePackages(inventoryDetails) {
	var results = [];	
    var packingLots = [];
    for (var i = 0 ; i < inventoryDetails.packingSummary.length ; i++) {
    	packingLots.push(inventoryDetails.packingSummary[i].inventoryNumber);
    }
    logRecord(inventoryDetails, 'inventoryDetails');
    var filters = [];
	var itemFilter = SEARCHMODULE.createFilter({
		name: 'custrecord_mob_cmpl_item',
		operator: SEARCHMODULE.Operator.IS,
		values: inventoryDetails.itemId
	});
	var lotNumberFilter = SEARCHMODULE.createFilter({
		name: 'custrecord_mob_cmpl_lotnumber',
		operator: SEARCHMODULE.Operator.ANY,
		values: packingLots[0]
	});
	var emptyShipmentNumberFilter = SEARCHMODULE.createFilter({
		name: 'custrecord_mob_cmpl_shipmentnumber',
		operator: SEARCHMODULE.Operator.ISEMPTY
	});
	var currentShipmentNumberFilter = SEARCHMODULE.createFilter({
		name: 'custrecord_mob_cmpl_shipmentnumber',
		operator: SEARCHMODULE.Operator.IS,
		values: inventoryDetails.fulfillment
	});
	var shipmentLineFilter = SEARCHMODULE.createFilter({
		name: 'custrecord_mob_cmpl_shipmentline',
		operator: SEARCHMODULE.Operator.IS,
		values: inventoryDetails.line
	});
	
	// SuiteAnswers 71258 + 51141 + 85142 + 30270
	// ==> no way to combine filters with AND and OR operators
	// Several suite answers deal with the use of OR and arrays of filters but nothing works
	// filter Expression does not work
	// each time we get an WRONG_PARAMETER_TYPE error
	//var shipmentFilters = [];
	//shipmentFilters.push(emptyShipmentNumberFilter);
	//shipmentFilters.push('OR');
	//shipmentFilters.push(currentShipmentNumberFilter);
	//
	//filters.push(lotNumberFilter);
	//filters.push('AND');
	//filters.push(shipmentFilters);
	
	//var precompletionSearchObj = SEARCHMODULE.create({
	//	type: 'customrecord_mob_wo_pre_completion',
	//	columns: ['internalid', 'custrecord_mob_cmpl_lotnumber', 'custrecord_mob_cmpl_packagenumber', 'custrecord_mob_cmpl_traceability', 'custrecord_mob_cmpl_bin', 'custrecord_mob_cmpl_quantity', 
	//	          'custrecord_mob_cmpl_quantityreceived', 'custrecord_mob_cmpl_siloid', 'custrecord_mob_cmpl_sourcedsilonumber', 'custrecord_mob_cmpl_sourcedwocompletion', 
	//	          'custrecord_mob_cmpl_sourcedwocompletion', 'custrecord_mob_cmpl_lotstatus'],
	//	filters: [itemFilter, [[lotNumberFilter], 'OR', [currentShipmentNumberFilter]]]
	//});
	
	//var precompletionSearchObj = SEARCHMODULE.create({
	//	type: 'customrecord_mob_wo_pre_completion',
	//	columns: ['internalid', 'custrecord_mob_cmpl_lotnumber', 'custrecord_mob_cmpl_packagenumber', 'custrecord_mob_cmpl_traceability', 'custrecord_mob_cmpl_bin', 'custrecord_mob_cmpl_quantity', 
	//	          'custrecord_mob_cmpl_quantityreceived', 'custrecord_mob_cmpl_siloid', 'custrecord_mob_cmpl_sourcedsilonumber', 'custrecord_mob_cmpl_sourcedwocompletion', 
	//	          'custrecord_mob_cmpl_sourcedwocompletion', 'custrecord_mob_cmpl_lotstatus'],
	//	filters: [itemFilter],
	//	filterExpression: [
	//		['custrecord_mob_cmpl_lotnumber', SEARCHMODULE.Operator.ANY, packingLots],
	//		'and',
	//		['custrecord_mob_cmpl_shipmentnumber', SEARCHMODULE.Operator.ISEMPTY]
	//	]
	//});
	
	//var precompletionSearchObj = SEARCHMODULE.create({
	//	type: 'customrecord_mob_wo_pre_completion',
	//	columns: ['internalid', 'custrecord_mob_cmpl_lotnumber', 'custrecord_mob_cmpl_packagenumber', 'custrecord_mob_cmpl_traceability', 'custrecord_mob_cmpl_bin', 'custrecord_mob_cmpl_quantity', 
	//	          'custrecord_mob_cmpl_quantityreceived', 'custrecord_mob_cmpl_siloid', 'custrecord_mob_cmpl_sourcedsilonumber', 'custrecord_mob_cmpl_sourcedwocompletion', 
	//	          'custrecord_mob_cmpl_sourcedwocompletion', 'custrecord_mob_cmpl_lotstatus'],
	//	filters: [
	//			['custrecord_mob_cmpl_item', SEARCHMODULE.Operator.IS, itemId],
	//			'and',
	//			['custrecord_mob_cmpl_lotnumber', SEARCHMODULE.Operator.ANY, packingLots],
	//			'and',
	//			['custrecord_mob_cmpl_shipmentnumber', SEARCHMODULE.Operator.ISEMPTY]
	//		]
	//});
	
	//var precompletionSearchObj = SEARCHMODULE.create({
	//	type: 'customrecord_mob_wo_pre_completion',
	//	columns: ['internalid', 'custrecord_mob_cmpl_lotnumber', 'custrecord_mob_cmpl_packagenumber', 'custrecord_mob_cmpl_traceability', 'custrecord_mob_cmpl_bin', 'custrecord_mob_cmpl_quantity', 
	//	          'custrecord_mob_cmpl_quantityreceived', 'custrecord_mob_cmpl_siloid', 'custrecord_mob_cmpl_sourcedsilonumber', 'custrecord_mob_cmpl_sourcedwocompletion', 
	//	          'custrecord_mob_cmpl_sourcedwocompletion', 'custrecord_mob_cmpl_lotstatus'],
	//	filters: [
	//			['custrecord_mob_cmpl_item', SEARCHMODULE.Operator.IS, itemId],
	//			'and',
	//			['custrecord_mob_cmpl_lotnumber', SEARCHMODULE.Operator.ANY, packingLots],
	//			'and',
	//			[
	//				['custrecord_mob_cmpl_shipmentnumber', SEARCHMODULE.Operator.ISEMPTY], 
	//				'or',
	//				['custrecord_mob_cmpl_shipmentnumber', SEARCHMODULE.Operator.IS, itemFulfillmentId]
	//			]
	//		]
	//});
	
	// SuiteAnswer 65118 ==> "WRONG_PARAMETER_TYPE"
//	var precompletionSearchObj = SEARCHMODULE.create({
//		type: 'customrecord_mob_wo_pre_completion',
//		columns: ['internalid', 'custrecord_mob_cmpl_lotnumber', 'custrecord_mob_cmpl_packagenumber',
//		          'custrecord_mob_cmpl_quantityreceived', 'custrecord_mob_cmpl_shipmentnumber'],
//		filters: [
//	          [[itemFilter]], 'AND', [[lotNumberFilter]]
//		]
//	});
	var precompletionSearchObj = SEARCHMODULE.create({
		type: 'customrecord_mob_wo_pre_completion',
		columns: ['internalid', 'custrecord_mob_cmpl_lotnumber', 'custrecord_mob_cmpl_packagenumber',
		          'custrecord_mob_cmpl_quantityreceived', 'custrecord_mob_cmpl_shipmentnumber'],
		filters: [
	          [itemFilter], 'AND', [lotNumberFilter]
		]
	});
	precompletionSearchObj.run().each(function(result) {
		results.push(result);
		return true;
	});
//	
//	var precompletionSearchObj = SEARCHMODULE.create({
//		type: 'customrecord_mob_wo_pre_completion',
//		columns: ['internalid', 'custrecord_mob_cmpl_lotnumber', 'custrecord_mob_cmpl_packagenumber',
//		          'custrecord_mob_cmpl_quantityreceived', 'custrecord_mob_cmpl_shipmentnumber'],
//		filters: [itemFilter, lotNumberFilter, emptyShipmentNumberFilter]
//	});
//	precompletionSearchObj.run().each(function(result) {
//		results.push(result);
//		return true;
//	});	
	return results;
}

function retrieveAvailablePackages(inventoryDetails) {
	var results = [];	
	var packingLots = [];
	for (var i = 0 ; i < inventoryDetails.packingSummary.length ; i++) {
		var inventoryNumberDisplay = inventoryDetails.packingSummary[i].inventoryNumberDisplay;
		if (packingLots.indexOf(inventoryNumberDisplay) < 0) {
	    	packingLots.push(inventoryNumberDisplay);    		
		}
	}
	logRecord(packingLots, 'packingLots');
	
	// The empty filter does not apply (see OLDretrieveAvailablePackages)
	// ==> so, does not apply the filters on shipment number and filter data manually
	// in the each statement
	/*
	var emptyShipmentNumberFilter = SEARCHMODULE.createFilter({
		name: 'custrecord_mob_cmpl_shipmentnumber',
		operator: SEARCHMODULE.Operator.ISEMPTY
	});
	var currentShipmentNumberFilter = SEARCHMODULE.createFilter({
		name: 'custrecord_mob_cmpl_shipmentnumber',
		operator: SEARCHMODULE.Operator.IS,
		values: inventoryDetails.fulfillment
	});
	var shipmentLineFilter = SEARCHMODULE.createFilter({
		name: 'custrecord_mob_cmpl_shipmentline',
		operator: SEARCHMODULE.Operator.IS,
		values: inventoryDetails.line
	});
	*/
    
    // ANY filter does not work - on text field ? Tryed ANYOF: produces an error
//	var lotNumberFilter = SEARCHMODULE.createFilter({
//		name: 'custrecord_mob_cmpl_lotnumber',
//		operator: SEARCHMODULE.Operator.ANY,
//		values: packingLots
//	});	

	var itemFilter = SEARCHMODULE.createFilter({
		name: 'custrecord_mob_cmpl_item',
		operator: SEARCHMODULE.Operator.IS,
		values: inventoryDetails.itemId
	});

	var quantityFilter = SEARCHMODULE.createFilter({
		name: 'custrecord_mob_cmpl_quantityreceived',
		operator: SEARCHMODULE.Operator.GREATERTHAN,
		values: 0
	});
    
    // ==> need to perform one search per lot
    for (var i = 0 ; i < packingLots.length ; i++) {
    	var lotNumberFilter = SEARCHMODULE.createFilter({
    		name: 'custrecord_mob_cmpl_lotnumber',
    		operator: SEARCHMODULE.Operator.IS,
    		values: packingLots[i]
    	});
    	
    	var precompletionSearchObj = SEARCHMODULE.create({
    		type: 'customrecord_mob_wo_pre_completion',
    		columns: ['internalid', 'custrecord_mob_cmpl_lotnumber', 'custrecord_mob_cmpl_bin', 'custrecord_mob_cmpl_packagenumber',
    		          'custrecord_mob_cmpl_quantityreceived', 'custrecord_mob_cmpl_shipmentnumber', 'custrecord_mob_cmpl_shipmentline'],
    		filters: [itemFilter, quantityFilter, lotNumberFilter]
    	});
    	precompletionSearchObj.run().each(function(result) {
    		var fulfillmentId = result.getValue('custrecord_mob_cmpl_shipmentnumber');
    		var fulfillmentLine = result.getValue('custrecord_mob_cmpl_shipmentline');
    		var binNumber = result.getValue('custrecord_mob_cmpl_bin');
    		var bypassFlag = false;
    		if (fulfillmentId) {
    			if (fulfillmentId != inventoryDetails.fulfillment || fulfillmentLine != inventoryDetails.line) {
    				bypassFlag = true;
    			}
    		} else {
    			var found = false;
    			for (var j = 0 ; j < inventoryDetails.packingSummary.length ; j++) {
    				if (inventoryDetails.packingSummary[j].inventoryNumberDisplay == packingLots[i] &&
    						inventoryDetails.packingSummary[j].binNumber == binNumber) {
    					found = true;
    				}
    			}
    			bypassFlag = !found;
    		}
    		if (!bypassFlag) {
    			results.push(result);
    		}
    		return true;
    	});    	
    }
    
    // Complete the list with WO pre completions previously selected for this item fulfillment line	
	var otherPrecompletionSearchObj = SEARCHMODULE.create({
		type: 'customrecord_mob_wo_pre_completion',
		columns: ['internalid', 'custrecord_mob_cmpl_lotnumber', 'custrecord_mob_cmpl_bin', 'custrecord_mob_cmpl_packagenumber',
		          'custrecord_mob_cmpl_quantityreceived', 'custrecord_mob_cmpl_shipmentnumber', 'custrecord_mob_cmpl_shipmentline'],
		filters: [ SEARCHMODULE.createFilter({
	    		name: 'custrecord_mob_cmpl_shipmentnumber',
	    		operator: SEARCHMODULE.Operator.IS,
	    		values: inventoryDetails.fulfillment
    		}),
    		SEARCHMODULE.createFilter({
	    		name: 'custrecord_mob_cmpl_shipmentline',
	    		operator: SEARCHMODULE.Operator.EQUALTO,
	    		values: inventoryDetails.line
    		})
        ]
	});
	otherPrecompletionSearchObj.run().each(function(result) {
		var internalId = result.getValue('internalid');
		var bypassFlag = false;
		for (var k = 0 ; k < results.length ; k++) {
			var resultInternalId = results[k].id;
			if (internalId == resultInternalId) {
				bypassFlag = true;
				break;
			}
		}
		if (!bypassFlag) {
			results.push(result);
		}
		return true;
	});    
    
	return results;
}
 
function getFormTemplate(itemFulfillmentId, fulfillmentLine, woCompletionAreCreated, errorMessage) {
	var translationHandle = TRANSLATIONMODULE.load({
		collections: [
		    {
		    	collection: 'custcollection_mob_outgoingquality', 
		    	alias: 'quality', 
		    	keys: ['SL_PACKINGLIST_TITLE', 'FULFILLMENT_FLD', 'ITEM_FLD', 'QUANTITY_FLD', 'LOTS_FLD', 'PACKINGLIST_GRD', 'SELECT_COL', 
		    	       'BATCH_COL', 'BIN_COL', 'PACKAGE_COL', 'QUANTITY_COL', 'FULFILLMENT_COL', 'MARK_ALL_BTN', 'UNMARK_ALL_BTN']}
        ]
	});
	
    var form = UISERVERWIDGETMODULE.createForm({
        title : translationHandle.quality.SL_PACKINGLIST_TITLE()
    });
    form.clientScriptModulePath = 'SuiteScripts/QualityModule/mob_cs_packinglist.js';
    
    var itemId = 0;
    var quantity = 0;
    log.debug({
    	title: 'fulfillmentLine',
    	details: fulfillmentLine
    });
    var inventoryDetails = retrieveInventoryDetails(itemFulfillmentId, fulfillmentLine);
    logRecord(inventoryDetails, 'inventoryDetails');
    var packingSummary = inventoryDetails.packingSummary;
    
    // Add fields on the form
    var fulfillmentField = form.addField({
    	id: 'custpage_mob_fulfillmentid',
    	label: translationHandle.quality.FULFILLMENT_FLD(),
    	type: UISERVERWIDGETMODULE.FieldType.SELECT,
    	source: 'transaction'
    });
    fulfillmentField.updateDisplayType({
		displayType: UISERVERWIDGETMODULE.FieldDisplayType.INLINE
	});
    fulfillmentField.defaultValue = inventoryDetails.fulfillment;
    
    var lineField = form.addField({
    	id: 'custpage_mob_fulfillmentline',
    	label: 'LINE',
    	type: UISERVERWIDGETMODULE.FieldType.TEXT
    });
    lineField.updateDisplayType({
		displayType: UISERVERWIDGETMODULE.FieldDisplayType.HIDDEN
	});
    lineField.defaultValue = inventoryDetails.line;
    
    var itemField = form.addField({
    	id: 'custpage_mob_itemid',
    	label: translationHandle.quality.ITEM_FLD(),
    	type: UISERVERWIDGETMODULE.FieldType.SELECT,
    	source: 'item'
    });
    itemField.updateDisplayType({
		displayType: UISERVERWIDGETMODULE.FieldDisplayType.INLINE
	});
    itemField.defaultValue = inventoryDetails.itemId;
    
    var fulfillmentQuantityField = form.addField({
    	id: 'custpage_mob_fulfillmentquantity',
    	label: 'QUANTITY FULFILLMENT',
    	type: UISERVERWIDGETMODULE.FieldType.TEXT
    });
    fulfillmentQuantityField.updateDisplayType({
		displayType: UISERVERWIDGETMODULE.FieldDisplayType.HIDDEN
	});
    fulfillmentQuantityField.defaultValue = inventoryDetails.quantity;
    
    var selectedQuantityField = form.addField({
    	id: 'custpage_mob_selectedquantity',
    	label: 'QUANTITY SELECTED',
    	type: UISERVERWIDGETMODULE.FieldType.TEXT
    });
    selectedQuantityField.updateDisplayType({
		displayType: UISERVERWIDGETMODULE.FieldDisplayType.HIDDEN
	});
    
    var quantityField = form.addField({
    	id: 'custpage_mob_quantity',
    	label: translationHandle.quality.QUANTITY_FLD(),
    	//type: UISERVERWIDGETMODULE.FieldType.FLOAT
    	type: UISERVERWIDGETMODULE.FieldType.TEXT
    });
    quantityField.updateDisplayType({
		displayType: UISERVERWIDGETMODULE.FieldDisplayType.INLINE
	});
    
    var packingSummaryField = form.addField({
    	id: 'custpage_mob_packingsummary',
    	label: 'LOTS',
    	type: UISERVERWIDGETMODULE.FieldType.TEXTAREA
    });
    packingSummaryField.updateDisplayType({
		displayType: UISERVERWIDGETMODULE.FieldDisplayType.HIDDEN
	});
    
    var packingSummaryDisplayField = form.addField({
    	id: 'custpage_mob_packingsummarydisplay',
    	label: translationHandle.quality.LOTS_FLD(),
    	type: UISERVERWIDGETMODULE.FieldType.TEXTAREA
    });
    packingSummaryDisplayField.updateDisplayType({
		displayType: UISERVERWIDGETMODULE.FieldDisplayType.INLINE
	});

    
    // *********************************************************
    //        Create the packing list with its fields
    // *********************************************************
    var packingList = form.addSublist({
    	id: 'custpage_mob_packinglist',
    	label: translationHandle.quality.PACKINGLIST_GRD(),
    	type: UISERVERWIDGETMODULE.SublistType.LIST
    });
    var checkBoxField = packingList.addField({
        id: 'custpage_mob_fld_select',
        label: translationHandle.quality.SELECT_COL(),
        type: UISERVERWIDGETMODULE.FieldType.CHECKBOX
    });
    var checkBoxOriginalValueField = packingList.addField({
        id: 'custpage_mob_fld_selectoriginalvalue',
        label: 'SelectOriginalValue',
        type: UISERVERWIDGETMODULE.FieldType.TEXT
    });
    checkBoxOriginalValueField.updateDisplayType({
    	displayType : UISERVERWIDGETMODULE.FieldDisplayType.HIDDEN
    });
    // Internal Id
    var internalidField = packingList.addField({
    	id: 'custpage_mob_fld_internalid',
    	label: 'InternalId',
    	type: UISERVERWIDGETMODULE.FieldType.TEXT
    });
    internalidField.updateDisplayType({
    	displayType : UISERVERWIDGETMODULE.FieldDisplayType.HIDDEN
    });
    // Inventory number (lot) - Display format 
    var inventoryNumberDisplayField = packingList.addField({
    	id: 'custpage_mob_fld_inventorynumberdisplay',
    	label: translationHandle.quality.BATCH_COL(),
    	type: UISERVERWIDGETMODULE.FieldType.TEXT
    });
    // Inventory number (lot) - internal id 
    var inventoryNumberField = packingList.addField({
    	id: 'custpage_mob_fld_inventorynumber',
    	label: 'Lot',
    	type: UISERVERWIDGETMODULE.FieldType.TEXT
    });
    inventoryNumberField.updateDisplayType({
    	displayType : UISERVERWIDGETMODULE.FieldDisplayType.HIDDEN
    });
    // Bin
    var binNumberField = packingList.addField({
    	id: 'custpage_mob_fld_binnumber',
    	label: translationHandle.quality.BIN_COL(),
    	type: UISERVERWIDGETMODULE.FieldType.SELECT,
    	source: 'bin'
    });
    binNumberField.updateDisplayType({
    	displayType : UISERVERWIDGETMODULE.FieldDisplayType.INLINE
    });
    // Package number
    var packageField = packingList.addField({
    	id: 'custpage_mob_fld_package',
    	label: translationHandle.quality.PACKAGE_COL(),
    	type: UISERVERWIDGETMODULE.FieldType.TEXT
    });
    // Quantity
    var packingQuantityField = packingList.addField({
    	id: 'custpage_mob_fld_packingquantity',
    	label: translationHandle.quality.QUANTITY_COL(),
    	type: UISERVERWIDGETMODULE.FieldType.INTEGER
    });
    // Fulfillment
    var packingFulfillmentField = packingList.addField({
    	id: 'custpage_mob_fld_packingfulfillment',
    	label: translationHandle.quality.FULFILLMENT_COL(),
    	type: UISERVERWIDGETMODULE.FieldType.SELECT,
    	source: 'transaction'
    });
    packingFulfillmentField.updateDisplayType({
    	displayType : UISERVERWIDGETMODULE.FieldDisplayType.INLINE
    });
//    packingFulfillmentField.updateDisplayType({
//    	displayType : UISERVERWIDGETMODULE.FieldDisplayType.HIDDEN
//    });
    
//    packingList.addMarkAllButtons();
    packingList.addButton({
    	id: 'custpage_mob_markallbutton',
    	label: translationHandle.quality.MARK_ALL_BTN(),
    	functionName: 'markAll'
    });
    packingList.addButton({
    	id: 'custpage_mob_unmarkallbutton',
    	label: translationHandle.quality.UNMARK_ALL_BTN(),
    	functionName: 'unmarkAll'
    });
    
    
    // *************************************************
    //          Fill the packing list with data
    // *************************************************

    var results = retrieveAvailablePackages(inventoryDetails);
    var packingQuantity = 0;
    
	for (var lineNumber = 0 ; lineNumber < results.length ; lineNumber++) {
		var result = results[lineNumber];
//		log.debug({
//			title: 'result',
//			details: result
//		});
		
		var internalid = result.getValue('internalid');
		if (internalid) {
			packingList.setSublistValue({
				id: 'custpage_mob_fld_internalid',
				line: lineNumber,
				value: internalid
			});
		}
		var inventoryNumberDisplay = result.getValue('custrecord_mob_cmpl_lotnumber');
		if (inventoryNumberDisplay) {
			packingList.setSublistValue({
				id: 'custpage_mob_fld_inventorynumberdisplay',
				line: lineNumber,
				value: inventoryNumberDisplay
			});
		}
		var inventoryNumber = retrieveInventoryNumber(inventoryDetails, inventoryNumberDisplay);
		if (inventoryNumber) {
			packingList.setSublistValue({
				id: 'custpage_mob_fld_inventorynumber',
				line: lineNumber,
				value: inventoryNumber
			});
		}
		var binNumber = result.getValue('custrecord_mob_cmpl_bin');
		if (binNumber) {
			packingList.setSublistValue({
				id: 'custpage_mob_fld_binnumber',
				line: lineNumber,
				value: binNumber
			});
		}		
		var packageNumber = result.getValue('custrecord_mob_cmpl_packagenumber');
		if (packageNumber) {
			packingList.setSublistValue({
				id: 'custpage_mob_fld_package',
				line: lineNumber,
				value: packageNumber
			});
		}
		var quantityReceived = result.getValue('custrecord_mob_cmpl_quantityreceived') || '0';
		packingList.setSublistValue({
			id: 'custpage_mob_fld_packingquantity',
			line: lineNumber,
			value: quantityReceived
		});
		var fulfillmentId = result.getValue('custrecord_mob_cmpl_shipmentnumber');
		if (fulfillmentId) {
//			log.debug('Fulfillment not empty');
			packingList.setSublistValue({
				id: 'custpage_mob_fld_packingfulfillment',
				line: lineNumber,
				value: fulfillmentId
			});
			// This package is already selected for this fulfillment line ==> increase packingQuantity on packingSummary
			var packing = {
					inventoryNumber: inventoryNumber,
//					inventoryNumnerDisplay: inventoryNumberDisplay,
					binNumber: binNumber,
					originalQuantity: 0,
					packingQuantity: quantityReceived
			};
			addToPackingSummary(packingSummary, packing);
			packingQuantity += parseInt(quantityReceived, 10);
			
			packingList.setSublistValue({
				id: 'custpage_mob_fld_select',
				line: lineNumber,
				value: 'T'
			});
			packingList.setSublistValue({
				id: 'custpage_mob_fld_selectoriginalvalue',
				line: lineNumber,
				value: 'true'
			});
		}
	}
	
	selectedQuantityField.defaultValue = packingQuantity;
    quantityField.defaultValue = packingQuantity + '/' + inventoryDetails.quantity;
    packingSummaryField.defaultValue = JSON.stringify(packingSummary);
    logRecord(JSON.stringify(packingSummary),'packingSummary');
    packingSummaryDisplayField.defaultValue = formatPackingSummary(packingSummary);
	
    form.addSubmitButton({
        label : 'Save'
    });
	
    form.addButton({
    	id: 'custpage_cancelbutton',
    	label: 'Cancel',
    	functionName: 'cancelForm(' + itemFulfillmentId + ')'
    });
    
    return form;
}

function retrieveInventoryNumber(inventoryDetails, inventoryNumberDisplay) {
	var inventoryNumber = '';
	for (var i = 0 ; i < inventoryDetails.packingSummary.length ; i++) {
		if (inventoryDetails.packingSummary[i].inventoryNumberDisplay == inventoryNumberDisplay) {
			inventoryNumber = inventoryDetails.packingSummary[i].inventoryNumber;
			break;
		}
	} 		
	return inventoryNumber;
}

function formatPackingSummary(packingSummary) {
	var formattedText = '';
	var packingArray = packingSummary || [];
	for (var i = 0 ; i < packingArray.length ; i++) {
		if (i > 0) {
			formattedText += '\r\n';
		}
		formattedText += packingArray[i].inventoryNumberDisplay + ' | ' + packingArray[i].binNumberDisplay + ' : ' + packingArray[i].packingQuantity + '/' + packingArray[i].originalQuantity;
	}
	return formattedText;
}

/**
 * Parse an HTML line corresponding to a table line
 * @param options
 * @returns {String}
 */
function getSublistValue(options) {
	/*
	options.resultLine
	options.resultColumns
	options.fieldId
	*/
	var delimiter = /\u0001/;
	var result = '';
	var column = options.resultColumns.indexOf(options.fieldId);
	var fieldsValues = options.resultLine.split(delimiter);
	if (column >= 0 && column < fieldsValues.length) {
		result = fieldsValues[column];
	}
	return result;
}

function logRecord(record, logTitle) {
	var logRecord = JSON.stringify(record);
	var recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);		
	}
}

function getInputData() {
	// Construct the SuiteQL query string
	var suiteQL =
	"SELECT " +
		" \"TRANSACTION\".tranid AS tranidRAW /*{tranid↑RAW}*/ , " +
		" \"TRANSACTION\".trandate AS trandateRAW /*{trandate↑RAW}*/, " +
		" \"TRANSACTION\".postingperiod AS postingperiodDISPLAY /*{postingperiod↑DISPLAY}*/ " +
		"FROM " +
		" \"TRANSACTION\" WHERE \"TRANSACTION\".\"ID\" = ? ";
	// Return the query results as input data. The value 271 is the
	// internal ID of a transaction record
	return {
		type: 'suiteql',
		query: suiteQL,
		params: [271]
	};
}