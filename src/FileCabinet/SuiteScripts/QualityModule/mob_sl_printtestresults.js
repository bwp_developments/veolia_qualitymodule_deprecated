var QUERYMODULE, RECORDMODULE, RENDERMODULE, RUNTIMEMODULE, SEARCHMODULE, UISERVERWIDGETMODULE;
var AVERAGEMODES = {'PACKAGE': 0, 'QUANTITY': 1};
var AVERAGEMODE = AVERAGEMODES['QUANTITY'];
// var AVERAGEMODE = AVERAGEMODES['PACKAGE'];

/**
 * @NApiVersion 2.1
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['N/query', 'N/record', 'N/render', 'N/runtime', 'N/search', 'N/ui/serverWidget'], runSuitelet);

function runSuitelet(query, record, render, runtime, search, uiserverWidget) {
	QUERYMODULE= query;
	RECORDMODULE= record;
	RENDERMODULE= render;
	RUNTIMEMODULE= runtime;
	SEARCHMODULE= search;
	UISERVERWIDGETMODULE= uiserverWidget;
	
	var returnObj = {};
	returnObj['onRequest'] = _onRequest;
	return returnObj;	
}
   
/**
 * Definition of the Suitelet script trigger point.
 *
 * @param {Object} context
 * @param {ServerRequest} context.request - Encapsulation of the incoming request
 * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
 * @Since 2015.2
 */
function _onRequest(context) {
    let method = context.request.method;
    //logRecord(context, 'context');

	log.debug(method);
    if (method == 'GET') {
    	getFunction(context);
    } else if (method == 'POST') {
    	postFunction(context);
    }
}

/**
 * For a given fulfillment, this program generates average quality measures per item and lot number
 * @param context
 */
function getFunction(context) {	
	// Read the GET parameters
	let itemFulfillmentId = context.request.parameters.custparam_mob_itemfulfillment;
	let workOrderId = context.request.parameters.custparam_mob_workorderid;
	if (!itemFulfillmentId && !workOrderId) {
		return;
	}
	
	// 1. Retrieve global informations
	var globalInfo = retrieveGlobalInfo(itemFulfillmentId, workOrderId);
	
	// 2. Search items and lots concerned by the item fulfillment or the siloId
	var itemLots = retrieveItemLots(itemFulfillmentId, workOrderId);
	
	// 3. Get results informations
	completeItemLotsWithResults(itemLots);
	// logRecord(globalInfo, 'globalInfo');
	// logRecord(itemLots, 'itemLots');
	
	// 4. Calculate average measures
	var qualityResults = averageMeasures(itemLots, globalInfo);
	// logRecord(qualityResults, 'qualityResults');
	
	if (qualityResults.qualityresultsets.length == 0) {
		context.response.write('No packings available for this item fulfillment. Please select packings first.');
		return;
	}

	var renderer = RENDERMODULE.create();
	renderer.setTemplateByScriptId({
		scriptId: 'CUSTTMPL_MOB_OUTGOINGQUALITYMEASURES'
	});
	
	renderer.addCustomDataSource({
		format: RENDERMODULE.DataSource.JSON,
		alias: 'JSON',
		data: JSON.stringify(qualityResults, escapeHtml)
	});
	
	context.response.addHeader({
		name: 'Content-Type:',
		value: 'application/pdf'
		});
	context.response.addHeader({
		name: 'Content-Disposition',
		value: 'inline; filename=report.pdf'
	});

	/*
    // load printing template file from cabinet
    var templateFile = file.load({
        id: '34267'
    });

    var renderer = RENDERMODULE.create();
    renderer.templateContent = templateFile.getContents();
    
	renderer.addCustomDataSource({
		format: RENDERMODULE.DataSource.JSON,
		alias: 'JSON',
		data: JSON.stringify(qualityResults)
	});
	*/

	renderer.renderPdfToResponse(context.response);
}

function postFunction(context) {
    context.response.write('none found');
}

function escapeHtml(key, value) {
	if (typeof value === 'string'){
		let map = {
		'&': '&amp;',
		'<': '&lt;',
		'>': '&gt;',
		'"': '&quot;',
		"'": '&#039;'
		};
		
		return value.replace(/[&<>"']/g, function(m) { return map[m]; });
	}
	return value;
  }

function retrieveGlobalInfo(itemFulfillmentId, workOrderId) {
	let globalInfo = {
		subsidiaryId: '',
		customerId: '',
		subsidiaryName: '',
		subsidiaryNameNoHierarchy: '',
		customerName: '',
		fulfillmentLines: []
	};

	// Read information from itemFulfillmentId or work order linked to the silo
	// TODO: coding for siloId  
	if (workOrderId){
		retrieveWorkOrderInfo(globalInfo, workOrderId);
	} else {
		retrieveItemFulfillmentInfo(globalInfo, itemFulfillmentId);
	}

	let subsidiaryFields = SEARCHMODULE.lookupFields({
		type: SEARCHMODULE.Type.SUBSIDIARY,
		id: globalInfo.subsidiaryId,
		columns: ['name', 'namenohierarchy']
	});
	if ('name' in  subsidiaryFields) {
		globalInfo.subsidiaryName = subsidiaryFields.name;
	}
	if ('namenohierarchy' in  subsidiaryFields) {
		globalInfo.subsidiaryNameNoHierarchy = subsidiaryFields.namenohierarchy;
	}
	
	if (globalInfo.customerId) {
		let customerFields = SEARCHMODULE.lookupFields({
			type: SEARCHMODULE.Type.CUSTOMER,
			id: globalInfo.customerId,
			columns: ['altname']
		});
		if ('altname' in  customerFields) {
			globalInfo.customerName = customerFields.altname;
		}		
	}

	log.debug({
		title: 'globalInfo',
		details: globalInfo
	});
	
	return globalInfo;
}

function retrieveItemFulfillmentInfo(globalInfo, itemFulfillmentId) {	
	if (!itemFulfillmentId) {
		return;
	}
	let itemFulfillmentRecord = RECORDMODULE.load({
		type: RECORDMODULE.Type.ITEM_FULFILLMENT,
		id: itemFulfillmentId
	});
	globalInfo.subsidiaryId = itemFulfillmentRecord.getValue('subsidiary');
	globalInfo.customerId = itemFulfillmentRecord.getValue('entity');
	
	
	let lineCount = itemFulfillmentRecord.getLineCount({
		sublistId: 'item'
	});
	for (let i = 0 ; i < lineCount ; i++) {
		globalInfo.fulfillmentLines.push({
			line: itemFulfillmentRecord.getSublistValue({
				sublistId: 'item',
				fieldId: 'line',
				line: i
			}),
			customerPartName: itemFulfillmentRecord.getSublistValue({
				sublistId: 'item',
				fieldId: 'custcol_scm_customerpartnumber_display',
				line: i
			}),
			customerPartDesc: itemFulfillmentRecord.getSublistValue({
				sublistId: 'item',
				fieldId: 'custcol_mob_descitemcli',
				line: i
			})
		})
	}
}

function retrieveWorkOrderInfo(globalInfo, workOrderId) {
	if (!workOrderId) {
		return;
	}
	let workOrderRecord = RECORDMODULE.load({
		type: RECORDMODULE.Type.WORK_ORDER,
		id: workOrderId
	});
	globalInfo.subsidiaryId = workOrderRecord.getValue('subsidiary');
	globalInfo.customerId = workOrderRecord.getValue('entity');
}

function averageMeasures(itemLots, globalInfo) {
	var qualityResultSets = {qualityresultsets: []};
	for (var i = 0 ; i < itemLots.length ; i++) {
		var itemQuantity = parseInt(itemLots[i].itemQuantity || 0, 10);
		var packageCount = parseInt(itemLots[i].packageCount || 0, 10);
	
		// If customer part information is available on item fulfillment, use it
		var itemFulfillmentLine = {};
		for (var k = 0 ; k < globalInfo.fulfillmentLines.length ; k++) {
			if (globalInfo.fulfillmentLines[k].line == itemLots[i].shipmentline) {
				itemFulfillmentLine = globalInfo.fulfillmentLines[k];
				break;
			}
		}
		
		var resultSet = {
			subsidiaryId: globalInfo.subsidiaryId,
			// customerId is populated with toLocation in case it is non empty
			customerId: itemLots[i].toLocation || globalInfo.customerId,
			subsidiaryname: globalInfo.subsidiaryName,
			subsidiarynamenohierarchy: globalInfo.subsidiaryNameNoHierarchy,
			// customername is populated with toLocationName in case it is non empty
			customername: itemLots[i].toLocationName || globalInfo.customerName,
			qualityresultset: i,
			item: itemLots[i].item,
			itemname: itemLots[i].itemName,
			customerPartName: itemFulfillmentLine.customerPartName || itemLots[i].customerPartName || '',
			customerPartDesc: itemFulfillmentLine.customerPartDesc || itemLots[i].customerPartDesc || '',
			inventorynumber: itemLots[i].inventoryNumber,
			quantity: itemQuantity,
			packagecount: packageCount,
			customer: '',
			customeraltname: '',
			numberofuc: '',
			results: []
		};

		let totalQuantity = 0;
		for (var j = 0 ; j < itemLots[i].resultSets.length ; j++ ) {
			totalQuantity += parseFloat(itemLots[i].resultSets[j].itemQuantity || 0);
		}
		if (totalQuantity == 0)	{
			AVERAGEMODE = AVERAGEMODES['PACKAGE'];
		}
		
		for (var j = 0 ; j < itemLots[i].qualityResults.length ; j++ ) {
			var ponderedmeasure = 0;
			var measureExistsFlag = false;
			var measuresItemQuantity = 0;
			var measuresPackageCount = 0;
			let divider = 0;
			
			for (var k = 0 ; k < itemLots[i].qualityResults[j].measures.length ; k++) {
				var measureObj = itemLots[i].qualityResults[j].measures[k];
				if ( !measureObj.measure) {
					continue;
				}
				var multiplier = 0;
				if (AVERAGEMODE == AVERAGEMODES['QUANTITY']) {
					multiplier = parseFloat(measureObj.itemQuantity || 0);
				} else {
					multiplier = parseFloat(measureObj.packageCount || 0);
				}			
				ponderedmeasure += parseFloat(measureObj.measure || 0) * multiplier;
				measureExistsFlag = true;
				divider += multiplier;
				// measuresItemQuantity += parseFloat(measureObj.itemQuantity || 0);
				// measuresPackageCount += parseFloat(measureObj.packageCount || 0);
			}
			if (!measureExistsFlag) {
				continue;
			}
			// var divider = 0;
			// if (AVERAGEMODE = AVERAGEMODES['QUANTITY']) {
			// 	divider = measuresItemQuantity;
			// } else {
			// 	divider = measuresPackageCount;
			// }
			var averageMeasure = 0;
			if (divider != 0) {
				averageMeasure = Math.round(ponderedmeasure / divider * 100) / 100;
			}
			
			var averageResult = {
//				qualityresultset: i,
//				item: itemLots[i].item,
//				inventoryNumber: itemLots[i].inventoryNumber,
//				quantity: itemQuantity,
//				packagecount: packageCount,
				test: itemLots[i].qualityResults[j].test,
				norm: itemLots[i].qualityResults[j].norm,
				operatingmode: itemLots[i].qualityResults[j].operatingMode,
				measure: averageMeasure,
				unit: itemLots[i].qualityResults[j].unit,
				lowervalue: itemLots[i].qualityResults[j].lowerValue,
				highervalue: itemLots[i].qualityResults[j].higherValue
			};
//			qualityResultSets.qualityresultsets.push(averageResult);
			resultSet.results.push(averageResult);			
		}
		qualityResultSets.qualityresultsets.push(resultSet);
	}
	return qualityResultSets;
}

function completeItemLotsWithResults(itemLots) {
	// let resultSets = [];
	// logRecord(itemLots, 'itemLots');
	
	let woCompletions = [];
	let resultSetIds = [];
	if (!itemLots) {
		return;
	}

	for (let i = 0 ; i < itemLots.length ; i++) {
		// TODO: remove when finished
		if (itemLots[i].woCompletions) {
			for (let j = 0 ; j < itemLots[i].woCompletions.length ; j++) {
				let woCompletion = itemLots[i].woCompletions[j].woCompletion;
				if (woCompletion) {
					woCompletions.push(woCompletion);
				}
			}
		}
		
		// TODO: definitive code
		if (itemLots[i].resultSets) {
			for (let j = 0 ; j < itemLots[i].resultSets.length ; j++) {
				let resultSetId = itemLots[i].resultSets[j].resultSetId;
				if (resultSetId) {
					resultSetIds.push(itemLots[i].resultSets[j].resultSetId);
				}
			}
		}
	}
	
	if (woCompletions.length == 0 && resultSetIds.length == 0) {
		return;
	}
	
	let qualityResultsFilter;
	if (woCompletions.length > 0) { 
		qualityResultsFilter = SEARCHMODULE.createFilter({
			name: 'custrecord_mob_resultsetwocompletion',
			join: 'custrecord_mob_qualityresultset',
			operator: SEARCHMODULE.Operator.ANYOF,
			values: woCompletions
		});
	}
	if (resultSetIds.length > 0) { 
		// In case of resultSetIds, overwritte the filter
		qualityResultsFilter = SEARCHMODULE.createFilter({
			name: 'custrecord_mob_qualityresultset',
			operator: SEARCHMODULE.Operator.ANYOF,
			values: resultSetIds
		});
	}
	// logRecord(woCompletions, 'woCompletions');
	// logRecord(resultSetIds, 'resultSetIds');
	// logRecord(qualityResultsFilter, 'woCompletionFilter');
	let qualityResultsSearchObj = SEARCHMODULE.create({
		type: 'customrecord_mob_qualityresult', 		
		columns: [
			'internalid', 'custrecord_mob_resulttest', 'custrecord_mob_resultnorm', 'custrecord_mob_resultoperatingmode', 'custrecord_mob_resultpassed',
			'custrecord_mob_resultretainedmeasure', 'custrecord_mob_resultunit', 'custrecord_mob_resultlowervalue', 'custrecord_mob_resulthighervalue',
			'custrecord_mob_resulttesttype', 'custrecord_mob_qualityresultset', 
			SEARCHMODULE.createColumn({
				name: 'custrecord_mob_resultdisplaysequence',
				sort: SEARCHMODULE.Sort.ASC
			}),
			SEARCHMODULE.createColumn({
				name: 'custrecord_mob_resultsetwocompletion',
				join: 'custrecord_mob_qualityresultset'
			})
        ],
		filters: [qualityResultsFilter]
	});
	qualityResultsSearchObj.run().each(function(result) {
		// logRecord(result, 'result');
		let qualityResult = {};
		qualityResult.internalid = result.getValue('internalid');
		qualityResult.testId = result.getValue('custrecord_mob_resulttest');
		qualityResult.displaySequence = result.getValue('custrecord_mob_resultdisplaysequence');
		qualityResult.test = result.getText('custrecord_mob_resulttest');
		qualityResult.norm = result.getText('custrecord_mob_resultnorm');
		qualityResult.operatingMode = result.getText('custrecord_mob_resultoperatingmode');
		qualityResult.passed = result.getValue('custrecord_mob_resultpassed');
		qualityResult.measures = [{
			measure: result.getValue('custrecord_mob_resultretainedmeasure'),
			packageCount: 0,
			itemQuantity: 0,
			woCompletion: ''
		}];
		qualityResult.unit = result.getText('custrecord_mob_resultunit');
		qualityResult.lowerValue = result.getValue('custrecord_mob_resultlowervalue');
		qualityResult.higherValue = result.getValue('custrecord_mob_resulthighervalue');
		qualityResult.type = result.getText('custrecord_mob_resulttesttype');
		let woCompletionId = result.getValue({
			name: 'custrecord_mob_resultsetwocompletion',
			join: 'custrecord_mob_qualityresultset'
		});
		let resultSetId = result.getText('custrecord_mob_qualityresultset');
		addQualityResult(itemLots, woCompletionId, resultSetId, qualityResult);
		
		return true;
	});
	// logRecord(itemLots, 'itemLots');
	// return resultSets;
}

function addQualityResult(itemLots, woCompletionId, resultSetId, qualityResult) {
	let itemLot;
	for (let i = 0 ; i < itemLots.length ; i++) {
		if (itemLots[i].woCompletions) {
			for (let j = 0 ; j < itemLots[i].woCompletions.length ; j++) {
				if (itemLots[i].woCompletions[j].woCompletion == woCompletionId) {
					itemLot = itemLots[i];
					qualityResult.measures[0].packageCount = itemLots[i].woCompletions[j].packageCount;
					qualityResult.measures[0].itemQuantity = itemLots[i].woCompletions[j].itemQuantity;
					qualityResult.measures[0].woCompletion = itemLots[i].woCompletions[j].woWompletion;
					break;
				}
			}
		}
		if (itemLots[i].resultSets) {
			for (let j = 0 ; j < itemLots[i].resultSets.length ; j++) {
				if (itemLots[i].resultSets[j].resultSetId == resultSetId) {
					itemLot = itemLots[i];
					qualityResult.measures[0].packageCount = itemLots[i].resultSets[j].packageCount;
					qualityResult.measures[0].itemQuantity = itemLots[i].resultSets[j].itemQuantity;
					qualityResult.measures[0].resultSetId = itemLots[i].resultSets[j].resultSetId;
					break;
				}
			}
		}
		if (itemLot) {
			break;
		}
	}
	let existingQualityResult;
	for (let i = 0 ; i < itemLot.qualityResults.length ; i++) {
		if (itemLot.qualityResults[i].testId == qualityResult.testId) {
			existingQualityResult = itemLot.qualityResults[i];
			break;
		}
	}
	if(existingQualityResult) {
		existingQualityResult.measures.push(qualityResult.measures[0]);
	} else {
		itemLot.qualityResults.push(qualityResult);
	}
}

function retrieveItemLots(itemFulfillment, workOrderId) {
	// One item / lot could be selected on several lines of the item fulfillment
	// itemLot.shipmentline stores information from the first line processed by the program
	
	var itemLots = [];
	logRecord(itemFulfillment, 'itemFulfillment');

	let sqlString = `	Select t1.id as precompletionid, 
							t2.id as siloid, 
							t1.custrecord_mob_cmpl_shipmentnumber, 
							t1.custrecord_mob_cmpl_shipmentline, 
							t1.custrecord_mob_cmpl_item, t4.itemid, 
							t4.displayname as itemname, 
							t1.custrecord_mob_cmpl_lotnumber, 
							t1.custrecord_mob_cmpl_packagenumber, 
							t1.custrecord_mob_cmpl_quantityreceived, 
							t1.custrecord_mob_cmpl_tolocation, t5.name as tolocationname, 
							t3.custbody_lienarticleclient, t6.name as custpartname, t6.custrecord_mob_custitemdesc as custpartdesc, 
							t2.name, 
							t2.custrecord_mob_cmpl_qualresultset, 
							t2.custrecord_mob_cmpl_wocompletionold 
						From customrecord_mob_wo_pre_completion t1 
							join customrecord_mob_wo_productionsilo t2 
							on t2.id = t1.custrecord_mob_cmpl_siloid 
							join transaction t3 
							on t3.id = t2.custrecord_mob_cmpl_silowonumber 
							left outer join item t4 
							on t4.id = t1.custrecord_mob_cmpl_item 
							left outer join location t5
							on t5.id = t1.custrecord_mob_cmpl_tolocation 
							left outer join customrecord_scm_customerpartnumber t6 
							on t6.id = t3.custbody_lienarticleclient `;
	let whereStatement = '';
	if (workOrderId) {
		whereStatement = `Where t2.custrecord_mob_cmpl_silowonumber = ${workOrderId} `;
	} else {
		whereStatement = `Where t1.custrecord_mob_cmpl_shipmentnumber = ${itemFulfillment} `;
	}
	sqlString += whereStatement;
	let sqlResults = QUERYMODULE.runSuiteQL({ query: sqlString }).asMappedResults();
	// logRecord(sqlResults, 'sqlResults');
	
	sqlResults.forEach(result => {
		let itemLot = {};
		itemLot.item = result.custrecord_mob_cmpl_item;
		itemLot.itemName = result.itemname;
		itemLot.customerPartName = result.custpartname;
		itemLot.customerPartDesc = result.custpartdesc;
		itemLot.shipmentline = result.custpartname;
		itemLot.toLocation = result.custrecord_mob_cmpl_tolocation;
		itemLot.toLocationName = result.tolocationname;
		itemLot.inventoryNumber =  result.custrecord_mob_cmpl_lotnumber;
		itemLot.packageCount = 1;
		itemLot.itemQuantity = parseInt(result.custrecord_mob_cmpl_quantityreceived || 0, 10);
		if (result.custrecord_mob_cmpl_wocompletionold) {
			itemLot.woCompletions = [{ 
				woCompletion: result.custrecord_mob_cmpl_wocompletionold,
				itemQuantity: parseInt(result.custrecord_mob_cmpl_quantityreceived || 0, 10),
				packageCount: 1
			}];
		}
		if (result.custrecord_mob_cmpl_qualresultset) {
			// log.debug('Step2 V2');
			itemLot.resultSets = [{ 
				resultSetId: result.custrecord_mob_cmpl_qualresultset,
				itemQuantity: parseInt(result.custrecord_mob_cmpl_quantityreceived || 0, 10),
				packageCount: 1
			}];
		}
		itemLot.qualityResults = [];
		addItemLot(itemLots, itemLot);
	});

	return itemLots;
}

function addItemLot(itemLots, itemLot) {
	let existingItemLot;
	for (let i = 0 ; i < itemLots.length ; i++) {
		if (itemLots[i].item == itemLot.item && itemLots[i].inventoryNumber == itemLot.inventoryNumber) {
			existingItemLot = itemLots[i];
			break;
		}		
	}
	if (!existingItemLot) {
		itemLots.push(itemLot);
	} else {
		if (itemLot.woCompletions) {
			let existingWOCompletion;
			for (let i = 0 ; i < existingItemLot.woCompletions.length ; i++) {
				if (existingItemLot.woCompletions[i].woCompletion == itemLot.woCompletions[0].woCompletion) {
					existingWOCompletion = existingItemLot.woCompletions[i];
					break;
				}		
			}
			if (!existingWOCompletion) {
				existingItemLot.woCompletions.push(itemLot.woCompletions[0]);
			} else {
				existingWOCompletion.itemQuantity += itemLot.woCompletions[0].itemQuantity;
				existingWOCompletion.packageCount += 1;
			}
		}

		if (itemLot.resultSets) {
			let existingResultSet;
			for (let i = 0 ; i < existingItemLot.resultSets.length ; i++) {
				if (existingItemLot.resultSets[i].resultSetId == itemLot.resultSets[0].resultSetId) {
					existingResultSet = existingItemLot.resultSets[i];
					break;
				}		
			}
			if (!existingResultSet) {
				existingItemLot.resultSets.push(itemLot.resultSets[0]);
			} else {
				existingResultSet.itemQuantity += itemLot.resultSets[0].itemQuantity;
				existingResultSet.packageCount += 1;
			}
		}

		existingItemLot.packageCount += 1;
		// existingItemLot.itemQuantity += itemLot.woCompletions[0].itemQuantity;
		existingItemLot.itemQuantity += itemLot.itemQuantity;
	}
}

//function findItemLot(itemLots, itemLot) {
//}

function logRecord(record, logTitle) {
	let logRecord = JSON.stringify(record) || 'UNDEFINED';
	let recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);		
	}
}

function logVar(value, logTitle) {
	log.debug({
		title: logTitle + ' (' + typeof value + ')',
		details: value 
	});	
}