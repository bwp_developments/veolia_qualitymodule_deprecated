var RUNTIMEMODULE, SEARCHMODULE, TRANSLATIONMODULE, UISERVERWIDGETMODULE, URLMODULE;

/*
 * WARNING: quality status on WO Completion are handled by workflow
 * This script works in cooperation with the Outgoing Quality workflow
 */

/**
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/runtime', 'N/search', 'N/translation', 'N/ui/serverWidget', 'N/url'], runUserEvent);

function runUserEvent(runtime, search, translation, uiserverWidget, url) {
	RUNTIMEMODULE= runtime;
	SEARCHMODULE= search;
	TRANSLATIONMODULE= translation;
	UISERVERWIDGETMODULE= uiserverWidget;
	URLMODULE= url;
	
	var returnObj = {};
	returnObj['beforeLoad'] = _beforeLoad;
//	returnObj['beforeSubmit'] = _beforeSubmit;
//	returnObj['afterSubmit'] = _afterSubmit;
	return returnObj;
}
   
/**
 * Function definition to be triggered before record is loaded.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {string} scriptContext.type - Trigger type
 * @param {Form} scriptContext.form - Current form
 * @Since 2015.2
 */
function _beforeLoad(scriptContext) {
	log.debug('BeforeLoad started');
	logRecord(scriptContext, 'scriptContext');
	
	var resultSetId = scriptContext.newRecord.getValue('custbody_mob_qualityresultset');
	var globalStatus = scriptContext.newRecord.getValue('custbody_mob_qualityglobalstatus');
	
	logVar('globalStatus', globalStatus);
	
	// Set value of Original Quality Status field with the value of Quality Status
	var originalQualityStatusField = scriptContext.form.getField({
		id: 'custbody_mob_originalqualitystatus'
	});
	originalQualityStatusField.defaultValue = scriptContext.newRecord.getValue('custbody_mob_status_qualite');
	
	if (scriptContext.type == scriptContext.UserEventType.VIEW) {
		if (globalStatus == '2' && resultSetId) {			
			var resultSetRecordUrl = URLMODULE.resolveRecord({
				recordType: 'customrecord_mob_qualityresultset',
				recordId: resultSetId,
				isEditMode: true
			});
			
	    	//hook point for the button that will be fired client side;
	    	var src = "require([], function() { window.location='"+resultSetRecordUrl+"'; });";
//	    	src = "alert(\"" + src + "\");";
	    	
			var refreshButton = scriptContext.form.addButton({
				id : 'custpage_button_redirecttoresultset',
				label : TRANSLATIONMODULE.get({collection: 'custcollection_mob_outgoingquality', key: 'ENTER_RESULTS_BTN'})(),
				functionName : src
			});			
		}
	}
	
	if (globalStatus == '1' || globalStatus == '2') {
		var resultSetField = scriptContext.form.getField({
			id: 'custbody_mob_qualityresultset'
		});
		resultSetField.updateDisplayType({
			displayType: UISERVERWIDGETMODULE.FieldDisplayType.HIDDEN
		});
	}
	
	log.debug('BeforeLoad completed');
}

/**
 * Function definition to be triggered before record is loaded.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {Record} scriptContext.oldRecord - Old record
 * @param {string} scriptContext.type - Trigger type
 * @Since 2015.2
 */
function _beforeSubmit(scriptContext) {
	/*
	 * Trying to handle this by workflow
	 * 
	// Status are handled by workflow but it is still possible to change status manually
	// Refer to client script to see which manual updates are allowed
	// In consequence the Global Status must sometimes be updated
	var inProgressStatuses = ['1', '2', '3', '4'];
	var qualityStatus = scriptContext.newRecord.getValue('custbody_mob_status_qualite');
	log.debug({
		title: 'qualityStatus',
		details: qualityStatus
	});
	*/
}

/**
 * Function definition to be triggered before record is loaded.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {Record} scriptContext.oldRecord - Old record
 * @param {string} scriptContext.type - Trigger type
 * @Since 2015.2
 */
function _afterSubmit(scriptContext) {

}

function logRecord(record, logTitle) {
	var logRecord = JSON.stringify(record);
	var recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);		
	}
}

function logVar(title, value) {
	log.debug({
		title: title + ' (' + typeof value + ')',
		details: value 
	});	
}

