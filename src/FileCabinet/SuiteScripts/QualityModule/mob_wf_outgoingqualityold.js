var SEARCHMODULE, QUALITYMODULE;

/*
 * Worflow script for WO Completion record. It is in charge to initialize the quality result set
 */

/**
 * @NApiVersion 2.1
 * @NScriptType workflowactionscript
 */
define(['N/search', './qualityModule'], runWorkflow)

function runWorkflow(search, qualityModule) {
	SEARCHMODULE= search;
	QUALITYMODULE= qualityModule;
	
	var returnObj = {};
	returnObj['onAction'] = _onAction;
	return returnObj;    
}

/**
 * Definition of the Suitelet script trigger point.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {Record} scriptContext.oldRecord - Old record
 * @Since 2016.1
 */
function _onAction(scriptContext) {
	// Read step to launch form parameters
	var script = RUNTIMEMODULE.getCurrentScript();
	var requestedAction = script.getParameter({
		name: 'custscript_mob_wf_requestedaction'
	});
	requestedAction = requestedAction || 'INITIALIZE_RESULTSET';
	log.debug({
		title: 'Requested action',
		details: requestedAction
	});
	
	var woCompletionRecord = scriptContext.newRecord;
	var itemId = woCompletionRecord.getValue('item');
	var woCompletion = woCompletionRecord.getValue('id');
	var woNumber = woCompletionRecord.getValue('createdfrom');
	var subsidiary = woCompletionRecord.getValue('subsidiary');
	var qualityResultSet = woCompletionRecord.getValue('custbody_mob_qualityresultset') || 0;
	qualityResultSet = parseInt(qualityResultSet, 10);
	
//	logVar('itemId', itemId);
//	logVar('woCompletion', woCompletion);
//	logVar('woNumber', woNumber);
//	logVar('subsidiary', subsidiary);
//	logVar('qualityResultSet', qualityResultSet);
	
	var returnResultSetId;

	var fieldsValues = SEARCHMODULE.lookupFields({
		type: SEARCHMODULE.Type.WORK_ORDER,
		id: woNumber,
		columns: ['entity']
	});
	var customerId = '0';
	if ('entity' in fieldsValues && fieldsValues.entity.length > 0) {
		customerId = fieldsValues.entity[0].value;		
	}

//	logVar('customerId', customerId);
	
	switch (requestedAction) {
	case 'INITIALIZE_RESULTSET':
		returnResultSetId = QUALITYMODULE.initializeResultSet(itemId, customerId, woCompletion, woNumber, subsidiary, qualityResultSet);
		break;
	case 'FINALIZE_RESULTSET':
		returnResultSetId = qualityResultSet;
		QUALITYMODULE.finalizeResultSet(qualityResultSet);
		break;   		
	}
	
	return returnResultSetId;	
}

function logRecord(record, logTitle) {
	var logRecord = JSON.stringify(record);
	var recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);		
	}
}

function logVar(title, value) {
	log.debug({
		title: title + ' (' + typeof value + ')',
		details: value 
	});	
}
