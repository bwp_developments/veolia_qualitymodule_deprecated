let SEARCHMODULE, TRANSLATIONMODULE, UIDIALOGMODULE, URLMODULE;

/**
 * @NApiVersion 2.1
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */
define(['N/search', 'N/translation', 'N/ui/dialog', 'N/url'], runClient);

function runClient(search, translation, uidialog, url) {
	SEARCHMODULE= search;
	TRANSLATIONMODULE= translation;
	UIDIALOGMODULE= uidialog;
	URLMODULE= url;
	
	let returnObj = {};
	returnObj['pageInit'] = _pageInit;
	returnObj['fieldChanged'] = _fieldChanged;
//	returnObj['postSourcing'] = _postSourcing;
//	returnObj['sublistChanged'] = _sublistChanged;
//	returnObj['lineInit'] = _lineInit;
//	returnObj['validateField'] = _validateField;
//	returnObj['validateLine'] = _validateLine;
//	returnObj['validateInsert'] = _validateInsert;
//	returnObj['validateDelete'] = _validateDelete;
//	returnObj['saveRecord'] = _saveRecord;
	returnObj['refreshButton'] = refreshButtonCall;
	return returnObj;
}
    
/**
 * Function to be executed after page is initialized.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.mode - The mode in which the record is being accessed (create, copy, or edit)
 *
 * @since 2015.2
 */
function _pageInit(scriptContext) {
	let currentRec = scriptContext.currentRecord;
//	alert(typeof currentRec.getValue('custpage_mob_wocompletionarecreated'));
	// // In Suitelet script the checkbox is populated with 'T' or 'F' but the getValue method returns a boolean !!
	// let woCompletionAreCreated = currentRec.getValue('custpage_mob_wocompletionarecreatedflag');
	// if (woCompletionAreCreated) {
	// 	let message = TRANSLATIONMODULE.get({
	// 		collection: 'custcollection_mob_outgoingquality',
	// 		key: 'CONFIRM_RECEPTION_MSG'
	// 	})();
	// 	UIDIALOGMODULE.alert({
	// 		title: 'Process completed',
	// 		message: message
	// 	});
	// }

	// Eric Moulin - 03/11/2021 - ADD BEGIN
	// custpage_mob_pageinitmessage can be a simple message or an exception

	// Disable fields on sublist line having a wo completion on a closed period
	let lineCount = currentRec.getLineCount({ sublistId: 'custpage_mob_receiptlist' });
	const fieldsToDisable = [
		'custpage_mob_fld_inventorynumber', 'custpage_mob_fld_package', 'custpage_mob_fld_traceability', 
		'custpage_mob_fld_silonumber', 'custpage_mob_fld_inventorystatus', 'custpage_mob_fld_bin', 'custpage_mob_fld_quantity', 
		'custpage_mob_fld_quantityreceived'
	]
	for (let i = 0 ; i < lineCount ; i++) {
		let isPeriodClosed = currentRec.getSublistValue({
			sublistId: 'custpage_mob_receiptlist',
			fieldId: 'custpage_mob_fld_isperiodclosed',
			line: i
		});
		let siloWoCompletion = currentRec.getSublistValue({
			sublistId: 'custpage_mob_receiptlist',
			fieldId: 'custpage_mob_fld_silowocompletion',
			line: i
		});
		if (!isPeriodClosed && !siloWoCompletion) { continue; }

		fieldsToDisable.forEach(fieldId => {
			try {
				currentRec.getSublistField({
					sublistId: 'custpage_mob_receiptlist',
					fieldId: fieldId,
					line: i
				}).isDisabled = true;
			} catch (ex) { /*getSublistField fails on non entry field*/ }
		})
	}

	let pageInitMessage = currentRec.getValue('custpage_mob_pageinitmessage');
	let exceptionMessage;
	try {
		exceptionMessage = JSON.parse(pageInitMessage);
	} catch (e) { }
	if (pageInitMessage) {
		let messageTitle = 'Process completed';
		let message = pageInitMessage;
		if (exceptionMessage && exceptionMessage.name) {
			messageTitle = exceptionMessage.name;
		}
		if (exceptionMessage && exceptionMessage.message) {
			message = exceptionMessage.message;
		}
		UIDIALOGMODULE.alert({
			title: messageTitle,
			message: message
		});
	}
	// Eric Moulin - 03/11/2021 - ADD BEGIN

	let labelsToPrint = currentRec.getValue('custpage_mob_labelstoprint');
	if (labelsToPrint) {
		let workOrderId = currentRec.getValue('custpage_mob_workorderid');
  		//get the url for the suitelet - add woRecordId parameter
  		let suiteletURL = URLMODULE.resolveScript({
  			scriptId: 'customscript_mob_sl_display_prod_labels',
  			deploymentId: 'customdeploy_mob_sl_display_prod_labels',
  			params: {
  				custparam_mob_workorderid: workOrderId,
  				custparam_mob_labelstoprint: labelsToPrint
			},
  			returnExternalUrl: false
  		});
  		
  		window.open(suiteletURL);
	}
}

/**
 * Function to be executed when field is changed.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 * @param {string} scriptContext.fieldId - Field name
 * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
 * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
 *
 * @since 2015.2
 */
function _fieldChanged(scriptContext) {
	if (scriptContext.fieldId == 'custpage_mob_bin') {
		// When bin is changed on the header, set the new value on lines.
		// Only selected lines or lines never received are changed
		let currentRec = scriptContext.currentRecord;
		let binValue = currentRec.getValue({
			fieldId: 'custpage_mob_bin'
		});
		let lineCount = currentRec.getLineCount({
			sublistId: 'custpage_mob_receiptlist'
		});
		
		for (let i = 0 ; i < lineCount ; i++) {			
			currentRec.selectLine({
				sublistId: 'custpage_mob_receiptlist',
				line: i
			});
			
			// custpage_mob_fld_receiveflag
			let isLineSelected = currentRec.getCurrentSublistValue({
					sublistId: 'custpage_mob_receiptlist',
					fieldId: 'custpage_mob_fld_receiveflag'
			});
			let quantityReceived = currentRec.getCurrentSublistValue({
				sublistId: 'custpage_mob_receiptlist',
				fieldId: 'custpage_mob_fld_quantityreceivedhidden'
			});
//			alert(i + ' - ' + isLineSelected + ' ' + quantityReceived);
			
			if (isLineSelected || quantityReceived == '0') {
				currentRec.setCurrentSublistValue({
					sublistId: 'custpage_mob_receiptlist',
					fieldId: 'custpage_mob_fld_bin',
					value: binValue
				});
				currentRec.commitLine({
					sublistId: 'custpage_mob_receiptlist'				
				});				
			}
		}		
	}
}

/**
 * Function to be executed when field is slaved.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 * @param {string} scriptContext.fieldId - Field name
 *
 * @since 2015.2
 */
function _postSourcing(scriptContext) {

}

/**
 * Function to be executed after sublist is inserted, removed, or edited.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @since 2015.2
 */
function _sublistChanged(scriptContext) {

}

/**
 * Function to be executed after line is selected.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @since 2015.2
 */
function _lineInit(scriptContext) {

}

/**
 * Validation function to be executed when field is changed.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 * @param {string} scriptContext.fieldId - Field name
 * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
 * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
 *
 * @returns {boolean} Return true if field is valid
 *
 * @since 2015.2
 */
function _validateField(scriptContext) {

}

/**
 * Validation function to be executed when sublist line is committed.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @returns {boolean} Return true if sublist line is valid
 *
 * @since 2015.2
 */
function _validateLine(scriptContext) {

}

/**
 * Validation function to be executed when sublist line is inserted.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @returns {boolean} Return true if sublist line is valid
 *
 * @since 2015.2
 */
function _validateInsert(scriptContext) {

}

/**
 * Validation function to be executed when record is deleted.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @returns {boolean} Return true if sublist line is valid
 *
 * @since 2015.2
 */
function _validateDelete(scriptContext) {

}

/**
 * Validation function to be executed when record is saved.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @returns {boolean} Return true if record is valid
 *
 * @since 2015.2
 */
function _saveRecord(scriptContext) {
	
}

function refreshButtonCall(workOrderId) {
	let suiteletURL = URLMODULE.resolveScript({
		scriptId: 'customscript_mob_sl_receivewo',
		deploymentId: 'customdeploy_mob_sl_receivewo',
		params: { custparam_mob_workorder: workOrderId },
		returnExternalUrl: false
	});
	
	window.location = suiteletURL;
}
