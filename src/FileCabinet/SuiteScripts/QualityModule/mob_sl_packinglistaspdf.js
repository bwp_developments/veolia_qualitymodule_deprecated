/**
 * @NApiVersion 2.1
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['N/search', 'N/render'],

function(search, render) {
   
    /**
     * Definition of the Suitelet script trigger point.
     *
     * @param {Object} context
     * @param {ServerRequest} context.request - Encapsulation of the incoming request
     * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
     * @Since 2015.2
     */
    function onRequest(context) {
    	
    	var request = context.request;
    	var response = context.response;
    	
    	if (context.request.method === 'GET') {
    		
    		// Read the GET parameters
    		var itemFulfillmentRecordId = request.parameters.mob_itemfulfillmentrecordid;
//    		var itemFulfillmentRecordId = '2163';

			// Create a filter on the internalId of the item fulfillment and add it to the filters of the existing saved search
			var filterOnShipmentNumber = search.createFilter({
				name: 'custrecord_mob_cmpl_shipmentnumber',
				operator: search.Operator.ANYOF,
				values: itemFulfillmentRecordId
			});
			var searchObj = search.load({
				id: 'customsearch_mob_packinglist'
			});
			searchObj.filters.push(filterOnShipmentNumber);
        	
			// Create a pdf with the renderer send it to a Suitelet that is in charge to send a Server reponse
			var results = searchObj.run().getRange({
				start: 0,
				end: 1000
			});
//        	logRecord(results, 'results');

        	var renderer = render.create();
        	renderer.setTemplateByScriptId({
        		scriptId: 'CUSTTMPL_MOB_PACKINGLIST_SUITELET'
        	});
        	
        	renderer.addSearchResults({
    			templateName: 'results', // The name in the #list tag of the advanced pdf template
    			searchResult: results
        	});
        	
        	response.addHeader({
        		name: 'Content-Type:',
        		value: 'application/pdf'
        		});
//    		response.addHeader({
//	    		name: 'Content-Disposition',
//	    		value: 'inline; filename=�report.pdf�'
//    		});
    		response.addHeader({
	    		name: 'Content-Disposition',
	    		value: 'inline; filename=report.pdf'
    		});
        		
        	renderer.renderPdfToResponse(response);
        	
    	} else {
            context.response.write('none found');
    	}
    }

    return {
        onRequest: onRequest
    };
    
});

function logRecord(record, logTitle) {
	var logRecord = JSON.stringify(record);
	var recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);		
	}
}
