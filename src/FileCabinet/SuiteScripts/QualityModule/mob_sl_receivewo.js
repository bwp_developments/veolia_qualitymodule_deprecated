var FORMATMODULE, QUERYMODULE, RECORDMODULE, REDIRECTMODULE, RUNTIMEMODULE, SEARCHMODULE, TRANSLATIONMODULE, UISERVERWIDGETMODULE, URLMODULE, QUALITYMODULE;

/**
 * @NApiVersion 2.1
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['N/format', 'N/query', 'N/record', 'N/redirect', 'N/runtime', 'N/search', 'N/translation', 'N/ui/serverWidget', 'N/url', './qualityModule'], runSuitelet);

function runSuitelet(format, query, record, redirect, runtime, search, translation, uiserverWidget, url, qualityModule) {
	FORMATMODULE= format;
	QUERYMODULE= query;
	RECORDMODULE= record;
	REDIRECTMODULE= redirect;
	RUNTIMEMODULE= runtime;
	SEARCHMODULE= search;
	TRANSLATIONMODULE= translation;
	UISERVERWIDGETMODULE= uiserverWidget;
	URLMODULE= url;
	QUALITYMODULE= qualityModule;
	
	let returnObj = {};
	returnObj['onRequest'] = _onRequest;
	return returnObj;	
}
   
/**
 * Definition of the Suitelet script trigger point.
 *
 * @param {Object} context
 * @param {ServerRequest} context.request - Encapsulation of the incoming request
 * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
 * @Since 2015.2
 */
function _onRequest(context) {
    var method = context.request.method;
    // logRecord(context, 'context');

	log.debug(method);
    if (method == 'GET') {
    	getFunction(context);
    } else if (method == 'POST') {
    	postFunction(context);
    }
}

function getFunction(context) {
	let workOrderId = context.request.parameters.custparam_mob_workorder || 0;
	let pageInitMessage = context.request.parameters.custparam_mob_pageInitMessage || '';
	let labelsToPrint = context.request.parameters.custparam_mob_labelsToPrint || '';
  	let form = getFormTemplate(workOrderId, pageInitMessage, labelsToPrint);
  	
  	context.response.writePage(form);
}

function postFunction(context) {
	// logRecord(context, 'context');
	let refererUrl = context.request.headers.Referer;
	var processResult = {
		woCompletionAreCreated: false,
		labelsToPrint: []
	};
	var pageInitMessage;
	var workOrderId = context.request.parameters.custpage_mob_workorderid;
	// Read information from resultset
	var fieldsValues = SEARCHMODULE.lookupFields({
		type: SEARCHMODULE.Type.WORK_ORDER,
		id: workOrderId,
		columns: ['custbody_lienarticleclient', 'status']
	});
	// log.debug({
	// 	title: 'fieldsValues',
	// 	details: fieldsValues
	// });
	var customerPart = '';
	if ('custbody_lienarticleclient' in fieldsValues && fieldsValues.custbody_lienarticleclient.length > 0) {
		customerPart = fieldsValues.custbody_lienarticleclient[0].value;		
	}
	// Eric Moulin - 03/11/2021 - ADD BEGIN
	var status = {};
	if ('status' in fieldsValues && fieldsValues.status.length > 0) {
		status.value = fieldsValues.status[0].value;
		status.text = fieldsValues.status[0].text;
		// logVar(fieldsValues.status, 'orderstatus');
	}
	// Status value does not depend on the user's language
	if (['pendingBuild', 'partiallyBuilt'].indexOf(status.value) < 0) {
		log.debug({ 
			title: 'Bad status - POST not processed'
		});
	} else {
		processResult = processPostedData(context, customerPart);
		// TODO: reactivate the code
		// try {
		// 	processResult = processPostedData(context, customerPart);
		// } catch (ex) {
		// 	logRecord(ex, 'Exception');
		// 	pageInitMessage = JSON.stringify(ex);
		// }
	}
	// Eric Moulin - 03/11/2021 - ADD END

	// var form = getFormTemplate(workOrderId, processResult.woCompletionAreCreated, processResult.labelsToPrint);
	// var form = getFormTemplate(workOrderId, pageInitMessage, processResult.labelsToPrint);
	// context.response.writePage(form);
	let parameters = {};
	if (processResult.message) {
		parameters.custparam_mob_pageInitMessage = processResult.message;
	}
	if (processResult.labelsToPrint) {
		parameters.custparam_mob_labelsToPrint = processResult.labelsToPrint.toString();
	}
	let redirectUrl = URLMODULE.resolveScript({
		deploymentId: 'customdeploy_mob_sl_receivewo',
		scriptId: 'customscript_mob_sl_receivewo',
		params: {
			custparam_mob_workorder: workOrderId
		}
	});
	REDIRECTMODULE.redirect({
		url: redirectUrl,
		parameters: parameters
	})
}

function processPostedData(context, customerPart) {
	let result = {
		woCompletionAreCreated: false,
		labelsToPrint: [], 
		message: ''
	};
	let delimiter = /\u0001/;
	let lineDelimiter = /\u0002/;
	
	let resultColumns = context.request.parameters.custpage_mob_receiptlistfields.split(delimiter);
	let completionListLines = context.request.parameters.custpage_mob_receiptlistdata.split(lineDelimiter);
	// let completionsBySilo = fillCompletionsBySilo(completionListLines, resultColumns);
	let precompletions = fillPrecompletions(completionListLines, resultColumns);

	if (checkDataValidity(precompletions, result)) {
		processPreCompletions(context, customerPart, precompletions, result);
	}
	return result;
}

function processPreCompletions(context, customerPart, precompletions, result) {	
	let workOrderId = context.request.parameters.custpage_mob_workorderid;
	let printLabelsFlag = context.request.parameters.custpage_mob_printlabelsflag;
	let frozenPeriodEndDate = retrieveFrozenPeriodDate();
	// Process the array:
	// if wo completion is empty: create a new wo completion
	// if wo completion not empty: update wo completion
	let isErrorDetected = false;
	let isWoCompletionRequired = false;
	for (let i = 0 ; i < precompletions.length ; i++) {
		let precompletion = precompletions[i];
		if (precompletion.receiveFlag) {
			if (precompletion.siloWoCompletion) {
				// This code is a security until data are migrated
				log.debug({
					title: 'Old data',
					details: 'pre completion not processed because silo wo completion is not empty'
				});
				continue;
			}
			let woCompletionId;
			let woCompletionDate;
			if (precompletion.woCompletion) {
				// Check transaction exists
				let fieldsValues = SEARCHMODULE.lookupFields({
					type: SEARCHMODULE.Type.TRANSACTION,
					id: precompletion.woCompletion,
					columns: ['internalid', 'trandate']
				});
				logRecord(fieldsValues, 'fieldsValues');

				if (!'internalid' in fieldsValues) {
					// The wo completion does not exists any longer : reset the field value
					precompletion.woCompletion = '';
				} else {
					woCompletionDate = fieldsValues.trandate;
				}
			}
			let isPeriodClosed = false;
			if (precompletion.woCompletion) {
				isPeriodClosed = checkPeriodIsClosed(woCompletionDate, frozenPeriodEndDate);
				if (!isPeriodClosed) {
					woCompletionId = updateWoCompletion(workOrderId, precompletion);
				}
			} else {
				woCompletionId = createWoCompletion(workOrderId, precompletion);
			}
			if (!isPeriodClosed && !woCompletionId) {
				isErrorDetected = true;
			} else {
				if (!isPeriodClosed) {
					isWoCompletionRequired = true;
					// Update wo pre completion
					RECORDMODULE.submitFields({
						type: 'customrecord_mob_wo_pre_completion',
						id: precompletion.internalid,
						values: {
							custrecord_mob_cmpl_lotnumber: precompletion.inventoryNumber,
							custrecord_mob_cmpl_packagenumber: precompletion.packageNumber,
							custrecord_mob_cmpl_traceability: precompletion.traceability,
							custrecord_mob_cmpl_bin: precompletion.bin,
							custrecord_mob_cmpl_lotstatus: precompletion.inventoryStatus,
							// custrecord_mob_cmpl_quantity: precompletion.quantityToReceive,
							custrecord_mob_cmpl_quantityreceived: precompletion.quantityToReceive,
							custrecord_mob_cmpl_customerpart: customerPart,
							custrecord_mob_cmpl_wocompletion: woCompletionId,
							custrecord_mob_cmpl_sysid: (new Date()).getTime().toString()
						}
					}); 
				}
				if (printLabelsFlag == 'T') {
					result.labelsToPrint.push(precompletion.internalid);				
				}	
			}
		} else {
			// completion.receiveFlag is false
			// logRecord(precompletion, 'precompletion');
			if (precompletion.quantityReceived == 0) {
				// update bin field, only bin + some fields to reset data in case of a manual suppression of a wo completion
				RECORDMODULE.submitFields({
					type: 'customrecord_mob_wo_pre_completion',
					id: precompletion.internalid,
					values: {
						custrecord_mob_cmpl_bin: precompletion.bin,
						custrecord_mob_cmpl_wocompletion: '',
						custrecord_mob_cmpl_quantityreceived: 0
					}
				});					
			}
		}
	}
	
	if (isWoCompletionRequired && !isErrorDetected) {
		result.woCompletionAreCreated = true;
		result.message = TRANSLATIONMODULE.get({
			collection: 'custcollection_mob_outgoingquality',
			key: 'CONFIRM_RECEPTION_MSG'
		})();
	}
}

/**
 * Check sysid has not changed since the user has initiated the form on its internet browser
 * @param {Object} precompletions 
 * @returns 
 */
function checkDataValidity(precompletions, result) {
	let isValid = true;
	let internalids = precompletions.map(x => x.internalid);let precompletionSearchObj = SEARCHMODULE.create({
		type: 'customrecord_mob_wo_pre_completion',
		columns: ['internalid', 'custrecord_mob_cmpl_sysid'],
		filters: [
			['internalid', 'anyof', internalids]
		]
	});
	logRecord(precompletions, 'precompletions');
	precompletionSearchObj.run().each(function(result) {
		let originalSysId = precompletions.find(x => x.internalid == result.getValue({ name: 'internalid' })).sysId || 0;
		let actualSysId = result.getValue({ name: 'custrecord_mob_cmpl_sysid' }) || 0;
		logVar(originalSysId, 'originalSysId');
		logVar(actualSysId, 'actualSysId');
		if (originalSysId != actualSysId) {
			isValid = false;
		}
		return isValid;
	});
	if (!isValid) {
		log.debug('validity is false');
		result.message = TRANSLATIONMODULE.get({
			collection: 'custcollection_mob_outgoingquality',
			key: 'DATA_HAS_CHANGED_MSG'
		})();
	}
	return isValid;
}

function updateWoCompletion(workOrderId, precompletions, packagesToPrint) {
	log.debug('updateWoCompletion started');
	logRecord(precompletions, 'precompletions');
	if (util.isObject(precompletions)) {
		precompletions = [precompletions];
	}
	if (!util.isArray(precompletions) || precompletions.length == 0) { return; }
	let woCompletionRecord;
	try {
		woCompletionRecord = RECORDMODULE.load({
			type: RECORDMODULE.Type.WORK_ORDER_COMPLETION,
			id: precompletions[0].woCompletion,
			isDynamic: true,
		});
		woCompletionRecord.setValue({
			fieldId: 'completedquantity',
			value: precompletions.reduce((a, b) => a + b.quantityToReceive, 0)
		});
		woCompletionRecord.setValue({
			fieldId: 'custbody_mob_silonumber',
			value: precompletions[0].siloNumber
		});
		let inventoryDetailSubRec = woCompletionRecord.getSubrecord({
			fieldId: 'inventorydetail'
		});
		
		// Delete existing inventory assignments
		let lineCount = inventoryDetailSubRec.getLineCount({
			sublistId: 'inventoryassignment'
		});
		for (let i = 0 ; i < lineCount ; i++) {
			inventoryDetailSubRec.removeLine({
				sublistId: 'inventoryassignment',
				line: 0				
			});
		}
		for (let i = 0 ; i < precompletions.length ; i++) {
			let precompletion = precompletions[i];
			if (precompletion.quantityToReceive > 0) {
				inventoryDetailSubRec.selectNewLine({
					sublistId: 'inventoryassignment'
				});
				inventoryDetailSubRec.setCurrentSublistValue({
					sublistId: 'inventoryassignment',
					fieldId: 'receiptinventorynumber',
					value: precompletion.inventoryNumber
				});
				if (precompletion.bin) {
					inventoryDetailSubRec.setCurrentSublistValue({
						sublistId: 'inventoryassignment',
						fieldId: 'binnumber',
						value: precompletion.bin
					});					
				}
				if (precompletion.inventoryStatus) {
					inventoryDetailSubRec.setCurrentSublistValue({
						sublistId: 'inventoryassignment',
						fieldId: 'inventorystatus',
						value: precompletion.inventoryStatus
					});					
				}
				inventoryDetailSubRec.setCurrentSublistValue({
					sublistId: 'inventoryassignment',
					fieldId: 'quantity',
					value: precompletion.quantityToReceive
				});
				inventoryDetailSubRec.commitLine({
					sublistId: 'inventoryassignment'
				});
			}
		}
//		logRecord(inventoryDetailSubRec, 'inventoryDetailSubRec');
	} catch (ex) {
		log.error({
			title: 'Failed to update WO completion',
			details: ex
		});
		return false;
	}

	let woCompletionRecordId = woCompletionRecord.save();

	log.debug('updateWoCompletion done');
	return woCompletionRecordId;
}

function createWoCompletion(workOrderId, precompletions, packagesToPrint) {
	log.debug('createWoCompletion started');
	if (util.isObject(precompletions)) {
		precompletions = [precompletions];
	}
	if (!util.isArray(precompletions) || precompletions.length == 0) { return; }
	let woCompletionRecordId = '';
	try {
		log.debug({
			title: 'workOrderId',
			details: workOrderId
		});
		let woCompletionRecord = RECORDMODULE.transform({
			fromType: RECORDMODULE.Type.WORK_ORDER,
			fromId: workOrderId,
			toType: RECORDMODULE.Type.WORK_ORDER_COMPLETION,
			isDynamic: true,
		});
		let operationsCount = woCompletionRecord.getLineCount({
			sublistId: 'operation'
		});
		let startoperation = '';
		let endoperation = '';
		if (operationsCount > 0){
			startoperation = woCompletionRecord.getSublistValue({
				sublistId: 'operation',
				fieldId: 'taskid',
				line: 0
			});
			endoperation = woCompletionRecord.getSublistValue({
				sublistId: 'operation',
				fieldId: 'taskid',
				line: operationsCount-1
			});
			woCompletionRecord.setValue({
				fieldId: 'startoperation',
				value: startoperation
			});
			woCompletionRecord.setValue({
				fieldId: 'endoperation',
				value: endoperation
			});
		}
		woCompletionRecord.setValue({
			fieldId: 'completedquantity',
			value: precompletions.reduce((a, b) => a + b.quantityToReceive, 0)
		});
		woCompletionRecord.setValue({
			fieldId: 'custbody_mob_silonumber',
			value: precompletions[0].siloNumber
		});
//		logRecord(woCompletionRecord, 'woCompletionRecord');
		let inventoryDetailSubRec = woCompletionRecord.getSubrecord({
			fieldId: 'inventorydetail'
		});
		for (let i = 0 ; i < precompletions.length ; i++) {
			let precompletion = precompletions[i];
			if (precompletion.quantityToReceive > 0) {
				inventoryDetailSubRec.selectNewLine({
					sublistId: 'inventoryassignment'
				});
				inventoryDetailSubRec.setCurrentSublistValue({
					sublistId: 'inventoryassignment',
					fieldId: 'receiptinventorynumber',
					value: precompletion.inventoryNumber
				});
				if (precompletion.bin) {
					inventoryDetailSubRec.setCurrentSublistValue({
						sublistId: 'inventoryassignment',
						fieldId: 'binnumber',
						value: precompletion.bin
					});					
				}
				if (precompletion.inventoryStatus) {
					inventoryDetailSubRec.setCurrentSublistValue({
						sublistId: 'inventoryassignment',
						fieldId: 'inventorystatus',
						value: precompletion.inventoryStatus
					});					
				}
				inventoryDetailSubRec.setCurrentSublistValue({
					sublistId: 'inventoryassignment',
					fieldId: 'quantity',
					value: precompletion.quantityToReceive
				});
				inventoryDetailSubRec.commitLine({
					sublistId: 'inventoryassignment'
				});
			}
		}
//		logRecord(inventoryDetailSubRec, 'inventoryDetailSubRec');
		woCompletionRecordId = woCompletionRecord.save();
	} catch (ex) {
		log.error({
			title: 'Failed to create WO completion',
			details: ex
		});
		return false;
	}
	log.debug({
		title: 'woCompletionRecordId',
		details: woCompletionRecordId
	});
	log.debug('createWoCompletion done');
	return woCompletionRecordId;
}
 
function getFormTemplate(workOrderId, pageInitMessage, labelsToPrint) {
	let translationHandle = TRANSLATIONMODULE.load({
		collections: [
		    {
		    	collection: 'custcollection_mob_outgoingquality', 
		    	alias: 'quality', 
		    	keys: ['SL_RECEIVEWO_TITLE', 'ORDER_FLD', 'CUSTOMER_FLD', 'ASSEMBLYITEM_FLD', 'MANUFROUTING_FLD', 'TOTAL_RECEIVED_FLD', 'RECEIVE_GRD'
		    	       , 'SELECT_COL', 'BATCH_COL', 'PACKAGE_COL', 'TRACEABILITY_COL', 'STATUS_COL', 'BIN_COL', 'QUANTITY_COL', 'RECEIVED_COL'
		    	       , 'SILO_COL', 'DATE_COL', 'CONTINUE_BTN', 'CONFIRM_RECEPTION_MSG', 'NO_MORE_COMPLETIONS_MSG', 'REFRESH_BTN']}
        ]
	});

    let form = UISERVERWIDGETMODULE.createForm({
        title : translationHandle.quality.SL_RECEIVEWO_TITLE()
    });
    form.clientScriptModulePath = 'SuiteScripts/QualityModule/mob_cs_receivewo.js';
    
	// Read informations from work order
	let fieldsValues = SEARCHMODULE.lookupFields({
		type: SEARCHMODULE.Type.WORK_ORDER,
		id: workOrderId,
		columns: ['tranid', 'item', 'quantity', 'location', 'entity', 'item', 'manufacturingrouting', 'status'] //['tranid', 'assemblyitem', 'quantity']
	});
//	log.debug({
//		title: 'fieldsValues',
//		details: fieldsValues
//	});
	let locationId = fieldsValues.location[0].value;
	let entityId = '';
	if ('entity' in fieldsValues) {
		if (fieldsValues.entity.length > 0) {
			entityId = fieldsValues.entity[0].value;
		}
	}
	let assemblyItemId = '';
	if ('item' in fieldsValues) {
		if (fieldsValues.item.length > 0) {
			assemblyItemId = fieldsValues.item[0].value;
		}
	}
	let manufRoutingId = '';
	if ('manufacturingrouting' in fieldsValues) {
		if (fieldsValues.manufacturingrouting.length > 0) {
			manufRoutingId = fieldsValues.manufacturingrouting[0].value;
		}
	}
	// Eric Moulin - 28/10/2021 - ADD BEGIN
	let status = {};
	if ('status' in fieldsValues && fieldsValues.status.length > 0) {
		status.value = fieldsValues.status[0].value;
		status.text = fieldsValues.status[0].text;
		// logVar(fieldsValues.status, 'orderstatus');
	}
	// Eric Moulin - 28/10/2021 - ADD END
    
	
	// **********************************
    // ***   Add fields on the form   ***
	// **********************************
	
    let workOrderField = form.addField({
    	id: 'custpage_mob_workorderid',
    	label: translationHandle.quality.ORDER_FLD(),
    	type: UISERVERWIDGETMODULE.FieldType.SELECT,
    	source: 'transaction'
    });
    workOrderField.updateDisplayType({
		displayType: UISERVERWIDGETMODULE.FieldDisplayType.INLINE
	});
    workOrderField.defaultValue = workOrderId;
	
	// Eric Moulin - 28/10/2021 - ADD BEGIN
	// Status value does not depend on the user's language
	if (['pendingBuild', 'partiallyBuilt'].indexOf(status.value) < 0) {
		log.debug({ 
			title: 'Bad WO status'
		});
		let workOrderStatusField = form.addField({
			id: 'custpage_mob_workorderstatus',
			label: translationHandle.quality.STATUS_COL(),
			type: UISERVERWIDGETMODULE.FieldType.TEXT
		});
		workOrderStatusField.updateDisplayType({
			displayType: UISERVERWIDGETMODULE.FieldDisplayType.INLINE
		});
		workOrderStatusField.defaultValue = status.text;
		
		let messageField = form.addField({
			id: 'custpage_mob_message',
			label: 'Message',
			type: UISERVERWIDGETMODULE.FieldType.TEXTAREA
		});
		messageField.updateDisplayType({
			displayType: UISERVERWIDGETMODULE.FieldDisplayType.INLINE
		});
		messageField.defaultValue = translationHandle.quality.NO_MORE_COMPLETIONS_MSG()

		return form;
	}
	// Eric Moulin - 28/10/2021 - ADD END
    
    let printLabelsFlagField = form.addField({
    	id: 'custpage_mob_printlabelsflag',
    	label: 'Print Labels', //translationHandle.quality.PRINTLABELS_FLD(),
    	type: UISERVERWIDGETMODULE.FieldType.CHECKBOX
    });
    printLabelsFlagField.defaultValue = 'T';
    
	let customerField = form.addField({
		id: 'custpage_mob_customer',
		label: translationHandle.quality.CUSTOMER_FLD(),
		type: UISERVERWIDGETMODULE.FieldType.SELECT,
		source: 'customer'
	});
	customerField.updateDisplayType({
		displayType: UISERVERWIDGETMODULE.FieldDisplayType.INLINE
	});
	customerField.defaultValue = entityId;
	
	let assemblyItemField = form.addField({
		id: 'custpage_mob_assemblyitem',
		label: translationHandle.quality.ASSEMBLYITEM_FLD(),
		type: UISERVERWIDGETMODULE.FieldType.SELECT,
		source: 'item'
	});
	assemblyItemField.updateDisplayType({
		displayType: UISERVERWIDGETMODULE.FieldDisplayType.INLINE
	});
	assemblyItemField.defaultValue = assemblyItemId;
	
	let manufRoutingField = form.addField({
		id: 'custpage_mob_manufrouting',
		label: translationHandle.quality.MANUFROUTING_FLD(),
		type: UISERVERWIDGETMODULE.FieldType.SELECT,
		source: 'manufacturingrouting'
	});
	manufRoutingField.updateDisplayType({
		displayType: UISERVERWIDGETMODULE.FieldDisplayType.INLINE
	});
	manufRoutingField.defaultValue = manufRoutingId;
	
    // Bin
    let binField = form.addField({
    	id: 'custpage_mob_bin',
    	label: translationHandle.quality.BIN_COL(),
    	type: UISERVERWIDGETMODULE.FieldType.SELECT
    });    
    binField.updateDisplayType({
    	displayType: UISERVERWIDGETMODULE.FieldDisplayType.ENTRY
    });
    let binValues = valuesForBin(assemblyItemId, locationId);
    // Add empty value at the beginning of the list
	binField.addSelectOption({
		value: 0,
		text: ' ',
		isSelect: false
	});
    for (let i = 0 ; i < binValues.authorizedValues.length ; i++) {
    	let authorizedBin = binValues.authorizedValues[i];
    	binField.addSelectOption({
    		value: authorizedBin.value,
    		text: authorizedBin.text,
    		isSelect: false
    	});
    }
    
    // Total received
    let totalReceivedField = form.addField({
    	id: 'custpage_mob_totalreceived',
    	label: translationHandle.quality.TOTAL_RECEIVED_FLD(),
    	type: UISERVERWIDGETMODULE.FieldType.INTEGER
    });
    totalReceivedField.updateDisplayType({
		displayType: UISERVERWIDGETMODULE.FieldDisplayType.INLINE
	});
    
    // // custpage_mob_wocompletionarecreatedflag field is used by client script to know if a popup must be displayed to indicated 
    // // that the creation of wo completions is successfull
    // var woCompletionAreCreatedFlagField = form.addField({
    // 	id: 'custpage_mob_wocompletionarecreatedflag',
    // 	label: 'Created Flag',
    // 	type: UISERVERWIDGETMODULE.FieldType.CHECKBOX,
    // });
    // woCompletionAreCreatedFlagField.updateDisplayType({
	// 	displayType: UISERVERWIDGETMODULE.FieldDisplayType.HIDDEN
	// });
    // if (woCompletionAreCreated) {
    // 	woCompletionAreCreatedFlagField.defaultValue = 'T';    	
    // } else {
    // 	woCompletionAreCreatedFlagField.defaultValue = 'F';    	
    // }
	
    // custpage_mob_pageinitmessage field is used by client script to display a message at pageInit if a popup must be displayed to indicated 
    // that the creation of wo completions is successfull
    let pageInitMessageField = form.addField({
    	id: 'custpage_mob_pageinitmessage',
    	label: 'Page init message',
    	type: UISERVERWIDGETMODULE.FieldType.TEXTAREA,
    });
    pageInitMessageField.updateDisplayType({
		displayType: UISERVERWIDGETMODULE.FieldDisplayType.HIDDEN
	});
	pageInitMessageField.defaultValue = pageInitMessage;
    
    // custpage_mob_labelstoprint field is used to store the array of internalids 
    let labelsToPrintField = form.addField({
    	id: 'custpage_mob_labelstoprint',
    	label: 'Packages to Print',
    	type: UISERVERWIDGETMODULE.FieldType.LONGTEXT,
    });
    labelsToPrintField.updateDisplayType({
		displayType: UISERVERWIDGETMODULE.FieldDisplayType.HIDDEN
	});
    if (labelsToPrint) {
    	labelsToPrintField.defaultValue = labelsToPrint.toString();    	
    }

    form.addSubmitButton({
        label : translationHandle.quality.CONTINUE_BTN()
    });
	// form.addButton({
	// 	id: 'custpage_mob_refreshbutton',
	// 	label: translationHandle.quality.REFRESH_BTN(),
	// 	functionName: `refreshButton(${workOrderId})`
	// });
	getFormTemplateList(form, binValues, translationHandle);
	populateFormTemplate(form, workOrderId, binValues);
    
    return form;
}

/**
 * Create the list of lines to receive with its fields
 * @param {*} form 
 */
function getFormTemplateList(form, binValues, translationHandle) {
    let receiptList = form.addSublist({
    	id: 'custpage_mob_receiptlist',
    	label: translationHandle.quality.RECEIVE_GRD(),
    	type: UISERVERWIDGETMODULE.SublistType.LIST
    });
    // The checkbox to indicate the lines to process
    let checkBoxField = receiptList.addField({
        id: 'custpage_mob_fld_receiveflag',
        label: translationHandle.quality.SELECT_COL(),
        type: UISERVERWIDGETMODULE.FieldType.CHECKBOX
    });
    let isPeriodClosedField = receiptList.addField({
        id: 'custpage_mob_fld_isperiodclosed',
        label: 'is period closed',
        type: UISERVERWIDGETMODULE.FieldType.CHECKBOX
    });
    isPeriodClosedField.updateDisplayType({
    	displayType : UISERVERWIDGETMODULE.FieldDisplayType.HIDDEN
    });
    // Internal Id
    let internalidField = receiptList.addField({
    	id: 'custpage_mob_fld_internalid',
    	label: 'InternalId',
    	type: UISERVERWIDGETMODULE.FieldType.TEXT
    });
    internalidField.updateDisplayType({
    	displayType : UISERVERWIDGETMODULE.FieldDisplayType.HIDDEN
    });
    // Inventory number (lot)
    let inventoryNumberField = receiptList.addField({
    	id: 'custpage_mob_fld_inventorynumber',
    	label: translationHandle.quality.BATCH_COL(),
    	type: UISERVERWIDGETMODULE.FieldType.TEXT
    });
    inventoryNumberField.updateDisplayType({
    	displayType: UISERVERWIDGETMODULE.FieldDisplayType.ENTRY
    });
    // Package number
    let packageField = receiptList.addField({
    	id: 'custpage_mob_fld_package',
    	label: translationHandle.quality.PACKAGE_COL(),
    	type: UISERVERWIDGETMODULE.FieldType.TEXT
    });
    packageField.updateDisplayType({
    	displayType: UISERVERWIDGETMODULE.FieldDisplayType.ENTRY
    });
    // Traceability
    let traceabilityField = receiptList.addField({
    	id: 'custpage_mob_fld_traceability',
    	label: translationHandle.quality.TRACEABILITY_COL(),
    	type: UISERVERWIDGETMODULE.FieldType.TEXT
    });
    traceabilityField.updateDisplayType({
    	displayType: UISERVERWIDGETMODULE.FieldDisplayType.ENTRY
    });
    // Silo
    let siloField = receiptList.addField({
    	id: 'custpage_mob_fld_siloid',
    	label: 'Silo ID',
    	type: UISERVERWIDGETMODULE.FieldType.TEXT
    });
    siloField.updateDisplayType({
    	displayType : UISERVERWIDGETMODULE.FieldDisplayType.HIDDEN
    });
    // Silo number
    let siloNumberField = receiptList.addField({
    	id: 'custpage_mob_fld_silonumber',
    	label: translationHandle.quality.SILO_COL(),
    	type: UISERVERWIDGETMODULE.FieldType.TEXT
    });

    // Silo WO Completion: wo completion from silo record. 
	// This field is a security to prevent the new version of the program to process data created by the previous version of the program
    let siloWoCompletionField = receiptList.addField({
    	id: 'custpage_mob_fld_silowocompletion',
    	label: 'Silo WO Completion',
    	type: UISERVERWIDGETMODULE.FieldType.TEXT
    });
    siloWoCompletionField.updateDisplayType({
    	displayType : UISERVERWIDGETMODULE.FieldDisplayType.HIDDEN
    });

    // WO Completion
    let woCompletionField = receiptList.addField({
    	id: 'custpage_mob_fld_wocompletion',
    	label: 'WO Completion',
    	type: UISERVERWIDGETMODULE.FieldType.TEXT
    });
    woCompletionField.updateDisplayType({
    	displayType : UISERVERWIDGETMODULE.FieldDisplayType.HIDDEN
    });

    // Inventory status
    let inventoryStatusField = receiptList.addField({
    	id: 'custpage_mob_fld_inventorystatus',
    	label: translationHandle.quality.STATUS_COL(),
    	type: UISERVERWIDGETMODULE.FieldType.SELECT,
    	source: 'inventorystatus'
    });
    inventoryStatusField.updateDisplayType({
    	displayType: UISERVERWIDGETMODULE.FieldDisplayType.ENTRY
    });
    // Bin
    let binColumnField = receiptList.addField({
    	id: 'custpage_mob_fld_bin',
    	label: translationHandle.quality.BIN_COL(),
    	type: UISERVERWIDGETMODULE.FieldType.SELECT
    });    
    binColumnField.updateDisplayType({
    	displayType: UISERVERWIDGETMODULE.FieldDisplayType.ENTRY
    });
    // let binValues = valuesForBin(assemblyItemId, locationId);
    for (let i = 0 ; i < binValues.authorizedValues.length ; i++) {
    	let authorizedBin = binValues.authorizedValues[i];
    	let isSelected = (authorizedBin.value === binValues.defaultValue);
//    	log.debug({
//    		title: 'Bin / preferred',
//    		details: authorizedBin.value + '|' + authorizedBin.text + '-' + binValues.defaultValue + ' : ' + isSelected + ' ' + typeof isSelected
//    	});
    	binColumnField.addSelectOption({
    		value: authorizedBin.value,
    		text: authorizedBin.text,
    		isSelect: isSelected
    	});
    }
    // Quantity
    let quantityField = receiptList.addField({
    	id: 'custpage_mob_fld_quantity',
    	label: translationHandle.quality.QUANTITY_COL(),
    	type: UISERVERWIDGETMODULE.FieldType.INTEGER
    });
    quantityField.updateDisplayType({
    	displayType: UISERVERWIDGETMODULE.FieldDisplayType.ENTRY
    });
    // Quantity Received
	let quantityReceivedField = receiptList.addField({
		id: 'custpage_mob_fld_quantityreceived',
		label: translationHandle.quality.RECEIVED_COL(),
		type: UISERVERWIDGETMODULE.FieldType.INTEGER
	});
    // Quantity Received HIDDEN. An hidden field is required for the client script to read 
	// the value with getCurrentSublistValue() function
	let quantityReceivedHiddenField = receiptList.addField({
		id: 'custpage_mob_fld_quantityreceivedhidden',
		label: translationHandle.quality.RECEIVED_COL(),
		type: UISERVERWIDGETMODULE.FieldType.INTEGER
	});
	quantityReceivedHiddenField.updateDisplayType({
		displayType: UISERVERWIDGETMODULE.FieldDisplayType.HIDDEN
	});

    // WO completion date
    let woCompletionDateField = receiptList.addField({
    	id: 'custpage_mob_fld_wocompletiondate',
    	label: translationHandle.quality.DATE_COL(),
    	type: UISERVERWIDGETMODULE.FieldType.DATE
    });

	// system id field to check data validity
	let sysIdField = receiptList.addField({
		id: 'custpage_mob_fld_sysid',
		label: 'sysid',
		type: UISERVERWIDGETMODULE.FieldType.TEXT
	});
	sysIdField.updateDisplayType({
		displayType: UISERVERWIDGETMODULE.FieldDisplayType.HIDDEN
	});
    
    receiptList.addMarkAllButtons();
}

/**
 * Fill the list of lines to receive with data
 * @param {*} form 
 */
function populateFormTemplate(form, workOrderId, binValues) {    
    let defaultStatus = retrieveDefaultInventoryStatus();
    let totalReceived = 0;

	let frozenPeriodEndDate = retrieveFrozenPeriodDate();
	/*
	* removed because we can't give access to accountingperiod record
	let closedPeriodEndDate;
	let sqlResults = QUERYMODULE.runSuiteQL({
		query: `Select max(enddate) as periodenddate
			From accountingperiod
			Where alllocked = 'T'`
	}).asMappedResults();
	if (sqlResults.length > 0) {
		closedPeriodEndDate = FORMATMODULE.parse({
			value: sqlResults[0].periodenddate,
			type: FORMATMODULE.Type.DATE
		});
	}
	*/
    
	let precompletionSearchObj = SEARCHMODULE.create({
		type: 'customrecord_mob_wo_pre_completion',
		columns: ['internalid', 'custrecord_mob_cmpl_lotnumber', 'custrecord_mob_cmpl_packagenumber', 'custrecord_mob_cmpl_traceability', 
			'custrecord_mob_cmpl_bin', 'custrecord_mob_cmpl_quantity', 'custrecord_mob_cmpl_quantityreceived', 'custrecord_mob_cmpl_siloid', 
			'custrecord_mob_cmpl_sourcedsilonumber', 'custrecord_mob_cmpl_sourcedwocompletion', 'custrecord_mob_cmpl_lotstatus', 
			'custrecord_mob_cmpl_wocompletion', 'custrecord_mob_cmpl_wocompletion.trandate', 'custrecord_mob_cmpl_sysid'],
		filters: [
			['custrecord_mob_cmpl_wonumber', 'is', workOrderId], 
			'AND',
            [
                ['custrecord_mob_cmpl_wocompletion','noneof','@NONE@'],
                'AND',['custrecord_mob_cmpl_wocompletion.mainline','is','T'],
                'OR',['custrecord_mob_cmpl_wocompletion','anyof','@NONE@']
            ]
		]
	});
	let receiptList = form.getSublist({
    	id: 'custpage_mob_receiptlist'
    });
	let sublistLine = 0;
	precompletionSearchObj.run().each(function(result) {
		// logRecord(result, 'result');
		let internalid = result.getValue('internalid');
		if (internalid) {
			receiptList.setSublistValue({
				id: 'custpage_mob_fld_internalid',
				line: sublistLine,
				value: internalid
			});
		}
		let inventoryNumber = result.getValue('custrecord_mob_cmpl_lotnumber');
		if (inventoryNumber) {
			receiptList.setSublistValue({
				id: 'custpage_mob_fld_inventorynumber',
				line: sublistLine,
				value: inventoryNumber
			});
		}
		let packageNumber = result.getValue('custrecord_mob_cmpl_packagenumber');
		if (packageNumber) {
			receiptList.setSublistValue({
				id: 'custpage_mob_fld_package',
				line: sublistLine,
				value: packageNumber
			});
		}
		let traceability = result.getValue('custrecord_mob_cmpl_traceability');
		if (traceability) {
			receiptList.setSublistValue({
				id: 'custpage_mob_fld_traceability',
				line: sublistLine,
				value: traceability
			});
		}
		let silo = result.getValue('custrecord_mob_cmpl_siloid');
		if (silo) {
			receiptList.setSublistValue({
				id: 'custpage_mob_fld_siloid',
				line: sublistLine,
				value: silo
			});
		}
		let siloNumber = result.getValue('custrecord_mob_cmpl_sourcedsilonumber');
		if (siloNumber) {
			receiptList.setSublistValue({
				id: 'custpage_mob_fld_silonumber',
				line: sublistLine,
				value: siloNumber
			});
		}
		let siloWoCompletion = result.getValue('custrecord_mob_cmpl_sourcedwocompletion');
		if (siloWoCompletion){
			receiptList.setSublistValue({
				id: 'custpage_mob_fld_silowocompletion',
				line: sublistLine,
				value: siloWoCompletion
			});
		}
		let woCompletionId = result.getValue('custrecord_mob_cmpl_wocompletion');
		if (woCompletionId){
			receiptList.setSublistValue({
				id: 'custpage_mob_fld_wocompletion',
				line: sublistLine,
				value: woCompletionId
			});
		}
		let woCompletionDate = result.getValue({ name: 'trandate', join: 'custrecord_mob_cmpl_wocompletion' });
		if (woCompletionDate) {
			receiptList.setSublistValue({
				id: 'custpage_mob_fld_wocompletiondate',
				line: sublistLine,
				value: woCompletionDate
			});
			receiptList.setSublistValue({
				id: 'custpage_mob_fld_isperiodclosed',
				line: sublistLine,
				//value: checkPeriodIsClosed(woCompletionDate, frozenPeriodEndDate, closedPeriodEndDate) ? 'T': 'F'
				value: checkPeriodIsClosed(woCompletionDate, frozenPeriodEndDate) ? 'T': 'F'
			});
		}
		let inventoryStatus = result.getValue('custrecord_mob_cmpl_lotstatus')  || defaultStatus;
		if (inventoryStatus){
			receiptList.setSublistValue({
				id: 'custpage_mob_fld_inventorystatus',
				line: sublistLine,
				value: inventoryStatus
			});
		}
				
		// Eric Moulin - 06/04/2021 - COMMENT BEGIN
//		let bin = result.getValue('custrecord_mob_cmpl_bin');
//		if (bin) {
//			receiptList.setSublistValue({
//				id: 'custpage_mob_fld_bin',
//				line: sublistLine,
//				value: bin
//			});
//		} else if (binValues.defaultValue) {
//			receiptList.setSublistValue({
//				id: 'custpage_mob_fld_bin',
//				line: sublistLine,
//				value: binValues.defaultValue
//			});			
//		}
		// Eric Moulin - 06/04/2021 - COMMENT END
		
		// Eric Moulin - 06/04/2021 - ADD BEGIN
		let bin = result.getValue('custrecord_mob_cmpl_bin') || binValues.defaultValue || binValues.authorizedValues[0].value;
		receiptList.setSublistValue({
			id: 'custpage_mob_fld_bin',
			line: sublistLine,
			value: bin
		});
		// Eric Moulin - 06/04/2021 - ADD END

		let quantityReceived = result.getValue('custrecord_mob_cmpl_quantityreceived') || '0';
		if (!woCompletionId) {
			quantityReceived = '0';
		}
		let quantity = parseInt(quantityReceived, 10) > 0 ? quantityReceived : result.getValue('custrecord_mob_cmpl_quantity') || '0';
		totalReceived += parseInt(quantityReceived, 10);
		receiptList.setSublistValue({
			id: 'custpage_mob_fld_quantity',
			line: sublistLine,
			value: quantity
		});
		receiptList.setSublistValue({
			id: 'custpage_mob_fld_quantityreceived',
			line: sublistLine,
			value: quantityReceived
		});
		receiptList.setSublistValue({
			id: 'custpage_mob_fld_quantityreceivedhidden',
			line: sublistLine,
			value: quantityReceived
		});
		let sysId = result.getValue('custrecord_mob_cmpl_sysid') || 0;
		logVar(sysId, 'sysId');
		receiptList.setSublistValue({
			id: 'custpage_mob_fld_sysid',
			line: sublistLine,
			value: sysId
		});
		
		sublistLine++;
		return true;
	});

    // Total received
    var totalReceivedField = form.getField({
    	id: 'custpage_mob_totalreceived'
    });
	totalReceivedField.defaultValue = totalReceived;
}

function retrieveFrozenPeriodDate() {
	let frozenPeriod = QUALITYMODULE.retrieveFrozenPeriod();
	let date = new Date('2020/12/31');
	if (frozenPeriod) {
		date = frozenPeriod.period.endDate;
	}
	return date;
}

/**
 * 
 * @param {String} trandate a string date because it is retrieved from a search
 * @param {Date} frozenDate 
 * @returns {boolean}
 */
function checkPeriodIsClosed(stringDate, frozenDate, closedPeriodEndDate) {
	// logVar(stringDate, 'stringDate');
	let dateVal = FORMATMODULE.parse({
		value: stringDate,
		type: FORMATMODULE.Type.DATE
	});
	return dateVal <= Math.max(frozenDate, closedPeriodEndDate || frozenDate);
}

function retrieveDefaultInventoryStatus() {
	var defaultStatus;
	var searchObj = SEARCHMODULE.create({
		type: SEARCHMODULE.Type.INVENTORY_STATUS,
		columns: ['internalid', 'name'],
		filters: [SEARCHMODULE.createFilter({
			name: 'state',
			operator: SEARCHMODULE.Operator.IS,
			values: 'D'
			})]
	});
	var result;
	try {
		defaultStatus = searchObj.run().getRange({start: 0, end: 1})[0].getValue('internalid');
	} catch (ex) {
		// Nothing to do. There is just no default value
	}
	return defaultStatus;
}

function fillPrecompletions(completionListLines, resultColumns) {
	// logRecord(completionListLines, 'completionListLines');
	let precompletions = [];
	for (let i = 0 ; i < completionListLines.length; i++) {
		let receiveFlag = getSublistValue({
			resultColumns: resultColumns,
			resultLine: completionListLines[i],
			fieldId: 'custpage_mob_fld_receiveflag'		
		});
		let internalid = getSublistValue({
			resultColumns: resultColumns,
			resultLine: completionListLines[i],
			fieldId: 'custpage_mob_fld_internalid'			
		});
		let inventoryNumber = getSublistValue({
			resultColumns: resultColumns,
			resultLine: completionListLines[i],
			fieldId: 'custpage_mob_fld_inventorynumber'			
		});
		let packageNumber = getSublistValue({
			resultColumns: resultColumns,
			resultLine: completionListLines[i],
			fieldId: 'custpage_mob_fld_package'			
		});
		let traceability = getSublistValue({
			resultColumns: resultColumns,
			resultLine: completionListLines[i],
			fieldId: 'custpage_mob_fld_traceability'			
		});
		let siloId = getSublistValue({
			resultColumns: resultColumns,
			resultLine: completionListLines[i],
			fieldId: 'custpage_mob_fld_siloid'			
		});
		let siloNumber = getSublistValue({
			resultColumns: resultColumns,
			resultLine: completionListLines[i],
			fieldId: 'custpage_mob_fld_silonumber'			
		});
		let siloWoCompletion = getSublistValue({
			resultColumns: resultColumns,
			resultLine: completionListLines[i],
			fieldId: 'custpage_mob_fld_silowocompletion'			
		});
		let woCompletion = getSublistValue({
			resultColumns: resultColumns,
			resultLine: completionListLines[i],
			fieldId: 'custpage_mob_fld_wocompletion'			
		});
		let inventoryStatus = getSublistValue({
			resultColumns: resultColumns,
			resultLine: completionListLines[i],
			fieldId: 'custpage_mob_fld_inventorystatus'			
		});
		let bin = getSublistValue({
			resultColumns: resultColumns,
			resultLine: completionListLines[i],
			fieldId: 'custpage_mob_fld_bin'			
		});
		let quantity = getSublistValue({
			resultColumns: resultColumns,
			resultLine: completionListLines[i],
			fieldId: 'custpage_mob_fld_quantity'			
		});
		let quantityReceived = getSublistValue({
			resultColumns: resultColumns,
			resultLine: completionListLines[i],
			fieldId: 'custpage_mob_fld_quantityreceived'			
		});
		let sysId = getSublistValue({
			resultColumns: resultColumns,
			resultLine: completionListLines[i],
			fieldId: 'custpage_mob_fld_sysid'			
		});

		let quantityToReceive = parseInt(quantity || 0, 10);		
		quantityReceived = parseInt(quantityReceived || 0, 10);
		let precompletion = {
			internalid: internalid,
			receiveFlag: (receiveFlag == 'T'),
			siloId: siloId,
			siloNumber: siloNumber,
			siloWoCompletion: siloWoCompletion,
			woCompletion: woCompletion,
			inventoryNumber: inventoryNumber,
			packageNumber: packageNumber,
			traceability: traceability,
			inventoryStatus: inventoryStatus,
			bin: bin,
			quantityReceived: quantityReceived,
			quantityToReceive: quantityToReceive,
			sysId: sysId
		};
		precompletions.push(precompletion);
	}
	return precompletions;
}

function findCompletionBySilo(completionsBySilo, siloId) {
	for (var i = 0 ; i < completionsBySilo.length ; i++) {
		if (completionsBySilo[i].siloId == siloId) {
			return completionsBySilo[i];
		}		
	}
}

function valuesForBin(itemId, locationId) {
	var binValues = {};
	binValues.defaultValue = getItemPreferredBin(itemId, locationId);
	binValues.authorizedValues = getLocationBins(locationId);
	return binValues;
}

function getLocationBins(locationId) {
	var binSearch = SEARCHMODULE.create({
		type: SEARCHMODULE.Type.BIN,
		columns: ['internalid', 'binnumber'],
		filters: [SEARCHMODULE.createFilter({
			name: 'location',
			operator: SEARCHMODULE.Operator.IS,
			values: locationId
			})]
	});
	var binArray = [];
	binSearch.run().each(function(result) {
		var bin = {};
		bin.value = result.getValue('internalid');
		bin.text = result.getValue('binnumber');
		binArray.push(bin);
		return true;
	});
	return binArray;
}

function getItemPreferredBin(itemId, locationId) {	
	var itemSearch = SEARCHMODULE.create({
		type: SEARCHMODULE.Type.ITEM,
		columns: ['internalid'],
		filters: [SEARCHMODULE.createFilter({
			name: 'internalid',
			operator: SEARCHMODULE.Operator.IS,
			values: itemId
			})]
	});
	var itemSearchResult = itemSearch.run().getRange({start: 0, end: 1})[0];
	
	var itemRecord = RECORDMODULE.load({
		type: itemSearchResult.recordType,
		id: itemSearchResult.id
	});
	
	var lineCount = itemRecord.getLineCount({
		sublistId: 'binnumber'
	});
	
	// Read item preferred bin
	for (var i = 0 ; i < lineCount ; i++) {
		var preferredBin = itemRecord.getSublistValue({
			sublistId: 'binnumber',
			fieldId: 'preferredbin',
			line: i
		});
		var binLocation = itemRecord.getSublistValue({
			sublistId: 'binnumber',
			fieldId: 'location',
			line: i
		});
		if (preferredBin && binLocation == locationId) {
			return itemRecord.getSublistValue({
				sublistId: 'binnumber',
				fieldId: 'binnumber',
				line: i
			});
		}
	}
	return '';
}

/**
 * Parse an HTML line corresponding to a table line
 * @param options
 * @returns {String}
 */
function getSublistValue(options) {
	/*
	options.resultLine
	options.resultColumns
	options.fieldId
	*/
	let delimiter = /\u0001/;
	let result = '';
	let column = options.resultColumns.indexOf(options.fieldId);
	let fieldsValues = options.resultLine.split(delimiter);
	if (column >= 0 && column < fieldsValues.length) {
		result = fieldsValues[column];
	}
	return result;
}

function logRecord(record, logTitle) {
	let logRecord = JSON.stringify(record) || 'UNDEFINED';
	let recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);		
	}
}

function logVar(value, title) {
	log.debug({
		title: title + ' (' + typeof value + ')',
		details: value 
	});	
}
