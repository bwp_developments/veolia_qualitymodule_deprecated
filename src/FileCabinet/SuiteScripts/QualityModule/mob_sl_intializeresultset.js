var ERRORMODULE, HTTPMODULE, RECORDMODULE, REDIRECTMODULE, SEARCHMODULE, QUALITYMODULE;

/**
 * @NApiVersion 2.1
 * @NScriptType Suitelet
 */
define(['N/error', 'N/http', 'N/record', 'N/redirect', 'N/search', './qualityModule'], runSuitelet);

function runSuitelet(error, http, record, redirect, search, qualityModule) {
    ERRORMODULE= error;
    HTTPMODULE= http;
	RECORDMODULE= record;
	REDIRECTMODULE= redirect;
	SEARCHMODULE= search;
    QUALITYMODULE= qualityModule;
	
	let returnObj = {};
	returnObj['onRequest'] = _onRequest;
	return returnObj;
}
   
/**
 * Defines the Suitelet script trigger point.
 * @param {Object} scriptContext
 * @param {ServerRequest} scriptContext.request - Incoming request
 * @param {ServerResponse} scriptContext.response - Suitelet response
 * @since 2015.2
 */
function _onRequest(context) {
    let method = context.request.method;
    //logRecord(context, 'context');

	log.debug(method);
    if (method == 'GET') {
    	getFunction(context);
    } else if (method == 'POST') {
    	// postFunction(context);
    }
}

function getFunction(context) {
    let productionSilo = context.request.parameters.custparam_mob_productionsilo;
    let editMode = (context.request.parameters.custparam_mob_editmode || 'F') === 'T';
    let fromUrl = context.request.headers.referer;

    if (!productionSilo) { return; }

    let siloRecord = RECORDMODULE.load({
        type: 'customrecord_mob_wo_productionsilo',
        id: productionSilo,
        isDynamic: false
    });
    logRecord(siloRecord, 'siloRecord');
    
    let workOrder = siloRecord.getValue({ fieldId: 'custrecord_mob_cmpl_silowonumber' })
    if (!workOrder) {
        throw ERRORMODULE.create({
            name: 'BWP_UNEXPECTED_ERROR',
            message: `Production silo ${productionSilo} is not attached to any work order`
        });
    }

    let woInfo = retrieveWorkOrderInfo(workOrder);
    let siloName = siloRecord.getValue({ fieldId: 'name' });
    let siloResultSet = siloRecord.getValue({ fieldId: 'custrecord_mob_cmpl_qualresultset' });

    let qualityResultSet = QUALITYMODULE.initializeResultSet(woInfo.item, woInfo.customer, 
        woInfo.internalId, woInfo.subsidiary, productionSilo, 
        siloName, siloResultSet
    );

    if (!siloResultSet && qualityResultSet) {
        siloRecord.setValue({
            fieldId: 'custrecord_mob_cmpl_qualresultset',
            value: qualityResultSet
        });
        siloRecord.save();
    }
  	
    if (qualityResultSet) {
        context.response.sendRedirect({
            type: HTTPMODULE.RedirectType.RECORD,
            identifier: 'customrecord_mob_qualityresultset',
            id: qualityResultSet,
            editMode: editMode,
            parameters: { custparam_mob_fromurl: fromUrl }
        });
        // REDIRECTMODULE.toRecord({
        //     id: qualityResultSet,
        //     type: 'customrecord_mob_qualityresultset',
        //     isEditMode: true
        // });
    } else {
        context.response.write('Unexpected error - failed to retrieve a valid resultset');
    }
}

function postFunction(context) {

}

function retrieveProductionSiloInfo(productionSilo) {    
	let productionSiloInfo = {
        workOrder: ''
    };
    let productionSiloFieldsValues = SEARCHMODULE.lookupFields({
        type: 'customrecord_mob_wo_productionsilo',
        id: productionSilo,
        columns: ['custrecord_mob_cmpl_silo', 'custrecord_mob_cmpl_silowonumber', 'custrecord_mob_cmpl_qualresultset']
    });
    if ('custrecord_mob_cmpl_silo' in productionSiloFieldsValues) {
        productionSiloInfo.siloNumber = productionSiloFieldsValues.custrecord_mob_cmpl_silo;
    }
    if ('custrecord_mob_cmpl_silowonumber' in productionSiloFieldsValues && productionSiloFieldsValues.custrecord_mob_cmpl_silowonumber.length > 0) {
        productionSiloInfo.workOrder = productionSiloFieldsValues.custrecord_mob_cmpl_silowonumber[0].value;
    }
    if (!productionSiloInfo.workOrder) {
        throw ERRORMODULE.create({
            name: 'BWP_UNEXPECTED_ERROR',
            message: `Production silo ${productionSilo} is not attached to any work order`
        });
    }
    if ('custrecord_mob_cmpl_qualresultset' in productionSiloFieldsValues && productionSiloFieldsValues.custrecord_mob_cmpl_qualresultset.length > 0) {
        productionSiloInfo.qualityResultSet = productionSiloFieldsValues.custrecord_mob_cmpl_qualresultset[0].value;
    }
    return productionSiloInfo;
}

function retrieveWorkOrderInfo(workOrder) {
    let workOrderInfo = {
        internalId: workOrder,
        customer: '',
        item: '',
        subsidiary: '',
        tranid: ''
    };
    let woFieldsValues = SEARCHMODULE.lookupFields({
		type: SEARCHMODULE.Type.WORK_ORDER,
		id: workOrder,
		columns: ['tranid', 'item', 'entity', 'status', 'subsidiary'] //['assemblyitem', 'quantity']
	});
    workOrderInfo.tranid = woFieldsValues.tranid;
	if ('entity' in woFieldsValues && woFieldsValues.entity.length > 0) {
        workOrderInfo.customer = woFieldsValues.entity[0].value;
	}
	if ('item' in woFieldsValues && woFieldsValues.item.length > 0) {
        workOrderInfo.item = woFieldsValues.item[0].value;
	}
	if ('subsidiary' in woFieldsValues && woFieldsValues.subsidiary.length > 0) {
        workOrderInfo.subsidiary = woFieldsValues.subsidiary[0].value;
	}
    let emptyField;
    if (!workOrderInfo.subsidiary) {
        emptyField = 'subsidiary';
    } else if (!workOrderInfo.item) {
        emptyField = 'item';
    }
    if (emptyField) {
        throw ERRORMODULE.create({
            name: 'BWP_UNEXPECTED_ERROR',
            message: `Field ${emptyField} on work order ${workOrderInfo.tranid} should not be empty`
        });
    }
    return workOrderInfo;
}

function logRecord(record, logTitle) {
	let logRecord = JSON.stringify(record) || 'UNDEFINED';
	let recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);		
	}
}

function logVar(value, logTitle) {
	log.debug({
		title: logTitle + ' (' + typeof value + ')',
		details: value 
	});	
}