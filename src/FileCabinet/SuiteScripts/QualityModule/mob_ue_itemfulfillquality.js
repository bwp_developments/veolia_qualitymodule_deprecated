var RECORDMODULE, RUNTIMEMODULE, SEARCHMODULE, TRANSLATIONMODULE, UISERVERWIDGETMODULE, URLMODULE;

/*
 * WARNING: quality status on WO Completion are handled by workflow
 * This script works in cooperation with the Outgoing Quality workflow
 */

/**
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/runtime', 'N/search', 'N/translation', 'N/ui/serverWidget', 'N/url'], runUserEvent);

function runUserEvent(record, runtime, search, translation, uiserverWidget, url, qualityModule) {
	RECORDMODULE= record;
	RUNTIMEMODULE= runtime;
	SEARCHMODULE= search;
	TRANSLATIONMODULE= translation;
	UISERVERWIDGETMODULE= uiserverWidget;
	URLMODULE= url;
	
	var returnObj = {};
	returnObj['beforeLoad'] = _beforeLoad;
//	returnObj['beforeSubmit'] = _beforeSubmit;
	returnObj['afterSubmit'] = _afterSubmit;
	return returnObj;
}
   
/**
 * Function definition to be triggered before record is loaded.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {string} scriptContext.type - Trigger type
 * @param {Form} scriptContext.form - Current form
 * @Since 2015.2
 */
function _beforeLoad(scriptContext) {
	log.debug('BeforeLoad started');
//	logRecord(scriptContext, 'scriptContext');
	
	if (scriptContext.type == scriptContext.UserEventType.VIEW) {
		var itemFulfillmentId = scriptContext.newRecord.getValue('id');
		
		/*
		 * does not work: impossible to create a transaction line field on item fulfillment with an URL link
		 * see case 3825176
		var itemSublist = scriptContext.form.getSublist({
			id: 'item'
		});
		logRecord(itemSublist, 'itemSublist');
		for (var i = 0 ; i < itemSublist.lineCount ; i++) {
			// Set value of Packing List URL
			var packingListUrl = URLMODULE.resolveScript({
				scriptId: 'customscript_mob_sl_packinglist',
				deploymentId: 'customdeploy_mob_sl_packinglist',
				params: {
					custparam_mob_itemfulfillment: itemFulfillmentId,
					custparam_mob_fulfillmentline: i
				}
			});
			log.debug({
				title: 'packingListUrl',
				details: packingListUrl
			});
//			itemSublist.setSublistValue({
//				id: 'custcol_mob_packinglist',
//				line: i,
//				value: packingListUrl
//			});
		}
		*/
		
		// Add PACKING button		
    	//get the url for the suitelet - add woRecordId parameter
    	var packingSuiteletURL = URLMODULE.resolveScript({
    		scriptId: 'customscript_mob_sl_itemfulfillmentlines',
    		deploymentId: 'customdeploy_mob_sl_itemfulfillmentlines',
    		params: {custparam_mob_itemfulfillment: itemFulfillmentId},
    		returnExternalUrl: false
    	});

    	//hook point for the button that will be fired client side;
//    	var scr1 = "require([], function() { window.open('"+packingSuiteletURL+"');});";
    	var scr1 = "require([], function() { window.location.assign('"+packingSuiteletURL+"');});";

    	//add button to the form; when clicked, call the scr function 
		scriptContext.form.addButton({
			id : 'custpage_mob_button_packing',
			label : TRANSLATIONMODULE.get({collection: 'custcollection_mob_outgoingquality', key: 'PACKING_BTN'})(),
			functionName : scr1
		});
		

		
		// Add MEASURES button		
    	//get the url for the suitelet - add woRecordId parameter
    	var printMeasuresSuiteletURL = URLMODULE.resolveScript({
    		scriptId: 'customscript_mob_sl_printtestresults',
    		deploymentId: 'customdeploy_mob_sl_printtestresults',
    		params: {custparam_mob_itemfulfillment: itemFulfillmentId},
    		returnExternalUrl: false
    	});

    	//hook point for the button that will be fired client side;
    	var scr2 = "require([], function() { window.open('"+printMeasuresSuiteletURL+"');});";

    	//add button to the form; when clicked, call the scr function 
		scriptContext.form.addButton({
			id : 'custpage_mob_button_printmeasures',
			label : TRANSLATIONMODULE.get({collection: 'custcollection_mob_outgoingquality', key: 'PRINT_MEASURES_BTN'})(),
			functionName : scr2
		});
		
		
		
		// Add Picking List button
    	//get the url for the suitelet - add woRecordId parameter
    	var printPackingListSuiteletURL = URLMODULE.resolveScript({
    	 'scriptId':'customscript_mob_sl_packinglistaspdf',
    	 'deploymentId':'customdeploy_mob_sl_packinglistaspdf',
    	 'returnExternalUrl': false
    	}) + '&mob_itemfulfillmentrecordid=' + itemFulfillmentId;

    	//hook point for the button that will be fired client side;
    	var scr3 = "require([], function() { window.open('"+printPackingListSuiteletURL+"');});";


    	//add button to the form; when clicked, call the scr function 
		scriptContext.form.addButton({
			id : 'custpage_mob_button_printpackinglist',
			label : TRANSLATIONMODULE.get({collection: 'custcollection_mob_outgoingquality', key: 'PACKING_LIST_BTN'})(),
			functionName : scr3
		});
	}
	
	log.debug('BeforeLoad completed');
}

/**
 * Function definition to be triggered before record is loaded.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {Record} scriptContext.oldRecord - Old record
 * @param {string} scriptContext.type - Trigger type
 * @Since 2015.2
 */
function _beforeSubmit(scriptContext) {
	log.debug('beforeSubmit started');
	
	var itemFulfillRecord = scriptContext.newRecord;
	logRecord(itemFulfillRecord, 'itemFulfillRecord');
	var lineCount = itemFulfillRecord.getLineCount({
		sublistId: 'item'
	});
	log.debug({
		title: 'lineCount',
		details: lineCount
	});
	// inventoryassignment and iteminventorydetail are visible in the logged record but can't be accessed thru the API.
	// It works for others customers ==> probaly a regression due to a bundle (WMS bundle ?)
	// getLineCount return -1
	var lineCount = itemFulfillRecord.getLineCount({
		sublistId: 'inventoryassignment'
	});
	log.debug({
		title: 'inventoryassignment',
		details: lineCount
	});
	var lineCount = itemFulfillRecord.getLineCount({
		sublistId: 'iteminventorydetail'
	});
	log.debug({
		title: 'lineCount',
		details: lineCount
	});
	lineCount	 = 2;
	var modifiedLines = []
	for (var i = 0 ; i < lineCount ; i++) {
		var modifiedLine = {
			fulfillment: itemFulfillRecord.getValue('id'),
			line: itemFulfillRecord.getSublistValue({
				sublistId: 'iteminventorydetail',
				fieldId: 'lineid',
				line: i
			}),
			inventories: []
		}
		var sys_id = itemFulfillRecord.getSublistValue({
			sublistId: 'iteminventorydetail',
			fieldId: 'sys_id',
			line: i
		});
		
		var inventLineCount = itemFulfillRecord.getLineCount({
			sublistId: 'inventoryassignment'
		});
		for (var j = 0 ; j < inventLineCount ; j++) {
			var sys_parentid = itemFulfillRecord.getSublistValue({
				sublistId: 'inventoryassignment',
				fieldId: 'sys_parentid',
				line: j
			});
			if (sys_parentid == sys_id) {
				var inventory = itemFulfillRecord.getSublistValue({
					sublistId: 'inventoryassignment',
					fieldId: 'issueinventorynumber_display',
					line: j
				});
				modifiedLine.inventories.push(inventory);
			}
		}
		modifiedLines.push(modifiedLine);
	}

	logRecord(modifiedLines, 'modifiedLines');
	
	log.debug('beforeSubmit done');
}

/**
 * Function definition to be triggered before record is loaded.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {Record} scriptContext.oldRecord - Old record
 * @param {string} scriptContext.type - Trigger type
 * @Since 2015.2
 */
function _afterSubmit(scriptContext) {
	log.debug('afterSubmit started');
	
	var itemFulfillRecord = scriptContext.newRecord;
//	logRecord(itemFulfillRecord, 'itemFulfillRecord');
	
	// Check all inventory details and verify they match the packing list. In case it does not match
	
	var lineCount = itemFulfillRecord.getLineCount({
		sublistId: 'item'
	});
	
	var fulfillmmentId = itemFulfillRecord.getValue('id');
	var fulfillmentAssignments = [];
	
	// Retrieve all inventory details
	for (var i = 0 ; i < lineCount ; i++) {
		var fulfillmentAssignment = {
			fulfillmentLine: itemFulfillRecord.getSublistValue({
				sublistId: 'item',
				fieldId: 'line',
				line: i
			}),
			inventoryAssignments: [],
			precompletions: []
		};
		var inventoryDetailRecord = itemFulfillRecord.getSublistSubrecord({
			sublistId: 'item',
			fieldId: 'inventorydetail',
			line: i			
		});
//		logRecord(inventoryDetailRecord, 'inventoryDetailRecord');
		var inventoryLineCount = inventoryDetailRecord.getLineCount({
			sublistId: 'inventoryassignment'			
		});
		for (var j = 0 ; j < inventoryLineCount ; j++) {
			var inventoryAssignment = {
				inventoryNumber: inventoryDetailRecord.getSublistValue({
					sublistId: 'inventoryassignment',
					fieldId: 'issueinventorynumber',
					line: j
				}),
				inventoryNumberDisplay: inventoryDetailRecord.getSublistValue({
					sublistId: 'inventoryassignment',
					fieldId: 'issueinventorynumber_display',
					line: j
				}),
				binNumber: inventoryDetailRecord.getSublistValue({
					sublistId: 'inventoryassignment',
					fieldId: 'binnumber',
					line: j
				}),
				quantityInventory: inventoryDetailRecord.getSublistValue({
					sublistId: 'inventoryassignment',
					fieldId: 'quantity',
					line: j
				}),
				quantityPacked: 0
			};
			addAssignmentToFulfillmentAssignments(fulfillmentAssignment, inventoryAssignment);			
		}
		fulfillmentAssignments.push(fulfillmentAssignment);
	}
	
//	logRecord(fulfillmentAssignments, 'fulfillmentAssignments');
	
	// Complete the list with the packed lots
	var searchObj = SEARCHMODULE.create({
		type: 'customrecord_mob_wo_pre_completion',
		columns: ['internalid', 
			SEARCHMODULE.createColumn({
				name: 'custrecord_mob_cmpl_shipmentline',
				sort: SEARCHMODULE.Sort.ASC
			}),
			SEARCHMODULE.createColumn({
				name: 'custrecord_mob_cmpl_lotnumber',
				sort: SEARCHMODULE.Sort.ASC
			}),
			'custrecord_mob_cmpl_bin', 'custrecord_mob_cmpl_quantityreceived'],
		filters: [SEARCHMODULE.createFilter({
			name: 'custrecord_mob_cmpl_shipmentnumber',
			operator: SEARCHMODULE.Operator.IS,
			values: fulfillmmentId
			})]		
	});
	
	searchObj.run().each(function(result) {
//		logRecord(result, 'result');
		var precompletionId = result.getValue('internalid');
		var shipmentLine = result.getValue('custrecord_mob_cmpl_shipmentline');
		var inventoryNumberDisplay = result.getValue('custrecord_mob_cmpl_lotnumber');
		var binNumber = result.getValue('custrecord_mob_cmpl_bin');
		var packedQuantity = result.getValue('custrecord_mob_cmpl_quantityreceived');
		addPackedQuantityToFulfillmentAssignments(fulfillmentAssignments, shipmentLine, inventoryNumberDisplay, binNumber, packedQuantity, precompletionId);
		return true;
	});

	logRecord(fulfillmentAssignments, 'fulfillmentAssignments');
	
	
	// Clear packings that does not match fulfillment
	for (var i = 0 ; i < fulfillmentAssignments.length ; i++) {
		var isValid = true;
		for (var j = 0 ; j < fulfillmentAssignments[i].inventoryAssignments.length ; j ++) {
			if (fulfillmentAssignments[i].inventoryAssignments[j].quantityInventory != fulfillmentAssignments[i].inventoryAssignments[j].quantityPacked) {
				isValid = false;
				break;
			}
		}
		
		if (!isValid) {
			for (var j = 0 ; j < fulfillmentAssignments[i].precompletions.length ; j++) {
				RECORDMODULE.submitFields({
					type: 'customrecord_mob_wo_pre_completion',
					id: fulfillmentAssignments[i].precompletions[j],
					values: {
						custrecord_mob_cmpl_shipmentnumber: '',
						custrecord_mob_cmpl_shipmentline: ''
					}
				});				
			}
		}		
	}
	
	log.debug('afterSubmit done');
}

function addPackedQuantityToFulfillmentAssignments(fulfillmentAssignments, shipmentLine, inventoryNumberDisplay, binNumber, quantity, precompletionId) {
	var found = false;
	for (var i = 0 ; i < fulfillmentAssignments.length ; i++) {
		if (fulfillmentAssignments[i].fulfillmentLine == shipmentLine) {
			for (var j = 0 ; j < fulfillmentAssignments[i].inventoryAssignments.length ; j++) {
				if (fulfillmentAssignments[i].inventoryAssignments[j].inventoryNumberDisplay == inventoryNumberDisplay &&
						fulfillmentAssignments[i].inventoryAssignments[j].binNumber == binNumber) {
					found = true;
					fulfillmentAssignments[i].inventoryAssignments[j].quantityPacked = parseInt(fulfillmentAssignments[i].inventoryAssignments[j].quantityPacked, 10) + parseInt(quantity, 10);
					fulfillmentAssignments[i].precompletions.push(precompletionId);
				}
			}
		}
	}
	if (!found) {
		RECORDMODULE.submitFields({
			type: 'customrecord_mob_wo_pre_completion',
			id: precompletionId,
			values: {
				custrecord_mob_cmpl_shipmentnumber: '',
				custrecord_mob_cmpl_shipmentline: ''
			}
		});
	}
}

function addAssignmentToFulfillmentAssignments(fulfillmentAssignment, inventoryAssignment) {
	var found = false;
	for (var i = 0 ; i < fulfillmentAssignment.inventoryAssignments.length ; i++) {
		if (fulfillmentAssignment.inventoryAssignments[i].inventoryNumber == inventoryAssignment.inventoryNumber &&
				fulfillmentAssignment.inventoryAssignments[i].binNumber == inventoryAssignment.binNumber) {
			found = true;
			fulfillmentAssignment.inventoryAssignments[i].quantity = parseInt(fulfillmentAssignment.inventoryAssignments[i].quantity, 10) + parseInt(inventoryAssignment.quantity, 10);
			break;
		}
	}
	if (!found) {
		fulfillmentAssignment.inventoryAssignments.push(inventoryAssignment);
	}
}

function logRecord(record, logTitle) {
	var logRecord = JSON.stringify(record);
	var recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);		
	}
}

function logVar(title, value) {
	log.debug({
		title: title + ' (' + typeof value + ')',
		details: value 
	});	
}

