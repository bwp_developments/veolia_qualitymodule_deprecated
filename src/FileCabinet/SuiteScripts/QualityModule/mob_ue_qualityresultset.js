var RECORDMODULE, SEARCHMODULE, TRANSLATIONMODULE, UISERVERWIDGETMODULE, QUALITYMODULE;

/**
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/search', 'N/translation', 'N/ui/serverWidget', './qualityModule'], runUserEvent);

function runUserEvent(record, search, translation, uiserverWidget, qualityModule) {
	RECORDMODULE= record;
	SEARCHMODULE= search;
	TRANSLATIONMODULE= translation;
	UISERVERWIDGETMODULE= uiserverWidget;
	QUALITYMODULE= qualityModule;
	
	var returnObj = {};
	returnObj['beforeLoad'] = _beforeLoad;
	returnObj['beforeSubmit'] = _beforeSubmit;
	returnObj['afterSubmit'] = _afterSubmit;
	return returnObj;
}
   
/**
 * Function definition to be triggered before record is loaded.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {string} scriptContext.type - Trigger type
 * @param {Form} scriptContext.form - Current form
 * @Since 2015.2
 */
function _beforeLoad(scriptContext) {
	log.debug('BeforeLoad started');
	
	if (scriptContext.request) {
		/*
		resultsetList = scriptContext.form.getSublist({ id: 'recmachcustrecord_mob_qualityresultset' });
		let record = scriptContext.newRecord;
		let lineCount = record.getLineCount({ sublistId: 'recmachcustrecord_mob_qualityresultset' });
		let fieldsIds = record.getSublistFields({ sublistId: 'recmachcustrecord_mob_qualityresultset' }).filter(x => x.includes('_mob_'));
		let data = [];
		for (let i = 0 ; i < lineCount ; i++) {
			let lineData = {
				id: record.getSublistValue({
					sublistId: 'recmachcustrecord_mob_qualityresultset',
					fieldId: 'id',
					line: i
				})
			};
			for (fieldId of fieldsIds) {
				if (line == 0) {
					let fieldObj = record.getSublistField({
						sublistId: 'recmachcustrecord_mob_qualityresultset',
						fieldId: fieldId,
						line: i
					});
					// logRecord(fieldObj, 'fieldObj');
				}
				lineData[fieldId] = record.getSublistValue({
					sublistId: 'recmachcustrecord_mob_qualityresultset',
					fieldId: fieldId,
					line: i
				});
			}
			data.push(lineData);
		}
		logRecord(data, 'data');
		*/

		// Store original status on an hidden field
		let originalStatusField = scriptContext.form.addField({
			id: 'custpage_mob_originalstatus',
			label: 'original status',
			type: UISERVERWIDGETMODULE.FieldType.TEXT
		});
		originalStatusField.updateDisplayType({
			displayType: UISERVERWIDGETMODULE.FieldDisplayType.HIDDEN
		});
		originalStatusField.defaultValue = scriptContext.newRecord.getValue({ fieldId: 'custrecord_mob_resultsetstatus' });

		// Display a link to the quality overview form --> the link is stored in a textarea field to control the way
		// the url is opened: in the actual tab, not in a new one
		let qualityFormField = scriptContext.form.getField({ id: 'custrecord_mob_resultsetqualitylink' });
		if ([scriptContext.UserEventType.VIEW].includes(scriptContext.type)) {
			let workOrderId;
			let resultSetId = scriptContext.newRecord.getValue({ fieldId: 'id' });
			logVar(resultSetId, 'resultSetId');
			
			let searchObj = SEARCHMODULE.create({
				type: 'customrecord_mob_wo_productionsilo',
				filters: [['custrecord_mob_cmpl_qualresultset', 'is', resultSetId]],
				columns: ['internalid', 'custrecord_mob_cmpl_silowonumber']
			});
			// One result set is referenced by one production silo
			let results = searchObj.run().getRange({ start: 0, end: 1 });
			if (results.length > 0) {
				workOrderId = results[0].getValue({ name: 'custrecord_mob_cmpl_silowonumber' });
				let suiteletURL = URLMODULE.resolveScript({
					scriptId: 'customscript_mob_sl_qualityoverview',
					deploymentId: 'customdeploy_mob_sl_qualityoverview',
					params: {custparam_mob_workorder: workOrderId},
					returnExternalUrl: false
				});
				if (suiteletURL) {
					let linkText = TRANSLATIONMODULE.get({collection: 'custcollection_mob_outgoingquality', key: 'QUALITY_BTN'})();
					qualityFormField.defaultValue = `<a class="dottedlink" href= "${suiteletURL}">${linkText}</a>`;
				}
			}
		}
		if (!qualityFormField.defaultValue) {
			qualityFormField.updateDisplayType({
				displayType: UISERVERWIDGETMODULE.FieldDisplayType.HIDDEN
			});
		}
	}

	log.debug('BeforeLoad completed');
}

/**
 * Function definition to be triggered before record is loaded.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {Record} scriptContext.oldRecord - Old record
 * @param {string} scriptContext.type - Trigger type
 * @Since 2015.2
 */
function _beforeSubmit(scriptContext) {
	log.debug('BeforeSubmit started');
	if (scriptContext.type == scriptContext.UserEventType.DELETE) {
		deleteResults(scriptContext.newRecord.getValue('id'));
	}
	log.debug('BeforeSubmit completed');
}

/**
 * Function definition to be triggered before record is loaded.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {Record} scriptContext.oldRecord - Old record
 * @param {string} scriptContext.type - Trigger type
 * @Since 2015.2
 */
function _afterSubmit(scriptContext) {
	log.debug('AfterSubmit started');
//	logRecord(scriptContext, 'scriptContext');
	
	// AfterSubmit is raised with the use of submitFields() method - inthis case scriptContext.type = 'xedit'
	// ==> test == 'edit' instead of != 'delete'
	// In create mode, all results are empty so there is no test type to update
	// if (scriptContext.type != 'delete') {
	if (scriptContext.type == scriptContext.UserEventType.EDIT) {
		// Loop on results and update Type field based on the quality status of the wo completion
		var resultSetRecord = scriptContext.newRecord;
		// var woCompletion = resultSetRecord.getValue('custrecord_mob_resultsetwocompletion');
		// var fieldsValues = SEARCHMODULE.lookupFields({
		// 	type: SEARCHMODULE.Type.TRANSACTION,
		// 	id: woCompletion,
		// 	columns: ['custbody_mob_status_qualite']
		// });
		
		var qualityStatus = '';
		// if ('custbody_mob_status_qualite' in fieldsValues && fieldsValues.custbody_mob_status_qualite.length > 0) {
		// 	qualityStatus = fieldsValues.custbody_mob_status_qualite[0].value;		
		// }
		qualityStatus = resultSetRecord.getValue({ fieldId: 'custrecord_mob_resultsetstatus' })
		// If the quality status is populated on the wo completion
		if (qualityStatus) {
			/*
			 * Values for Test Type are :
			 * - '1' --> Simple Test
			 * - '2' --> 72h Test
			 */
			var targetTestType = '2';
			// Quality Status '1' is 'En Attente Laboratoire'
			// Only measures entered while status is 'En Attente Laboratoire' correspond to simple test
			// Measures entered on a different status are 72h test
			if (qualityStatus == '1') {
				targetTestType = '1';
			}
			updateTestTypeOnResults(resultSetRecord, targetTestType);	
		}		
	}
	log.debug('AfterSubmit completed');
}

function deleteResults(resultSetId) {
	var searchObj = SEARCHMODULE.create({		
		type: 'customrecord_mob_qualityresult',
		columns: ['internalid'],
		filters: [SEARCHMODULE.createFilter({
			name: 'custrecord_mob_qualityresultset',
			operator: SEARCHMODULE.Operator.IS,
			values: resultSetId
		})]
	});
	searchObj.run().each(function(result) {
		RECORDMODULE['delete']({
			type: 'customrecord_mob_qualityresult',
			id: result.getValue('internalid')
		});
		return true;
	});
}

function updateTestTypeOnResults(resultSetRecord, targetTestType) {
	log.debug('updateTestTypeOnResults started');
	// logRecord(resultSetRecord, 'resultSetRecord');
	// recmachcustrecord_mob_qualityresultset
	var lineCount = resultSetRecord.getLineCount({
		sublistId: 'recmachcustrecord_mob_qualityresultset'
	});
	for (var i = 0 ; i < lineCount ; i++) {
		var testType = resultSetRecord.getSublistValue({
			sublistId: 'recmachcustrecord_mob_qualityresultset',
			fieldId: 'custrecord_mob_resulttesttype',
			line: i
		});
		var testMeasure = resultSetRecord.getSublistValue({
			sublistId: 'recmachcustrecord_mob_qualityresultset',
			fieldId: 'custrecord_mob_resulttestmeasure',
			line: i
		});
		var testRetainedMeasure = resultSetRecord.getSublistValue({
			sublistId: 'recmachcustrecord_mob_qualityresultset',
			fieldId: 'custrecord_mob_resultretainedmeasure',
			line: i
		});
		// First time a measure is entered populate retained measure and test type fields
		// Warning : test type field must be hidden, not inline. Inline fields are not available in the script context
		// (not in oldRecord and not in newRecord) ==> getSublistValue always returns empty with INLINE fields
		if ((testMeasure || testRetainedMeasure) && !testType) {
			var resultId = resultSetRecord.getSublistValue({
				sublistId: 'recmachcustrecord_mob_qualityresultset',
				fieldId: 'id',
				line: i
			});
			if (!testMeasure) {
				testMeasure = testRetainedMeasure;
			}
			if (!testRetainedMeasure) {
				testRetainedMeasure = testMeasure;
			}
			
			RECORDMODULE.submitFields({
				type: 'customrecord_mob_qualityresult',
				id: resultId,
				values: {
					custrecord_mob_resulttesttype: targetTestType,
					custrecord_mob_resulttestmeasure: testMeasure,
					custrecord_mob_resultretainedmeasure: testRetainedMeasure
				}
			});
		}
	}
	log.debug('updateTestTypeOnResults completed');
}

function logRecord(record, logTitle) {
	var logRecord = JSON.stringify(record);
	var recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);		
	}
}

function logVar(value, title) {
	log.debug({
		title: title + ' (' + typeof value + ')',
		details: value 
	});	
}