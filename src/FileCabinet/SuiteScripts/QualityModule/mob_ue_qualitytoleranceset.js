var RUNTIMEMODULE, SEARCHMODULE, UISERVERWIDGETMODULE, QUALITYMODULE;

/**
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/runtime', 'N/search', 'N/ui/serverWidget', './qualityModule'], runUserEvent);

function runUserEvent(runtime, search, uiserverWidget, qualityModule) {
	RUNTIMEMODULE= runtime;
	SEARCHMODULE= search;
	UISERVERWIDGETMODULE= uiserverWidget;
	QUALITYMODULE= qualityModule;
	
	var returnObj = {};
	returnObj['beforeLoad'] = _beforeLoad;
	returnObj['beforeSubmit'] = _beforeSubmit;
//	returnObj['afterSubmit'] = _afterSubmit;
	return returnObj;
}
   
/**
 * Function definition to be triggered before record is loaded.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {string} scriptContext.type - Trigger type
 * @param {Form} scriptContext.form - Current form
 * @Since 2015.2
 */
function _beforeLoad(scriptContext) {
	log.debug('beforeLoad started');

	var qualitySetup = QUALITYMODULE.getQualitySetup();
	if (!qualitySetup.customerLevelFlag) {
		var customerField = scriptContext.form.getField({
			id: 'custrecord_mob_tolerancesetcustomer'
		});
		customerField.updateDisplayType({
			displayType: UISERVERWIDGETMODULE.FieldDisplayType.HIDDEN
		});
	}

	if (scriptContext.type == scriptContext.UserEventType.VIEW) {
		var toleranceSetId = scriptContext.newRecord.getValue('id');
		var scr = "require(['/SuiteScripts/QualityModule/qualityModule'], function(quality) { quality.completeToleranceSet('" + toleranceSetId + "'); window.location.reload(true);});";
		var refreshButton = scriptContext.form.addButton({
			id : 'custpage_button_refreshtolerances',
			label : 'Refresh Tolerances',
			functionName : scr
		});
	}
	
	/*
	 * C'était un essai : ce code est sans effet, il n'est pas possible de changer le type d'une sublist
	if (scriptContext.type == scriptContext.UserEventType.EDIT) {		
		var toleranceSublist = scriptContext.form.getSublist({
			id: 'recmachcustrecord_mob_toleranceset' 
		});
		toleranceSublist.type = UISERVERWIDGETMODULE.SublistType.LIST;
	}
	 */
	log.debug('beforeLoad done');
}

/**
 * Function definition to be triggered before record is loaded.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {Record} scriptContext.oldRecord - Old record
 * @param {string} scriptContext.type - Trigger type
 * @Since 2015.2
 */
function _beforeSubmit(scriptContext) {
	log.debug('beforeSubmit started');
	if (scriptContext.type == 'delete') {
		deleteTolerances(scriptContext.newRecord.getValue('id'));
	}
	log.debug('beforeSubmit done');
}

/**
 * Function definition to be triggered before record is loaded.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {Record} scriptContext.oldRecord - Old record
 * @param {string} scriptContext.type - Trigger type
 * @Since 2015.2
 */
function _afterSubmit(scriptContext) {

}

function deleteTolerances(toleranceSetId) {
	var searchObj = SEARCHMODULE.create({		
		type: 'customrecord_mob_qm_tolerance',
		columns: ['internalid'],
		filters: [SEARCHMODULE.createFilter({
			name: 'custrecord_mob_toleranceset',
			operator: SEARCHMODULE.Operator.IS,
			values: toleranceSetId
		})]
	});
	searchObj.run().each(function(result) {
		RECORDMODULE['delete']({
			type: 'customrecord_mob_qm_tolerance',
			id: result.getValue('internalid')
		});
		return true;
	});
}

function logRecord(record, logTitle) {
	var logRecord = JSON.stringify(record);
	var recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);		
	}
}

